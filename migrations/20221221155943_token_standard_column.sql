alter table contract_public_abi add "token_standard" smallint NULL;
update contract_public_abi set token_standard = 1 where is_erc20 = true;
alter table contract_public_abi drop column is_erc20;
