ALTER TABLE byoc_token_transactions ADD coin_id int2 NOT NULL;
ALTER TABLE byoc_token_transactions ADD CONSTRAINT byoc_token_transactions_un UNIQUE (withdraw_nonce,epoch_nonce,coin_id);
