ALTER TABLE block_history ADD committee_id smallint;
ALTER TABLE block_history ALTER COLUMN committee_id SET NOT NULL, ALTER COLUMN committee_id TYPE smallint USING 1;
