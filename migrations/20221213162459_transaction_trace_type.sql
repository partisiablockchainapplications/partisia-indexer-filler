ALTER TABLE transaction_trace ADD event_type smallint null;

ALTER TABLE block_transactions DROP CONSTRAINT fk_block_level;

-- I can no longer have this contraint as the transaction events might happen in a different order. This can only apply when is_event_trace is false
-- ALTER TABLE block_transactions ADD CONSTRAINT fk_block_level 
--     FOREIGN KEY (shard_id,block_level) REFERENCES block_history(shard_id,block_level) 
--         ON DELETE cascade,
--     ADD CHECK (is_event_trace = false);


alter table block_transactions add block_hash bytea not null;
alter table block_transactions add block_timestamp bigint not null;