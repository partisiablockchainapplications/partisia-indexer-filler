CREATE TABLE polygon_scan_deposits (
	id bigserial NOT NULL,
	polygon_transaction_hash bytea NOT NULL,
	polygon_block_number int8 NOT NULL,
	polygon_timestamp int8 NOT NULL,
	nonce int8 NOT NULL,
	address bytea NOT NULL,
	amount bytea NOT NULL,
	CONSTRAINT polygon_scan_deposits_polygon_transaction_hash_key UNIQUE (polygon_transaction_hash),
	CONSTRAINT polygon_scan_deposits_nonce_key UNIQUE (nonce),
	CONSTRAINT polygon_scan_deposits_pkey PRIMARY KEY (id)
);

CREATE TABLE polygon_scan_withdraws (
	id bigserial NOT NULL,
	polygon_transaction_hash bytea NOT NULL,
	polygon_block_number int8 NOT NULL,
	polygon_timestamp int8 NOT NULL,
	withdrawal_nonce int8 NOT NULL,
	destination bytea NOT NULL,
	amount bytea NOT NULL,
	epoch_nonce int8 NOT NULL,
	CONSTRAINT polygon_scan_withdraws_polygon_transaction_hash_key UNIQUE (polygon_transaction_hash),
	CONSTRAINT polygon_scan_withdraws_pkey PRIMARY KEY (id),
	CONSTRAINT polygon_scan_withdraws_un UNIQUE (epoch_nonce, withdrawal_nonce)
);