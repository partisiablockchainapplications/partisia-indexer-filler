ALTER TABLE byoc_transfer RENAME COLUMN oracle_nonce TO epoch_nonce;
ALTER TABLE byoc_token_transactions RENAME COLUMN oracle_nonce TO epoch_nonce;
ALTER TABLE etherscan_withdraws RENAME COLUMN oracle_nonce TO epoch_nonce;
