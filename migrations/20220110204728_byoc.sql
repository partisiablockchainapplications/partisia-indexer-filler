-- create and add a seperate table for byoc_deposits. This is becasue there are three transactions for each deposit payload

alter table byoc_transfer drop constraint fk_token_transfer;

CREATE TABLE "byoc_token_deposit" (
    "token_transfer_id" bigserial NOT NULL,
    "shard_id" smallint NOT NULL,
    "transaction_hash" bytea NOT NULL unique,
    CONSTRAINT fk_token_transfer
        FOREIGN KEY(token_transfer_id) 
        REFERENCES byoc_transfer(id)
);

create index if not exists brin_token_transfer_id on byoc_token_deposit using brin (token_transfer_id);

insert into byoc_token_deposit (token_transfer_id, shard_id, transaction_hash)
select b2.id as token_transfer_id, shard_id, transaction_hash 
from byoc_transfer b1
join (
	select min(id) as id, nonce, address, amount
	from byoc_transfer
	group by nonce, address, amount
) b2 on b1.nonce = b2.nonce and b1.address = b2.address and b1.amount = b2.amount;


delete from byoc_transfer where id not in(
	select min(id)
	from byoc_transfer
	group by nonce, address, amount 
);

CREATE UNIQUE INDEX idx_byoc_transfer ON byoc_transfer (nonce, address, amount);


alter table byoc_transfer drop column shard_id;

alter table byoc_transfer drop column transaction_hash;
