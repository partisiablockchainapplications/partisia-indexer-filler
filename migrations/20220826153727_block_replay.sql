ALTER TABLE transaction_trace DROP CONSTRAINT fk_tx_base;
ALTER TABLE transaction_trace ADD CONSTRAINT fk_tx_base FOREIGN KEY (transaction_id) REFERENCES block_transactions(id) ON DELETE CASCADE;

ALTER TABLE block_transactions DROP CONSTRAINT fk_block_level;
ALTER TABLE block_transactions ADD CONSTRAINT fk_block_level FOREIGN KEY (shard_id,block_level) REFERENCES block_history(shard_id,block_level) ON DELETE CASCADE;
