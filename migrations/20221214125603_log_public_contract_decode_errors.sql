CREATE TABLE "abi_decode_errors" (
    "id" bigserial NOT NULL primary key,
    "tx_hash" bytea NOT NULL,
    "error_category" text NOT NULL,
    "error_params" text NOT NULL,
    "error_message" text NOT NULL
);
