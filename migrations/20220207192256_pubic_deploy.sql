CREATE TABLE "contract_recipient" (
    "tx_hash" bytea NOT NULL,
    "address_contract" bytea NOT NULL,
    "address" bytea NOT NULL
    -- dont put in any foreign contraints becasue there are edge cases where I need to insert into the contract_recipient before the actual contract gets indexed
    -- these cases can happen when they are across different shards
--    CONSTRAINT fk_contract_notify
--        FOREIGN KEY(address_contract) 
--        REFERENCES contract_public_abi(address_contract),
--    CONSTRAINT fk_contract_transaction_hash
--        FOREIGN KEY(tx_hash)
--        REFERENCES block_transactions(transaction_hash)
);

CREATE TABLE "contract_public_abi" (
    "id" bigserial primary key,
    "address_contract" bytea NOT NULL UNIQUE,
    "address_creator" bytea NOT NULL,
    "tx_hash" bytea NOT NULL UNIQUE,
    "abi" bytea NOT NULL,
    "is_erc20" boolean NULL,
    "shard_id" smallint null,
    "payload_init" bytea not null,
    CONSTRAINT contract_public_abi_fk
        FOREIGN KEY(tx_hash) 
        REFERENCES block_transactions(transaction_hash)
);


create index idx_address_recipient on contract_recipient using btree (address);

create index idx_address_from on block_transactions using btree (address_from);
create index idx_header_contract on block_transactions using btree (header_contract);
CREATE UNIQUE INDEX idx_contract_recipient ON contract_recipient (tx_hash, address);

ALTER TABLE block_transactions ADD is_public_contract bool not null default false;
update block_transactions set is_public_contract = true where substr(header_contract,1,1) like decode('02','hex');

