ALTER TABLE byoc_token_transactions DROP CONSTRAINT fk_token_transfer;
ALTER TABLE byoc_token_transactions ADD CONSTRAINT fk_token_transfer FOREIGN KEY (token_transfer_id) REFERENCES byoc_transfer(id) ON DELETE CASCADE;

ALTER TABLE byoc_token_transactions ADD CONSTRAINT byoc_token_transactions_fk FOREIGN KEY (transaction_hash) REFERENCES block_transactions(transaction_hash) ON DELETE CASCADE;

-- ALTER TABLE token_transfer DROP CONSTRAINT fk_token_transfer;
-- ALTER TABLE token_transfer ADD CONSTRAINT fk_token_transfer FOREIGN KEY (transaction_hash) REFERENCES block_transactions(transaction_hash) ON DELETE CASCADE;
