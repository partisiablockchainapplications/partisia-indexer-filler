ALTER TABLE byoc_token_deposit RENAME TO byoc_token_transactions;
ALTER TABLE byoc_token_transactions ALTER COLUMN token_transfer_id DROP NOT NULL;
ALTER TABLE byoc_token_transactions ALTER COLUMN token_transfer_id DROP DEFAULT;

ALTER TABLE byoc_token_transactions ADD withdraw_nonce bigint null;
ALTER TABLE byoc_token_transactions ADD signature bytea null;

drop index idx_byoc_transfer;
CREATE UNIQUE INDEX idx_byoc_transfer ON byoc_transfer (deposit_type, nonce);
ALTER TABLE byoc_transfer ALTER COLUMN nonce DROP NOT NULL;
ALTER TABLE byoc_transfer ADD eth_address bytea NULL;

CREATE TABLE IF NOT EXISTS "etherscan_deposits" (
    "id" bigserial primary key,
    "eth_transaction_hash" bytea NOT NULL unique,
    "eth_block_number" bigint NOT NULL,
    "eth_timestamp" bigint NOT NULL,
    "nonce" bigint NOT NULL UNIQUE,
    "address" bytea NOT NULL,
    "amount" bytea NOT NULL
);

CREATE TABLE IF NOT EXISTS "etherscan_withdraws" (
    "id" bigserial primary key,
    "eth_transaction_hash" bytea NOT NULL unique,
    "eth_block_number" bigint NOT NULL,
    "eth_timestamp" bigint NOT NULL,
    "withdrawal_nonce" bigint NOT NULL UNIQUE,
    "destination" bytea NOT NULL,
    "amount" bytea NOT NULL,
    "oracle_nonce" bigint NOT NULL
);
