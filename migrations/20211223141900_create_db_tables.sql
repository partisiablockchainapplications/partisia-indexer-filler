CREATE TABLE IF NOT EXISTS "indexer_status" (
    "shard_id" smallint NOT NULL primary key,
    "chain_id" TEXT NOT NULL,
    "block_level" bigint NOT NULL,
    "block_hash" bytea NOT NULL,
    "block_hash_previous" bytea NOT NULL,
    "block_timestamp" bigint NOT NULL,
    "transaction_count" bigint NOT NULL
);


CREATE TABLE IF NOT EXISTS "block_history" (
    "id" bigserial primary key,
    "shard_id" smallint,
    "block_level" bigint,
    "block_hash" bytea NOT NULL,
    "block_hash_previous" bytea NOT NULL,
    "block_timestamp" bigint NOT NULL,
    "producer" int NOT NULL,
    "transaction_count" int NOT NULL,
    UNIQUE (shard_id, block_level),
    UNIQUE (block_hash),
    CONSTRAINT fk_shard_id
        FOREIGN KEY(shard_id) 
        REFERENCES indexer_status(shard_id)
);


create index if not exists brin_block_timestamp on block_history using brin (block_timestamp);


CREATE TABLE IF NOT EXISTS "block_transactions" (
    "id" bigserial primary key,
    "transaction_hash" bytea NOT NULL unique,
    "shard_id" smallint NOT NULL,
    "block_level" bigint NOT NULL,
    "trx_signature" bytea NOT NULL,
    "address_from" bytea NOT NULL,
    "header_contract" bytea NOT NULL,
    "header_payload_length" int NOT NULL,
    "inner_nonce" int NOT NULL,
    "inner_valid_to" bigint NOT NULL,
    "inner_cost" int NOT NULL,
    "payload_data" bytea NOT NULL,
    CONSTRAINT fk_block_level
        FOREIGN KEY(shard_id, block_level) 
        REFERENCES block_history(shard_id, block_level)
);


CREATE TABLE IF NOT EXISTS "token_transfer" (
    "id" bigserial primary key,
    "shard_id" smallint NOT NULL,
    "transaction_hash" bytea NOT NULL unique,
    "transfer_from" bytea NOT NULL,
    "transfer_to" bytea NOT NULL,
    "symbol" bigint NOT NULL,
    "amount" bigint NOT NULL,
    CONSTRAINT fk_token_transfer
        FOREIGN KEY(transaction_hash) 
        REFERENCES block_transactions(transaction_hash)
);


CREATE TABLE IF NOT EXISTS "byoc_transfer" (
    "id" bigserial primary key,
    "shard_id" smallint NOT NULL,
    "transaction_hash" bytea NOT NULL unique,
    "coin_id" smallint NOT NULL,
    "deposit_type" smallint NOT NULL,
    "nonce" bigint NOT NULL,
    "address" bytea NOT NULL,
    "amount" bytea NOT NULL,
    CONSTRAINT fk_token_transfer
        FOREIGN KEY(transaction_hash) 
        REFERENCES block_transactions(transaction_hash)
);
