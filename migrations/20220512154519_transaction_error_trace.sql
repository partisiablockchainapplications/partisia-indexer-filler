ALTER TABLE block_transactions ADD has_error bool not NULL;
ALTER TABLE block_transactions ADD trace_count int not NULL;
ALTER TABLE block_transactions ADD error_message varchar NULL;

CREATE TABLE transaction_trace (
	transaction_id bigint NOT NULL,
	tx_base bytea NOT NULL,
	tx_hash bytea NOT NULL,
	shard_id smallint NOT NULL,
	execution_succeeded bool NOT NULL,
	error_message varchar NULL,
    CONSTRAINT fk_tx_base
        FOREIGN KEY(transaction_id)
        REFERENCES block_transactions(id)
);
create index if not exists idx_transaction_id on transaction_trace (transaction_id);
create index if not exists idx_tx_base on transaction_trace using btree (tx_base);

