ALTER TABLE byoc_token_transactions ADD oracle_nonce int8 NULL;

ALTER TABLE byoc_transfer ADD oracle_nonce int8 NULL;
DROP INDEX idx_byoc_transfer;
CREATE UNIQUE INDEX byoc_transfer_deposit_type_idx ON byoc_transfer (deposit_type,oracle_nonce,nonce);

