use crate::routes::*;
use rocket_okapi::{
    openapi_get_routes,
    swagger_ui::{make_swagger_ui, SwaggerUIConfig},
};
use sqlx::PgPool;
use std::{
    borrow::{Borrow, Cow},
    cell::RefCell,
};

thread_local!(static DB_POOL_CELL: RefCell<Option<PgPool>> = RefCell::new(None));
thread_local!(static API_PREFIX_CELL: RefCell<Option<Cow<'static, str>>> = RefCell::new(None));

// adding an init function so that I can pass in the variables to the rocket::main function becase the main function requires no parameters
pub fn init(db_pool: PgPool, api_prefix: Cow<'static, str>) {
    DB_POOL_CELL.with(|cell| {
        let mut v = cell.borrow_mut();
        *v = Some(db_pool);
    });

    API_PREFIX_CELL.with(|cell| {
        let mut v = cell.borrow_mut();
        *v = Some(api_prefix);
    });
}
fn get_docs(url: String) -> SwaggerUIConfig {
    SwaggerUIConfig { url, ..Default::default() }
}
// Take a look at #[rocket::launch] and async-entry
// https://api.rocket.rs/v0.5-rc/rocket/attr.main.html
// #[rocket::main], #[rocket::launch], and #[rocket::async_test] need detailed docs.
#[rocket::main]
pub async fn main() -> Result<(), rocket::Error> {
    let db_pool: PgPool = DB_POOL_CELL.with(|cell| cell.borrow().as_ref().expect("must call init first").clone());
    let api_prefix = API_PREFIX_CELL.with(|cell| cell.borrow().as_ref().expect("must call init first").clone());
    let api_prefix: &str = api_prefix.borrow();
    let api_prefix_separator = if api_prefix.ends_with("/") { "" } else { "/" };
    let url_openapi_json = format!("{}{}openapi.json", api_prefix_separator, api_prefix);
    let url_swagger = format!("{}{}swagger", api_prefix_separator, api_prefix);
    // start the api server
    let server: rocket::Rocket<rocket::Ignite> = rocket::build()
        .manage::<PgPool>(db_pool)
        .mount(
            api_prefix,
            openapi_get_routes![
                // endpoints::shutdown,
                endpoints::ping,
                endpoints::status,
                endpoints::block_by_hash,
                endpoints::blocks_by_time,
                endpoints::blocks_latest,
                endpoints::blocks_count,
                endpoints::trx_by_hash,
                endpoints::trxs_from_address,
                endpoints::trxs_to_address,
                endpoints::trxs_by_address,
                endpoints::trxs_by_contract,
                endpoints::trxs_traces_by_base,
                endpoints::trxs_lastest,
                endpoints::trxs_by_address_for_erc20, // deprecated
                endpoints::contract_erc20,            // deprecated
                endpoints::trxs_by_address_for_token,
                endpoints::contract_token,
                endpoints::trxs_count,
                endpoints::byoc_by_address,
                endpoints::byoc_by_transaction,
                endpoints::byoc_withdraw_pending,
                endpoints::byoc_deposit_pending,
                endpoints::transactions_by_block,
                endpoints::contract_public,
            ],
        )
        .mount(&url_swagger, make_swagger_ui(&get_docs(url_openapi_json)))
        .register("/", catchers![endpoints::default_catcher,])
        .ignite()
        .await
        .expect("unable to ignite server");

    let msg = format!(
        "API Server listening on {}:{}{} with {} workers",
        server.config().address,
        server.config().port,
        &api_prefix,
        server.config().workers
    );
    println!("{}", msg);
    println!("swagger docs endpoint: {}:{}{}", server.config().address, server.config().port, &url_swagger);
    server.launch().await
}
// async fn launch_server(rocket: rocket::Rocket<rocket::Ignite>) {
//     if let Err(_e) = rocket.launch().await {
//         panic!("unable to start server");
//     }
// }
