use anyhow::{bail, ensure, Result};
use async_channel::Receiver;
use futures::{
    future, pin_mut,
    stream::{SplitSink, SplitStream},
};
use futures_util::{SinkExt, StreamExt};
use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::{de, Deserialize, Deserializer, Serialize};
use std::{array::TryFromSliceError, net::SocketAddr, sync::Arc};
use tokio::net::TcpStream;
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};

use super::MapSubscribe;
use crate::routes::TypeFixedBytes21;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Subscribe {
    pub data: Vec<SubscribeData>,
}
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum SubscribeTypes {
    Block = 1,
    Transaction = 2,
}
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct SubscribeData {
    pub subscribe_type: SubscribeTypes,
    pub options: Option<SubscribeOptions>,
}
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct SubscribeOptions {
    #[serde(deserialize_with = "ary_hex_address_to_buffer")]
    pub address_from: Vec<TypeFixedBytes21>,
    #[serde(deserialize_with = "ary_hex_address_to_buffer")]
    pub address_to: Vec<TypeFixedBytes21>,
}
pub fn ary_hex_address_to_buffer<'a, D, T>(d: D) -> Result<Vec<T>, D::Error>
where
    D: Deserializer<'a>,
    for<'b> T: std::convert::TryFrom<&'b [u8], Error = TryFromSliceError>,
{
    // deserialize data into Vec<&str>
    let data = <Vec<&str>>::deserialize(d)?;

    // hex decode from &str into Vec<u8>
    match data.into_iter().map(|s| hex::decode(s)).collect::<Result<Vec<_>, _>>() {
        // try to convert the Vec<u8> into an array of [u8;21]
        Ok(v) => match v.into_iter().map(|f| T::try_from(&f)).collect() {
            Ok(a) => Ok(a),
            Err(e) => {
                // address string is valid hex but invalid length to fit into [u8;21]
                Err(de::Error::custom(e))
            }
        },
        Err(e) => {
            // user provided invalid hex string for address
            Err(de::Error::custom(e))
        }
    }
}
pub async fn on_connection(outgoing: &mut SplitSink<WebSocketStream<TcpStream>, Message>, incoming: &mut SplitStream<WebSocketStream<TcpStream>>, map: MapSubscribe, addr: SocketAddr) -> Result<()> {
    // outgoing.close().await.expect("Failed to close");

    // send something back
    // outgoing.send(Message::Ping(vec![])).await?;
    // info!("sent ping to {}", addr);

    // block thread until first message is received; this will determine the subcribers filters
    let msg: Message = if let Some(msg) = incoming.next().await {
        msg?
    } else {
        bail!("No Data found in subscribe message");
    };
    ensure!(!msg.is_empty(), "Received empty message");
    ensure!(msg.is_binary() || msg.is_text(), "Data for subscribe message must in binary or text format");

    let subscribe_options = if msg.is_binary() {
        serde_json::from_slice::<Subscribe>(&msg.into_data())?
    } else {
        serde_json::from_str::<Subscribe>(&msg.into_text()?)?
    };
    info!("msg from subscriber: {}", serde_json::to_string(&subscribe_options)?);

    // created the channel
    let (tx, rx) = async_channel::bounded(1);
    // tokio::spawn(toggle_disconnect(incoming, tx.clone()));
    map.insert(addr, (Arc::new(tx), subscribe_options));

    // blocking for all future subscriptions
    let r = read_client_connected(incoming);
    let s = send_msg(rx, outgoing);

    // This pins r and s to the stack to do the future::select
    pin_mut!(r, s);
    // whichever future returns first, (r) client disconnects or (s) sending fails because client is unavailable
    // should most of the time be (r) first but it happens that a client becomes unavailable in middle of transmission
    future::select(r, s).await;

    // at this point client is no longer available so hangup the channel
    map.remove(&addr);
    info!("ws disconnected: {} -> open connections: {}", &addr, map.len());
    Ok(())
}

async fn send_msg(rx: Receiver<Arc<Vec<u8>>>, outgoing: &mut SplitSink<WebSocketStream<TcpStream>, Message>) {
    loop {
        match rx.recv().await {
            Ok(msg) => {
                // send the message to the client
                // send_msg(outgoing, v.to_vec()).await?;
                if let Err(e) = outgoing.send(Message::Binary(msg.to_vec())).await {
                    // This is the only case I care about
                    info!("outgoing cant send to client, close socket: {}", e);
                    break;
                }
            }
            Err(e) => {
                // Should really ever happen
                error!("error internal socket recv: {}", e);
                break;
            }
        }
    }
}

async fn read_client_connected(incoming: &mut SplitStream<WebSocketStream<TcpStream>>) {
    // As long as the client is connect incoming will be able to await on next.
    loop {
        let msg = incoming.next().await;
        match msg {
            Some(msg) => match msg {
                Ok(msg) => {
                    // Only care about close messages. All other messages will be ignored
                    if msg.is_close() {
                        info!("{}", "client disconnected, close socket gracefully");
                        break;
                    }
                }
                Err(e) => {
                    // this is usually
                    // WebSocket protocol error: Connection reset without closing handshake
                    info!("error on read_client_connected: {}", e);
                    break;
                }
            },
            None => {
                error!("{}", "read_client_connected received None");
                break;
            }
        }
    }
}
