use anyhow::{bail, Result};
use futures::future;
use prost::Message;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

use super::{MapSubscribe, Tx};
use crate::{proto_structs, routes::TypeFixedBytes21, web_socket::ws_open_sockets::SubscribeTypes};

#[derive(Debug, Deserialize, Serialize)]
struct SubscribeBson {
    data: Vec<SubscribeData>,
}
#[derive(Debug, Deserialize, Serialize)]
struct SubscribeData {
    subscribe_type: String,
    options: Option<SubscribeOptions>,
}
#[derive(Debug, Deserialize, Serialize)]
struct SubscribeOptions {
    address_from: Vec<String>,
    address_to: Vec<String>,
}

pub async fn subs_logic(rx: &async_channel::Receiver<proto_structs::WsSenderProto>, map_subs: &MapSubscribe) -> Result<()> {
    let msg = rx.recv().await?;

    let data_type = SubscribeTypes::try_from(u8::try_from(msg.data_type)?)?;
    let sender: Option<TypeFixedBytes21> = if let Some(sender) = &msg.sender { Some(sender.as_slice().try_into()?) } else { None };
    let recipients = msg.recipients.iter().map(|r| r.as_slice().try_into()).collect::<Result<Vec<TypeFixedBytes21>, _>>()?;

    info!("ws msg from poller: {:?}", data_type);

    // send to any subscribed peers
    let vec_tx: Vec<_> = map_subs
        .iter()
        .filter_map(|dash_map| {
            let (_addr, map) = dash_map.pair();
            let (tx, sub_filter) = map;
            // If the subscribe filter is subscribed to the data_type enum
            // let data_type:Subsc
            match sub_filter.data.iter().position(|p| p.subscribe_type == data_type) {
                Some(idx) => {
                    match data_type {
                        SubscribeTypes::Block => {
                            // If it is a Block SubscribeTypes there are no options to match on
                            Some(Arc::clone(&tx))
                        }
                        SubscribeTypes::Transaction => {
                            // This should be a Transaction or Transfer
                            match &sub_filter.data[idx].options {
                                Some(opt) => {
                                    // if their is no filters set by the subscriber then its an automatic match
                                    if opt.address_from.len() == 0 && opt.address_to.len() == 0 {
                                        return Some(tx.clone());
                                    }

                                    // logic if subscriber matches the sender
                                    if let Some(sender) = sender {
                                        if opt.address_from.len() > 0 {
                                            if opt.address_from.contains(&sender) {
                                                return Some(tx.clone());
                                            }
                                        }
                                    }

                                    // logic if subscriber matches the recipient
                                    if recipients.len() > 0 && opt.address_to.len() > 0 {
                                        if recipients.iter().find(|&p| opt.address_to.contains(p)).is_some() {
                                            return Some(tx.clone());
                                        }
                                    }

                                    // at this point of the code the subscriber has specified at least one filter and it did not match
                                    None
                                }
                                None => {
                                    // if no address in option filter then it is a match
                                    Some(tx.clone())
                                }
                            }
                        }
                    }
                }
                None => None,
            }
        })
        .collect();

    // now await them to get the resolve's to complete
    let data_owner = Arc::new(msg.encode_to_vec());
    let vec_futures: Vec<_> = vec_tx.into_iter().map(|tx| send_to_subscribers(tx, Arc::clone(&data_owner))).collect();
    if vec_futures.len() > 0 {
        let (_, errors): (Vec<_>, Vec<_>) = future::join_all(vec_futures).await.into_iter().partition(Result::is_ok);
        let errors: Vec<_> = errors.into_iter().map(Result::unwrap_err).collect();
        errors.into_iter().for_each(|e| error!("ws send error: {}", e));
    }
    Ok(())
}

async fn send_to_subscribers(tx: Arc<Tx>, data: Arc<Vec<u8>>) -> Result<()> {
    if let Err(e) = tx.send(data).await {
        bail!(e);
    } else {
        Ok(())
    }
}
pub async fn subs(rx: async_channel::Receiver<proto_structs::WsSenderProto>, map_subs: MapSubscribe) {
    // while subs_logic(&rx, &map_subs).await.is_ok() {}
    loop {
        if let Err(e) = subs_logic(&rx, &map_subs).await {
            error!("Error sending ws data to subscribers: {}", e);
            break;
        }
    }
}
