use crate::proto_structs;
use anyhow::Result;
use dashmap::DashMap;
use futures::{SinkExt, StreamExt};
use std::{io::Error as IoError, net::SocketAddr, sync::Arc};
use tokio::net::{TcpListener, TcpStream};

pub mod ws_open_sockets;
pub mod ws_subscribe;

type Tx = async_channel::Sender<Arc<Vec<u8>>>;
type MapSubscribe = Arc<DashMap<SocketAddr, (Arc<Tx>, ws_open_sockets::Subscribe)>>;

// each web socket subscriber will get their own connection
async fn handle_connection(map: MapSubscribe, raw_stream: TcpStream, addr: SocketAddr) {
    info!("Incoming TCP connection from: {}", addr);

    if let Ok(ws_stream) = tokio_tungstenite::accept_async(raw_stream).await {
        info!("ws connected: {}", &addr);
        let (mut outgoing, mut incoming) = ws_stream.split();

        match ws_open_sockets::on_connection(&mut outgoing, &mut incoming, map, addr).await {
            Ok(_) => {}
            Err(e) => {
                info!("Error opening WebSocket on_connection {}: {}", addr, e);
                // Send error message to client
                // outgoing.send(Message::Text(format!("Error: {}", e))).await.expect("Failed to close");

                // Close the websocket
                outgoing.close().await.expect("Failed to close");
            }
        }
    } else {
        info!("Error during the websocket handshake occurred; cannot open websocket {}", addr);
    }
}

pub async fn ws_init(rx: async_channel::Receiver<proto_structs::WsSenderProto>, addr: &str) -> Result<(), IoError> {
    let state = MapSubscribe::new(DashMap::new());

    // Create the event loop and TCP listener we'll accept connections on.
    let listener = TcpListener::bind(addr).await.expect("Failed to bind");
    info!("Web Socket listening on: {}", addr);

    tokio::spawn(ws_subscribe::subs(rx, state.clone()));

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, addr)) = listener.accept().await {
        tokio::spawn(handle_connection(state.clone(), stream, addr));
    }

    Ok(())
}
