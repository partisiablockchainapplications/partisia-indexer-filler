use log::info;
use reqwest::Url;
use serde::{de, Deserialize, Deserializer};
use simplelog::LevelFilter;
use std::array::TryFromSliceError;
use std::borrow::Cow;
use std::fs;
use std::path::PathBuf;

use crate::routes::TypeFixedBytes21;

pub fn str_to_url<'a, D>(d: D) -> Result<Url, D::Error>
where
    D: Deserializer<'a>,
{
    let url = <&str>::deserialize(d)?;
    match Url::parse(url) {
        Ok(b) => Ok(b),
        Err(e) => Err(de::Error::custom(e)),
    }
}
// Convert hex string into fixed length bytes
pub fn hex_to_bytes<'a, D, T>(d: D) -> Result<T, D::Error>
where
    D: Deserializer<'a>,
    for<'b> T: std::convert::TryFrom<&'b [u8], Error = TryFromSliceError>,
{
    // deserialize data into &str
    let s = <Cow<str>>::deserialize(d)?;
    match hex::decode(s.as_ref()) {
        Ok(b) => match b.as_slice().try_into() {
            Ok(b) => Ok(b),
            Err(e) => Err(de::Error::custom(e)),
        },
        Err(e) => Err(de::Error::custom(e)),
    }
}
// Need to extend LevelFilter into a Struct so that I can impl Deserialize for it
// https://stackoverflow.com/questions/32034529/the-impl-does-not-reference-any-types-defined-in-this-crate
#[derive(Debug)]
pub struct LevelFilterExtended {
    pub filter: LevelFilter,
}
impl<'de> de::Deserialize<'de> for LevelFilterExtended {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let log_level = <Option<&str>>::deserialize(deserializer)?;
        let log_filter: LevelFilter = match log_level {
            Some(s) => s.parse().expect(&format!("Invalid Log Level: {}", s)),
            None => LevelFilter::Info,
        };
        Ok(LevelFilterExtended { filter: log_filter })
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct ShardsEnv {
    pub id: i16,
    pub name: Cow<'static, str>,
    #[serde(deserialize_with = "str_to_url")]
    pub endpoint: Url,
    #[serde(default = "default_block_start")]
    pub block_start: i64,
    #[serde(default = "default_block_end")]
    pub block_end: i64,
    pub replay_from_block: Option<i64>,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub config: ConfigEnv,
    pub contracts: ConfigContracts,
    pub database: ConfigDatabase,
    pub polling: ConfigPolling,
    pub polling_etherscan: ConfigEtherscan,
    pub polling_polygon: ConfigPolygon,
    pub polling_pending_withdraw_ethereum: ConfigPendingWithdrawEthereum,
    pub polling_pending_withdraw_polygon: ConfigPendingWithdrawPolygon,
    pub server: ConfigServer,
    pub web_socket: ConfigWebSocket,
}

#[derive(Debug, Deserialize)]
pub struct ConfigContracts {
    #[serde(deserialize_with = "hex_to_bytes")]
    pub public_deploy: TypeFixedBytes21,
    #[serde(deserialize_with = "hex_to_bytes")]
    pub byoc_ethereum_deposit: TypeFixedBytes21,
    #[serde(deserialize_with = "hex_to_bytes")]
    pub byoc_ethereum_withdraw: TypeFixedBytes21,
    #[serde(deserialize_with = "hex_to_bytes")]
    pub byoc_polygon_deposit: TypeFixedBytes21,
    #[serde(deserialize_with = "hex_to_bytes")]
    pub byoc_polygon_withdraw: TypeFixedBytes21,
}

#[derive(Debug, Deserialize)]
pub struct ConfigEnv {
    pub log_level: LevelFilterExtended,
    pub path_abi_client_bin: Cow<'static, str>,
    pub reqwest_timeout: Option<u64>,
    pub max_arg_length: usize,
}
#[derive(Debug, Deserialize)]
pub struct ConfigDatabase {
    #[serde(default = "database_url_from_env")]
    pub database_url: Cow<'static, str>,
    #[serde(default = "default_db_max_connections")]
    pub db_max_connections: u32,
    #[serde(default = "default_db_query_limit")]
    pub query_limit: i32,
    #[serde(default = "default_db_connect_timeout")]
    pub connect_timeout: u64,
    #[serde(default = "default_db_logging")]
    pub log_db: bool,
}
#[derive(Debug, Deserialize, Clone)]
pub struct ConfigWebSocket {
    #[serde(default = "default_ws_server")]
    pub ws_server: bool,
    #[serde(default = "default_ws_address")]
    pub ws_address: Cow<'static, str>,
}
#[derive(Debug, Deserialize, Clone)]
pub struct ConfigPolling {
    #[serde(default = "default_polling")]
    pub polling: bool,
    pub chain_id: Cow<'static, str>,
    pub shards: Vec<ShardsEnv>,
    #[serde(default = "default_polling_frequency")]
    pub polling_frequency: u64,
}
#[derive(Debug, Deserialize, Clone)]
pub struct ConfigEtherscan {
    #[serde(default = "default_polling")]
    pub polling: bool,
    pub api_endpoint: Cow<'static, str>,
    pub from_block: i64,
    pub address: Cow<'static, str>,
    #[serde(default = "default_api_key_etherscan")]
    pub api_key: Cow<'static, str>,
    pub topic_deposit: Cow<'static, str>,
    pub topic_withdraw: Cow<'static, str>,
    #[serde(default = "default_polling_frequency_etherscan")]
    pub polling_frequency: u64,
}
#[derive(Debug, Deserialize, Clone)]
pub struct ConfigPolygon {
    #[serde(default = "default_polling")]
    pub polling: bool,
    pub api_endpoint: Cow<'static, str>,
    pub from_block: i64,
    pub address: Cow<'static, str>,
    #[serde(default = "default_api_key_polygon")]
    pub api_key: Cow<'static, str>,
    pub topic_deposit: Cow<'static, str>,
    pub topic_withdraw: Cow<'static, str>,
    #[serde(default = "default_polling_frequency_polygon")]
    pub polling_frequency: u64,
}

#[derive(Debug, Deserialize, Clone)]
pub struct ConfigPendingWithdrawEthereum {
    #[serde(default = "default_polling")]
    pub polling: bool,
    pub endpoint: Cow<'static, str>,
    pub polling_frequency: u64,
}

#[derive(Debug, Deserialize, Clone)]
pub struct ConfigPendingWithdrawPolygon {
    #[serde(default = "default_polling")]
    pub polling: bool,
    pub endpoint: Cow<'static, str>,
    pub polling_frequency: u64,
}

#[derive(Debug, Deserialize)]
pub struct ConfigServer {
    #[serde(default = "default_api_server")]
    pub api_server: bool,
    #[serde(default = "default_api_prefix")]
    pub api_prefix: Cow<'static, str>,
}

// This function parses through the .env and converts into a struct
pub fn parse(env_path: &str) -> Config {
    let env_path: PathBuf = [".", env_path].iter().collect();
    let env_raw: String = fs::read_to_string(env_path).expect("Unable to read Config");
    let decoded: Config = toml::from_str(&env_raw).expect(".env.toml file is formatted badly");
    info!("{:#?}", decoded);

    decoded
}

fn database_url_from_env() -> Cow<'static, str> {
    Cow::Owned(dotenv::var("DATABASE_URL").expect("Must supply a DATABASE_URL in .env file"))
}
fn default_api_key_etherscan() -> Cow<'static, str> {
    Cow::Owned(dotenv::var("API_KEY_ETHERSCAN").expect("Must supply a API_KEY_ETHERSCAN in .env file"))
}
fn default_api_key_polygon() -> Cow<'static, str> {
    Cow::Owned(dotenv::var("API_KEY_POLYGON").expect("Must supply a API_KEY_POLYGON in .env file"))
}

fn default_db_max_connections() -> u32 {
    5
}
fn default_db_query_limit() -> i32 {
    20
}
fn default_db_connect_timeout() -> u64 {
    // this is in seconds
    20
}
fn default_db_logging() -> bool {
    false
}
fn default_polling() -> bool {
    false
}
fn default_api_server() -> bool {
    false
}
fn default_ws_server() -> bool {
    false
}
fn default_ws_address() -> Cow<'static, str> {
    Cow::Borrowed("127.0.0.1:3325")
}
fn default_api_prefix() -> Cow<'static, str> {
    Cow::Borrowed("/")
}
fn default_block_start() -> i64 {
    0
}
fn default_block_end() -> i64 {
    i64::MAX
}
fn default_polling_frequency() -> u64 {
    1000
}

fn default_polling_frequency_etherscan() -> u64 {
    // 20 seconds
    20000
}
fn default_polling_frequency_polygon() -> u64 {
    // 20 seconds
    20000
}
