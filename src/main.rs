#[macro_use]
extern crate rocket;

use anyhow::Result;
use clap::{crate_version, App, Arg, ArgMatches};
use dashmap::DashMap;
use futures::future::join_all;
use lazy_static::lazy_static;
use log::info;
use parse_config::ShardsEnv;
use simplelog::{ColorChoice, CombinedLogger, LevelFilter, TermLogger, TerminalMode, WriteLogger};
use sqlx::pool::PoolOptions;
use sqlx::postgres::{PgConnectOptions, PgPool, PgPoolOptions};
use sqlx::{migrate, ConnectOptions, Postgres};
use std::borrow::Cow;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::atomic::Ordering;
use std::{env, fs};
use tokio::sync::RwLock;
use tokio::task::JoinHandle;

use crate::bridge::ethereum::poller_deposit::EthereumDeposit;
use crate::bridge::ethereum::poller_withdraw::EthereumWithdraw;
use crate::bridge::main_polling;
use crate::bridge::polygon::poller_deposit::PolygonDeposit;
use crate::bridge::polygon::poller_withdraw::PolygonWithdraw;
use crate::indexer::bridge_ws::pg_listener;
use crate::routes::TypeFixedBytes21;

mod proto_structs {
    include!(concat!(env!("OUT_DIR"), "/structs.rs"));
}
mod abi;
mod bridge;
mod indexer;
mod parse_config;
mod reqwest_wrapper;
mod routes;
mod rpc;
mod server;
mod web_socket;

lazy_static! {
    static ref SHARD_MAP: DashMap<Cow<'static, str>, ShardsEnv> = DashMap::new();
    static ref SHARD_MAP_ID: DashMap<i16, ShardsEnv> = DashMap::new();
    static ref CONTRACT_PUBLIC_DEPLOY: RwLock<TypeFixedBytes21> = RwLock::new([0; 21]);
    static ref CONTRACT_BYOC_ETHEREUM_DEPOSIT: RwLock<TypeFixedBytes21> = RwLock::new([0; 21]);
    static ref CONTRACT_BYOC_ETHEREUM_WITHDRAW: RwLock<TypeFixedBytes21> = RwLock::new([0; 21]);
    static ref CONTRACT_BYOC_POLYGON_DEPOSIT: RwLock<TypeFixedBytes21> = RwLock::new([0; 21]);
    static ref CONTRACT_BYOC_POLYGON_WITHDRAW: RwLock<TypeFixedBytes21> = RwLock::new([0; 21]);
}

#[tokio::main]
async fn main() -> Result<()> {
    println!("My pid is {}", std::process::id());

    let version = crate_version!();
    let cli_args: ArgMatches = App::new("partia-block-filler")
        .about("Partia Block Filler")
        .version(version)
        .arg(Arg::with_name("daemonize").short("d").long("daemon").help("Run as a background daemon process").takes_value(false))
        .arg(
            Arg::with_name("env.toml path")
                .short("e")
                .long("env-toml-path")
                .help("location of .env.toml file")
                .default_value(".env.toml")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(".env path")
                .short("c")
                .long("env-path")
                .help("location of .env file")
                .default_value(".env")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("Rocket.toml path")
                .short("r")
                .long("rocket-toml-path")
                .help("location of Rocket.toml file")
                .default_value("Rocket.toml")
                .takes_value(true),
        )
        .get_matches();

    // println!("{:?}", cli_args);
    dotenv::from_path(cli_args.value_of(".env path").expect("missing .env file")).ok();

    // set the Rocket.toml config path
    let rocket_config_path: PathBuf = [".", cli_args.value_of("Rocket.toml path").expect("missing Rocket.toml file")].iter().collect();
    env::set_var("ROCKET_CONFIG", rocket_config_path.as_path());

    // parse the env.toml to get the config
    let envs = parse_config::parse(cli_args.value_of("env.toml path").expect("missing env.toml file"));

    // check and set env var for abi-client-bin
    let path_abi_client_bin = PathBuf::from(envs.config.path_abi_client_bin.as_ref()).canonicalize().expect("Failed to convert to absolute path");
    // Check the file exists
    fs::metadata(&path_abi_client_bin).expect("the path to path_abi_client_bin does not exist");
    env::set_var("PATH_ABI_CLIENT_BIN", path_abi_client_bin.as_os_str());
    env::set_var("MAX_ARG_LENGTH", envs.config.max_arg_length.to_string());

    // set the reqwest timeout
    reqwest_wrapper::REQWEST_TIMEOUT.store(envs.config.reqwest_timeout.unwrap_or_default(), Ordering::SeqCst);

    // populate the SHARD_MAP with env config
    envs.polling.shards.iter().for_each(|shard| {
        SHARD_MAP.insert(shard.name.clone(), shard.clone());
    });

    // populate the SHARD_MAP with env config
    envs.polling.shards.iter().for_each(|shard| {
        SHARD_MAP_ID.insert(shard.id, shard.clone());
    });

    // populate the contract info
    *CONTRACT_PUBLIC_DEPLOY.write().await = envs.contracts.public_deploy;
    *CONTRACT_BYOC_ETHEREUM_DEPOSIT.write().await = envs.contracts.byoc_ethereum_deposit;
    *CONTRACT_BYOC_ETHEREUM_WITHDRAW.write().await = envs.contracts.byoc_ethereum_withdraw;
    *CONTRACT_BYOC_POLYGON_DEPOSIT.write().await = envs.contracts.byoc_polygon_deposit;
    *CONTRACT_BYOC_POLYGON_WITHDRAW.write().await = envs.contracts.byoc_polygon_withdraw;

    // connect to the database
    // You can try to use PgConnectOptions to define a hostaddr to 172.17.0.1 and a PgSslMode::Disable instead.
    let db_pool: PgPool = {
        let pg_pool_options: PoolOptions<Postgres> = PgPoolOptions::new()
            .max_connections(envs.database.db_max_connections)
            // .acquire_timeout(timeout)
            .connect_timeout(std::time::Duration::from_secs(envs.database.connect_timeout));
        let mut db_pool_options = PgConnectOptions::from_str(&envs.database.database_url)?;
        if !envs.database.log_db {
            db_pool_options.disable_statement_logging();
        }
        pg_pool_options.connect_with(db_pool_options).await?
    };

    // Create all the tables in Postgres if not there for first time running
    migrate!().run(&db_pool).await?;

    // Set the logging level
    let log_level: LevelFilter = envs.config.log_level.filter; //  LevelFilter::Info;
    CombinedLogger::init(vec![
        TermLogger::new(log_level, simplelog::Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
        WriteLogger::new(log_level, simplelog::Config::default(), File::create("log.log")?),
    ])?;

    // if the cli arg daemonize is present than run this program in the background
    if cli_args.is_present("daemonize") {
        // Fork
        let path_pid: PathBuf = [".", "partisia-filler.pid"].iter().collect();
        #[cfg(unix)]
        daemonize(&path_pid)?;
    }

    // if the config allows polling then the shards we need to poll
    let vec_shards = if envs.polling.polling { envs.polling.shards.clone() } else { Vec::new() };

    // spawn a new thread for Etherscan Polling
    if envs.polling_etherscan.polling {
        // deposits
        let polling_etherscan = envs.polling_etherscan.clone();
        let db_pool_etherscan = db_pool.clone();
        tokio::spawn(async move {
            let etherscan = EthereumDeposit::new(polling_etherscan, db_pool_etherscan);
            main_polling(etherscan).await;
        });

        // withdraws
        let polling_etherscan = envs.polling_etherscan.clone();
        let db_pool_etherscan = db_pool.clone();
        tokio::spawn(async move {
            let etherscan = EthereumWithdraw::new(polling_etherscan, db_pool_etherscan);
            main_polling(etherscan).await;
        });
    }
    // spawn a new thread for Etherscan Polling
    if envs.polling_polygon.polling {
        // deposits
        let polling_polygon = envs.polling_polygon.clone();
        let db_pool_polygon = db_pool.clone();
        tokio::spawn(async move {
            let polygon = PolygonDeposit::new(polling_polygon, db_pool_polygon);
            main_polling(polygon).await;
        });

        // withdraws
        let polling_polygon = envs.polling_polygon.clone();
        let db_pool_polygon = db_pool.clone();
        tokio::spawn(async move {
            let polygon = PolygonWithdraw::new(polling_polygon, db_pool_polygon);
            main_polling(polygon).await;
        });
    }

    // poll pending withdraws to link signatures to table
    if envs.polling_pending_withdraw_ethereum.polling {
        let db_pool_pending_withdraw = db_pool.clone();
        let polling_pending = envs.polling_pending_withdraw_ethereum.clone();
        tokio::spawn(async move {
            let pending = bridge::ethereum::poller_withdraw::PendingWithdraw::new(polling_pending, db_pool_pending_withdraw);
            main_polling(pending).await;
        });
    }
    if envs.polling_pending_withdraw_polygon.polling {
        let db_pool_pending_withdraw = db_pool.clone();
        let polling_pending = envs.polling_pending_withdraw_polygon.clone();
        tokio::spawn(async move {
            let pending = bridge::polygon::poller_withdraw::PendingWithdraw::new(polling_pending, db_pool_pending_withdraw);
            main_polling(pending).await;
        });
    }
    // spawn a new thread for each scanning loop
    let mut vec_tasks: Vec<JoinHandle<indexer::poller::RpcPoller>> = Vec::with_capacity(vec_shards.len());

    for shard in vec_shards {
        // create a poller on a new thread for each shard that will be indexed
        let mut poller: indexer::poller::RpcPoller = indexer::init(db_pool.clone(), &shard, &envs.polling).await?;
        info!("init complete for shard_id {}, spinning to separate thread", shard.id);

        // spawn a new thread with the poller and add to a vec to be join_all later
        let task_poller = tokio::spawn(async {
            poller.start_scanning().await;
            poller
        });
        vec_tasks.push(task_poller);
    }

    if envs.server.api_server {
        // send the query_limit to the mod pg as an atomic int so that I dont need to pass it into each function
        // https://doc.rust-lang.org/std/sync/atomic/
        // https://doc.rust-lang.org/nomicon/atomics.html
        routes::pg::QUERY_LIMIT.store(envs.database.query_limit, Ordering::SeqCst);

        // use std::thread here to prevent tokio error "Cannot start a runtime from within a runtime"
        let db_pool_server = db_pool.clone();
        let api_prefix = envs.server.api_prefix.clone();
        let task_server = std::thread::spawn(move || {
            server::init(db_pool_server, api_prefix);
            server::main().expect("unable to start rocket server")
        });

        // if no polling shards join the rocket server thread to the main thread
        if vec_tasks.len() == 0 {
            task_server.join().expect("unable to join api server to main thread");
        }
    }

    // spawn a listener for the websocket
    if vec_tasks.len() > 0 && envs.web_socket.ws_server {
        // open a channel between the PgListener bridge and the web socket
        let (tx, rx) = async_channel::bounded(1);

        let addr = envs.web_socket.ws_address;
        tokio::spawn(async move {
            web_socket::ws_init(rx, &addr).await.expect("Cannot init web socket");
        });

        tokio::spawn(async move { pg_listener(db_pool.clone(), tx).await });
    }

    // execute the polling tasks to run in parallel
    join_all(vec_tasks).await;

    Ok(())
}

#[cfg(unix)]
fn daemonize<T: AsRef<Path>>(pid_file_path: T) -> Result<()> {
    if let Some(parent) = pid_file_path.as_ref().parent() {
        fs::create_dir_all(parent)?;
    }
    let daemonize = daemonize::Daemonize::new().pid_file(pid_file_path.as_ref()).working_directory(".");
    daemonize.start()?;
    Ok(())
}
