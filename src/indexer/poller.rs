use anyhow::Result;
use async_recursion::async_recursion;
use futures::future::try_join_all;
use log::{error, info};
use serde_json::json;
use sqlx::{
    postgres::{PgPool, Postgres},
    Transaction,
};
use std::convert::TryFrom;
use tokio::time;

use super::post_process::{post_process, PostProcess};
use super::{
    event_decoder::{get_event_for_indexing, parse_all_addresses_from_action},
    FnKind,
};
use super::{InnerTransactionType, TransactionEventTypes};
use crate::abi::{self, log_error_notify_address, take_value_or_bail};
use crate::rpc::api::Rpc;
use crate::rpc::rpc_structs::{RpcBlock, RpcStatus, RpcTransaction, TransactionTrace};
use crate::rpc::RpcEndpoints;

#[derive(Debug)]
pub struct RpcPoller {
    pub rpc: Rpc,
    pub db: PgPool,
    pub status: Option<RpcStatus>,
    pub shard_id: i16,
    pub chain_id: String,
    pub block_level_scan: i64,
    pub polling_frequency: u64,
    pub block_start: i64,
    pub block_end: i64,
    pub block_head: i64,
}
impl RpcPoller {
    async fn get_block_head_or_panic(&self) -> i64 {
        if let Ok(r) = self.rpc.get_block(None).await {
            r.block_level
        } else {
            panic!("Unable to determine the block head from rpc endpoint");
        }
    }
    async fn do_scan(&self) -> Result<Option<RpcStatus>> {
        // if this block has already been done and the head has not increased
        if let Some(status) = self.status.as_ref() {
            if self.block_head == status.block_level {
                // explicit return
                return Ok(None);
            }
        }

        let block: RpcBlock = self.rpc.get_block(Some(self.block_level_scan)).await?;
        // check the status to insure block previous blocks processes have not been orphaned
        if let Some(status) = self.status.as_ref() {
            if hex::decode(&block.block_hash_previous)? != status.block_hash {
                // Should never happen as RPC will only provide final blocks
                // TODO
                error!(
                    "orphaned block {} block.block_hash_previous: {} vs {}",
                    self.block_level_scan,
                    block.block_hash_previous,
                    hex::encode(&status.block_hash)
                );
                error!("block {} {}", self.shard_id, serde_json::to_string_pretty(&block.block_hash_previous)?,);
                error!("status {} {}", self.shard_id, serde_json::to_string_pretty(&self.status)?,);
                return Ok(None);
            }
        }

        // begin a postgres sql transaction with all the data
        let mut db_trx: Transaction<Postgres> = self.db.begin().await?;

        // set the status in the db
        let block_hash = hex::decode(&block.block_hash)?;
        let block_hash_previous = hex::decode(&block.block_hash_previous)?;
        let transaction_count = if let Some(status) = self.status.as_ref() { status.transaction_count } else { 0 } + block.transactions.len() as i64;
        let status = RpcStatus {
            shard_id: self.shard_id,
            chain_id: self.chain_id.clone(),
            block_level: block.block_level,
            block_hash,
            block_hash_previous,
            block_timestamp: block.block_timestamp,
            transaction_count,
        };

        sqlx::query!(
            r#"
            insert into indexer_status (chain_id, block_level, block_hash, block_hash_previous, shard_id, block_timestamp, transaction_count)
            values ($1,$2,$3,$4,$5,$6,$7)
            ON CONFLICT (shard_id)
            DO UPDATE SET 
                chain_id = EXCLUDED.chain_id, 
                block_level = EXCLUDED.block_level, 
                block_hash = EXCLUDED.block_hash, 
                block_hash_previous = EXCLUDED.block_hash_previous,
                transaction_count = EXCLUDED.transaction_count
            ;
            "#,
            &status.chain_id,
            &status.block_level,
            &status.block_hash,
            &status.block_hash_previous,
            &status.shard_id,
            &status.block_timestamp,
            &status.transaction_count,
        )
        .execute(&mut db_trx)
        .await?;
        // set the block_level in the db
        // add the block

        // This is an example of using the row_to_json syntax to get the whole row
        // r#"
        // with block as (
        //     insert into block_history (shard_id, block_level, block_hash, block_hash_previous, block_timestamp, producer, committee_id, transaction_count)
        //     values ($1, $2, $3, $4, $5, $6, $7, $8)
        //     RETURNING row_to_json(block_history.*)
        // )
        // select pg_notify('channel_block', block::text)
        // from block
        // "#,
        sqlx::query!(
            r#"
            with block as (
                insert into block_history (shard_id, block_level, block_hash, block_hash_previous, block_timestamp, producer, committee_id, transaction_count)
                values ($1, $2, $3, $4, $5, $6, $7, $8)
                RETURNING id
            )
            select pg_notify('channel_block', block.id::text)
            from block
            "#,
            &self.shard_id,
            &block.block_level,
            &status.block_hash,
            &status.block_hash_previous,
            &block.block_timestamp,
            &block.producer,
            &block.committee_id,
            block.transactions.len() as i32,
        )
        .execute(&mut db_trx)
        .await?;

        // set a new batch for transactions
        // these are all the transactions including their event trace
        let res_batch_trxs = get_transactions_with_trace(&self.rpc, &block.transactions.iter().map(|t| t.as_str()).collect::<Vec<&str>>()).await?;
        for trx in res_batch_trxs {
            let tx_hash = trx.tx_hash.as_slice();
            let trx_payload: abi::TrxPayload = abi::deserialize_transaction_normal(&trx.transaction_payload)?;
            let address_from = trx.from.as_slice();
            let error_message = if let Some(err_msg) = trx.event_trace.iter().find(|t| t.error_message.is_some()) {
                err_msg.error_message.as_ref()
            } else {
                None
            };
            // add error_message and execution_succeeded properly later
            sqlx::query!(
                r#"
                with trx as (
                    insert into block_transactions (
                        shard_id, block_level, transaction_hash, trx_signature, address_from, 
                        header_contract, header_payload_length, 
                        inner_nonce, inner_valid_to, inner_cost, 
                        payload_data, is_public_contract, has_error, error_message, trace_count,
                        block_hash, block_timestamp
                    )
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)
                    RETURNING id
                )
                select pg_notify('channel_trx', trx.id::text)
                from trx
                "#,
                &self.shard_id,
                &block.block_level,
                &tx_hash,
                &trx_payload.signature,
                &address_from,
                &trx_payload.header.contract.to_vec(),
                &i32::try_from(trx_payload.header.payload_length)?,
                &i32::try_from(trx_payload.inner.nonce)?,
                &i64::try_from(trx_payload.inner.valid_to)?,
                &i32::try_from(trx_payload.inner.cost)?,
                &trx_payload.payload,
                trx_payload.header.contract[0] == 2,
                trx.has_error,
                error_message,
                trx.event_trace.len() as i32,
                &status.block_hash,
                &status.block_timestamp,
            )
            .execute(&mut db_trx)
            .await?;

            for t in &trx.event_trace {
                let event_inner = match trx.has_error {
                    true => None,
                    false => {
                        let json_res = get_event_for_indexing(&t.transaction_payload);
                        let json = if let Err(e) = json_res {
                            // log this in table
                            log_error_notify_address(
                                &mut db_trx,
                                t.tx_hash.as_slice(),
                                "get_event_for_indexing",
                                serde_json::to_string(&json!({ "payload": hex::encode(&t.transaction_payload) }))?.as_str(),
                                e.to_string().as_str(),
                            )
                            .await?;
                            None
                        } else {
                            json_res.ok()
                        };
                        let event_inner = if let Some(mut j) = json {
                            Some((take_value_or_bail::<i16>(&mut j, "event_type")?, j))
                        } else {
                            None
                        };
                        event_inner
                    }
                };
                sqlx::query!(
                    r#"
                    insert into transaction_trace (transaction_id, tx_base, tx_hash, shard_id, execution_succeeded, error_message, event_type)
                    values ( (select id from block_transactions where transaction_hash = $1), $1, $2, $3, $4, $5, $6)
                    "#,
                    &tx_hash,
                    t.tx_hash.as_slice(),
                    t.shard_id,
                    t.execution_succeeded,
                    t.error_message,
                    if event_inner.is_some() { Some(event_inner.as_ref().expect("checked is_some").0) } else { None },
                )
                .execute(&mut db_trx)
                .await?;

                // further index for transaction and callback
                if let Some((event_type, mut json)) = event_inner {
                    let event_type = TransactionEventTypes::try_from(event_type)?;
                    let is_transaction = event_type == TransactionEventTypes::Transaction;
                    let inner_transaction_type = if is_transaction {
                        // get the inner type for a transaction
                        Some(InnerTransactionType::try_from(take_value_or_bail::<i16>(&mut json, "event_transaction_type")?)?)
                    } else {
                        None
                    };

                    match (event_type, inner_transaction_type) {
                        // add in Callback and Transaction of InteractContract into the block_transactions table
                        (TransactionEventTypes::Callback, _) | (TransactionEventTypes::Transaction, Some(InnerTransactionType::InteractContract)) => {
                            let address_from = hex::decode(take_value_or_bail::<String>(&mut json, "address_from")?)?;
                            let header_contract = hex::decode(take_value_or_bail::<String>(&mut json, "header_contract")?)?;
                            let inner_cost = take_value_or_bail::<String>(&mut json, "inner_cost")?.parse::<i32>()?;
                            let payload_data: Vec<u8> = hex::decode(take_value_or_bail::<String>(&mut json, "payload_data")?)?;

                            sqlx::query!(
                                r#"
                                with trx as (
                                    insert into block_transactions (
                                        shard_id, block_level, transaction_hash, trx_signature, address_from,
                                        header_contract, header_payload_length,
                                        inner_nonce, inner_valid_to, inner_cost,
                                        payload_data, is_public_contract, has_error, error_message, trace_count, is_event_trace,
                                        block_hash, block_timestamp
                                    )
                                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, true, $16, $17)
                                    RETURNING id
                                )
                                select pg_notify('channel_trx', trx.id::text)
                                from trx
                                "#,
                                &t.shard_id,
                                &t.block_level,
                                &t.tx_hash.as_slice(),
                                hex_literal::hex!("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000").as_slice(),
                                &address_from,
                                &header_contract,
                                &i32::try_from(payload_data.len())?,
                                0,
                                0,
                                inner_cost,
                                &payload_data,
                                header_contract[0] == 2,
                                !t.execution_succeeded,
                                t.error_message,
                                t.trace_count,
                                t.block_hash.as_slice(),
                                t.block_timestamp,
                            )
                            .execute(&mut db_trx)
                            .await?;

                            // do any post processing needed if this is another transaction
                            // this will only PostProcess for transaction events
                            if !trx.has_error && is_transaction {
                                post_process(
                                    &mut db_trx,
                                    PostProcess {
                                        contract: header_contract.as_slice(),
                                        payload: payload_data.as_slice(),
                                        address_from: address_from.as_slice(),
                                        tx_hash: &t.tx_hash,
                                        shard_id: t.shard_id,
                                    },
                                )
                                .await?;
                            }

                            // if it is a public contact then attempt to derive any addresses from the payload to notify
                            // this should process both transaction events and callback events
                            if !trx.has_error && header_contract[0] == 2 {
                                // assume if it is_transaction then it is an event action otherwise it is an event callback
                                let fn_kind = if is_transaction { FnKind::Action } else { FnKind::Callback };
                                self.public_contract_notify(
                                    &mut db_trx,
                                    t.tx_hash.as_slice(),
                                    address_from.as_slice(),
                                    header_contract.as_slice(),
                                    payload_data.as_slice(),
                                    &fn_kind,
                                )
                                .await?;
                            }
                        }
                        _ => {}
                    }
                }
            }

            if !trx.has_error {
                post_process(
                    &mut db_trx,
                    PostProcess {
                        contract: &trx_payload.header.contract,
                        payload: &trx_payload.payload,
                        address_from: &address_from,
                        tx_hash,
                        shard_id: self.shard_id,
                    },
                )
                .await?;

                // if the base transaction is a public contract then attempt to process it for address notify
                if trx_payload.header.contract[0] == 2 {
                    // assume since this is the base transaction that it is a fnKind of action
                    self.public_contract_notify(
                        &mut db_trx,
                        tx_hash,
                        address_from,
                        trx_payload.header.contract.as_slice(),
                        trx_payload.payload.as_slice(),
                        &FnKind::Action,
                    )
                    .await?;
                }
            }
        }

        // finally commit the postgres transaction
        db_trx.commit().await?;

        Ok(Some(status))
    }
    pub async fn start_scanning(&mut self) {
        // Need to know the block head to do the loop
        self.block_head = self.get_block_head_or_panic().await;
        info!("start_scanning at block {}", self.block_level_scan);

        loop {
            let mut block_level = self.block_level_scan;

            match self.do_scan().await {
                Ok(res) => {
                    if let Some(status) = res {
                        info!("block archived for shard {} {}", self.shard_id, status.block_level);
                        self.status = Some(status);
                    }
                }
                Err(e) => {
                    // catch
                    error!("rpc scan error {} {}: {}", &self.shard_id, block_level, e);
                    block_level = block_level - 1;

                    // Sleep for a little to prevent runaway CPU
                    // Can occur if chain is still syncing and node goes down
                    time::sleep(time::Duration::from_millis(self.polling_frequency)).await;
                }
            }
            // finally

            // On the unlikely event that the rpc client errors
            self.block_head = if let Ok(r) = self.rpc.get_block(None).await { r.block_level } else { self.block_head };

            // If still syncing then do not wait
            let block_next = std::cmp::min(block_level + 1, self.block_head);
            if self.block_head == block_next {
                time::sleep(time::Duration::from_millis(self.polling_frequency)).await;
            }
            self.block_level_scan = block_next;
        }
    }

    async fn get_abi(&self, address_contract: &[u8]) -> Result<Vec<u8>> {
        // notice I am using &self.db and not &mut db_trx so that it is not part of the db transaction
        match sqlx::query!("select abi from contract_public_abi where address_contract = $1", address_contract)
            .fetch_optional(&self.db)
            .await?
        {
            Some(v) => Ok(v.abi),
            None => {
                // if it cannot be found in the db then it is unlikely but possible that it has not been indexed yet. So also check the RPC call
                let res_contract = self
                    .rpc
                    .get_contract(address_contract, false)
                    .await
                    .map_err(|_| anyhow::Error::msg("Cannot find abi in indexer or rpc endpoint"))?;
                Ok(base64::decode(res_contract.abi)?)
            }
        }
    }

    async fn public_contract_notify(&self, db_trx: &mut Transaction<'_, Postgres>, tx_hash: &[u8], address_from: &[u8], address_contract: &[u8], payload: &[u8], fn_kind: &FnKind) -> Result<()> {
        // get the abi
        match self.get_abi(address_contract).await {
            Ok(abi) => {
                match parse_all_addresses_from_action(abi.as_slice(), payload, fn_kind) {
                    Ok(mut addresses) => {
                        addresses.push(address_from.try_into()?);
                        addresses.sort();
                        addresses.dedup();

                        let vec_a = addresses.iter().map(|a| a.to_vec()).collect::<Vec<Vec<u8>>>();

                        // put into contract_recipient
                        sqlx::query!(
                            r#"
                            with a as (
                                select UNNEST($1::bytea[]) as address
                            )
                            insert into contract_recipient (tx_hash, address_contract, address)
                            select $2, $3, a.address
                            from a
                            on conflict do nothing
                            "#,
                            vec_a.as_slice(),
                            tx_hash,
                            address_contract,
                        )
                        .execute(db_trx)
                        .await?;
                    }
                    Err(e) => {
                        // log error
                        log_error_notify_address(
                            db_trx,
                            tx_hash,
                            "parse_all_addresses_from_action",
                            serde_json::to_string(&json!({
                               "abi": hex::encode(abi),
                               "payload": hex::encode(payload),
                               "fnKind": fn_kind.to_string()
                            }))?
                            .as_str(),
                            e.to_string().as_str(),
                        )
                        .await?;
                    }
                }
            }
            Err(e) => {
                // log error
                log_error_notify_address(
                    db_trx,
                    tx_hash,
                    "unable_to_get_rpc",
                    serde_json::to_string(&json!({ "address_contract": hex::encode(address_contract) }))?.as_str(),
                    e.to_string().as_str(),
                )
                .await?;
            }
        }

        Ok(())
    }
}
pub async fn get_transactions_with_trace<T>(rpc: &T, transactions: &[&str]) -> Result<Vec<TransactionTrace>>
where
    T: RpcEndpoints,
{
    let mut batch_trxs = Vec::with_capacity(transactions.len());
    for tx_hash in transactions {
        batch_trxs.push(rpc.get_transaction_trace(tx_hash, true));
    }
    let res_batch_trxs: Vec<TransactionTrace> = try_join_all(batch_trxs).await?;
    Ok(res_batch_trxs)
}

pub async fn _get_transactions_without_trace<T>(rpc: &T, transactions: &[&str]) -> Result<Vec<RpcTransaction>>
where
    T: RpcEndpoints,
{
    let mut batch_trxs = Vec::with_capacity(transactions.len());
    for tx_hash in transactions {
        batch_trxs.push(rpc.get_transaction(tx_hash, true, None));
    }
    let res_batch_trxs: Vec<RpcTransaction> = try_join_all(batch_trxs).await?;
    Ok(res_batch_trxs)
}

#[async_recursion]
pub async fn get_recursive<T>(rpc: &T, tx: &RpcTransaction, vec_trace: &mut Vec<RpcTransaction>) -> Result<()>
where
    T: RpcEndpoints,
    T: std::marker::Sync,
{
    if let Some(events) = &tx.events {
        for event in events {
            let tx = rpc.get_transaction(&event.identifier, true, Some(&event.destination_shard)).await?;
            get_recursive(rpc, &tx, vec_trace).await?;
            vec_trace.push(tx);
        }
    }
    Ok(())
}
// fn do_vecs_match(a: &[u8], b: &[u8]) -> bool {
//     if a.len() != b.len() {
//         return false;
//     }
//     let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
//     matching == a.len()
// }
