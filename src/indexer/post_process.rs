use anyhow::Result;
use sqlx::{postgres::Postgres, Transaction};

use crate::{
    abi::{self, ByocCoins},
    CONTRACT_BYOC_ETHEREUM_DEPOSIT, CONTRACT_BYOC_ETHEREUM_WITHDRAW, CONTRACT_BYOC_POLYGON_DEPOSIT, CONTRACT_BYOC_POLYGON_WITHDRAW, CONTRACT_PUBLIC_DEPLOY,
};

pub struct PostProcess<'a> {
    pub contract: &'a [u8],
    pub payload: &'a [u8],
    pub tx_hash: &'a [u8],
    pub address_from: &'a [u8],
    pub shard_id: i16,
}

// Check if it is a special system contract to index
pub async fn post_process<'a>(db_trx: &mut Transaction<'_, Postgres>, post_process: PostProcess<'a>) -> Result<()> {
    // pub async fn post_process<'a>(post_process: PostProcess<'a>) -> Result<()> {
    let contract_public_deploy = *CONTRACT_PUBLIC_DEPLOY.read().await;
    let contract_byoc_ethereum_deposit = *CONTRACT_BYOC_ETHEREUM_DEPOSIT.read().await;
    let contract_byoc_ethereum_withdraw = *CONTRACT_BYOC_ETHEREUM_WITHDRAW.read().await;
    let contract_byoc_polygon_deposit = *CONTRACT_BYOC_POLYGON_DEPOSIT.read().await;
    let contract_byoc_polygon_withdraw = *CONTRACT_BYOC_POLYGON_WITHDRAW.read().await;

    let PostProcess {
        contract,
        payload,
        address_from,
        tx_hash,
        shard_id,
    } = post_process;
    let invocation = if payload.len() == 0 { None } else { Some(payload[0]) };

    if contract == contract_public_deploy {
        match invocation {
            Some(1) => {
                abi::deserialize_payload_public_deploy(&tx_hash, &payload, &address_from, db_trx).await?;
            }
            _ => {}
        }
    }
    // Deposit Ethereum
    else if contract == contract_byoc_ethereum_deposit && invocation == Some(0) {
        let coin_id = i16::from(ByocCoins::Ethereum);
        let _ = abi::deserialize_payload_byoc_deposit(&tx_hash, &payload, shard_id, coin_id, db_trx).await?;
    }
    // Deposit Polygon
    else if contract == contract_byoc_polygon_deposit && invocation == Some(0) {
        let coin_id = i16::from(ByocCoins::PolygonUSDC);
        let _ = abi::deserialize_payload_byoc_deposit(&tx_hash, &payload, shard_id, coin_id, db_trx).await?;
    }
    // Withdraws Ethereum
    else if contract == contract_byoc_ethereum_withdraw {
        info!("contract_byoc_ethereum_withdraw invocation: {}, shard: {} tx: {}", invocation.unwrap(), shard_id, hex::encode(&tx_hash));
        let coin_id = i16::from(ByocCoins::Ethereum);
        match invocation {
            Some(0) => {
                abi::deserialize_payload_byoc_withdraw(&tx_hash, &payload, shard_id, coin_id, &address_from, db_trx).await?;
            }
            Some(1) => {
                abi::deserialize_payload_byoc_signer(&tx_hash, &payload, shard_id, coin_id, db_trx).await?;
            }
            _ => {}
        }
    }
    // Withdraws Polygon
    else if contract == contract_byoc_polygon_withdraw {
        info!("contract_byoc_polygon_withdraw invocation: {}, shard: {} tx: {}", invocation.unwrap(), shard_id, hex::encode(&tx_hash));
        let coin_id = i16::from(ByocCoins::PolygonUSDC);
        match invocation {
            Some(0) => {
                abi::deserialize_payload_byoc_withdraw(&tx_hash, &payload, shard_id, coin_id, &address_from, db_trx).await?;
            }
            Some(1) => {
                abi::deserialize_payload_byoc_signer(&tx_hash, &payload, shard_id, i16::from(ByocCoins::PolygonUSDC), db_trx).await?;
            }
            _ => {}
        }
    }
    Ok(())
}
