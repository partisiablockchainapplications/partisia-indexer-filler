use anyhow::Result;
use partisia_abi_decoder::{run_abi_client_bin, CmdArg};
use serde_json::Value;

use crate::routes::TypeFixedBytes21;

use super::FnKind;

pub fn get_event_for_indexing(payload: &[u8]) -> Result<Value> {
    let res = run_abi_client_bin(vec![
        CmdArg {
            name: "--fn_name",
            value: "getEventForIndexing",
        },
        CmdArg { name: "--args.enc", value: "base64" },
        CmdArg {
            name: "--args.payload",
            value: base64::encode(payload).as_str(),
        },
    ])?;
    let json = serde_json::from_slice::<Value>(res.as_slice())?;

    Ok(json)
}

pub fn get_token_standard(abi: &[u8]) -> Result<Option<i16>> {
    let res = run_abi_client_bin(vec![
        CmdArg {
            name: "--fn_name",
            value: "getTokenStandard",
        },
        CmdArg { name: "--args.enc", value: "base64" },
        CmdArg {
            name: "--args.abi",
            value: base64::encode(abi).as_str(),
        },
    ])?;
    let json = serde_json::from_slice(res.as_slice())?;

    Ok(json)
}
fn string_to_fixed_bytes(s: &str) -> Result<TypeFixedBytes21> {
    Ok(hex::decode(s)?.as_slice().try_into()?)
}
pub fn parse_all_addresses_from_action(abi: &[u8], payload: &[u8], fn_kind: &FnKind) -> Result<Vec<TypeFixedBytes21>> {
    if payload.len() == 0 {
        // there can be a zero length payload and return empty array
        return Ok(Vec::new());
    }
    let ary_addresses = run_abi_client_bin(vec![
        CmdArg {
            name: "--fn_name",
            value: "parseAllAddressesFromAction",
        },
        CmdArg { name: "--args.enc", value: "base64" },
        CmdArg {
            name: "--args.abi",
            value: base64::encode(abi).as_str(),
        },
        CmdArg {
            name: "--args.payload",
            value: base64::encode(payload).as_str(),
        },
        CmdArg {
            name: "--args.fnKind",
            value: fn_kind.to_string().as_str(),
        },
    ])?;

    let addresses = serde_json::from_slice::<Vec<String>>(&ary_addresses)?;
    Ok(addresses.into_iter().map(|d| string_to_fixed_bytes(&d)).collect::<Result<Vec<TypeFixedBytes21>>>()?)
}
