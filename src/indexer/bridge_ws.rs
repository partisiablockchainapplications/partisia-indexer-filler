use crate::proto_structs;
use crate::routes::structs_db::{DbBlock, DbBlockHeap, DbTransaction, DbTransactionHeap};
use crate::web_socket::ws_open_sockets::SubscribeTypes;

use anyhow::Result;
use async_channel::Sender;
use log::error;
use sqlx::postgres::{PgListener, PgNotification};
use sqlx::PgPool;
use std::convert::TryInto;
use tokio::time;

async fn notify_ws(notification: PgNotification, db_pool: &PgPool, tx: &Sender<proto_structs::WsSenderProto>) -> Result<()> {
    match notification.channel() {
        "channel_block" => {
            let id: i64 = notification.payload().parse()?;
            let db_block: DbBlock = sqlx::query_as!(DbBlockHeap, "select * from block_history where id = $1", id).fetch_one(db_pool).await?.try_into()?;
            let db_block: proto_structs::DbBlock = db_block.into();

            let ws_sender = proto_structs::WsSenderProto {
                data_type: u8::from(SubscribeTypes::Block).into(),
                sender: None,
                recipients: Vec::new(),
                data: Some(proto_structs::ws_sender_proto::Data::Block(db_block)),
            };

            // send the proto data across the thread
            tx.send(ws_sender).await?;
        }
        "channel_trx" => {
            let id: i64 = notification.payload().parse()?;
            // limit on size of payload data to pass through to 1kb 1024
            let db_transaction: DbTransaction = sqlx::query_as!(
                DbTransactionHeap,
                r#"
                select bt.id, bt.transaction_hash, bt.shard_id, bt.block_level, bt.trx_signature, bt.address_from, bt.header_contract, bt.header_payload_length, bt.inner_nonce, bt.inner_valid_to, bt.inner_cost, bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count, bt.is_event_trace,
                    bt.block_hash, bt.block_timestamp,
                    case when header_payload_length <= 1024 then bt.payload_data end as payload_data
                from block_transactions bt
                where bt.id = $1
                "#,
                id
            )
            .fetch_one(db_pool)
            .await?.try_into()?;
            let recipients = vec![db_transaction.header_contract.to_vec()];
            let sender = Some(db_transaction.address_from.to_vec());
            let db_transaction: proto_structs::DbTransaction = db_transaction.into();

            let ws_sender = proto_structs::WsSenderProto {
                data_type: u8::from(SubscribeTypes::Transaction).into(),
                sender,
                recipients,
                data: Some(proto_structs::ws_sender_proto::Data::Transaction(db_transaction)),
            };
            // send the proto data across the thread
            tx.send(ws_sender).await?;
        }
        _ => unreachable!(),
    };
    Ok(())
}

pub async fn pg_listener(db_pool: PgPool, tx: Sender<proto_structs::WsSenderProto>) {
    let mut listener = PgListener::connect_with(&db_pool).await.expect("Cannot cannot to PgListener");
    listener.listen_all(vec!["channel_block", "channel_trx"]).await.expect("Cannot listener to channel");

    loop {
        match listener.recv().await {
            Ok(notification) => {
                if let Err(e) = notify_ws(notification, &db_pool, &tx).await {
                    error!("error notify_ws: {}", e);
                }
            }
            Err(e) => {
                error!("error listening on pg reconnect in 0.5 sec: {}", e);
                time::sleep(time::Duration::from_millis(500)).await;
            }
        }
    }
}
