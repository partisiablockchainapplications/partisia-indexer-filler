use std::fmt;

use anyhow::Result;
use hex::ToHex;
use log::info;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::{ser::SerializeSeq, Serializer};
use sqlx::postgres::PgPool;

use crate::parse_config::{ConfigPolling, ShardsEnv};
use crate::routes::TypeFixedBytes21;
use crate::rpc::{api, rpc_structs, RpcEndpoints};

pub mod bridge_ws;
pub mod event_decoder;
pub mod poller;
pub mod post_process;
#[cfg(test)]
mod test;

#[derive(TryFromPrimitive, PartialEq)]
#[repr(i16)]
enum TransactionEventTypes {
    Transaction = 0,
    Callback = 1,
    System = 2,
    Sync = 3,
}
#[derive(TryFromPrimitive, PartialEq)]
#[repr(i16)]
enum InnerTransactionType {
    DeployContract = 0,
    InteractContract = 1,
}

pub enum FnKind {
    Init,
    Action,
    Callback,
}

#[derive(IntoPrimitive, TryFromPrimitive, PartialEq)]
#[repr(i16)]
pub enum TokenStandard {
    Mpc20 = 1,
    Mpc721 = 2,
}

impl fmt::Display for FnKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FnKind::Init => write!(f, "init"),
            FnKind::Action => write!(f, "action"),
            FnKind::Callback => write!(f, "callback"),
        }
    }
}

pub async fn init(db_pool: PgPool, shard: &ShardsEnv, config_polling: &ConfigPolling) -> Result<poller::RpcPoller> {
    let shard_id = shard.id;
    info!("shard_id: {}", shard_id);

    // init the RPC
    let rpc = api::Rpc::new(shard.name.clone());

    // force match between chainId or panic
    let chain_id = rpc.get_chain_id().await?;
    assert_eq!(chain_id, config_polling.chain_id);

    // get the status from the DB
    let mut db_status = sqlx::query_as!(
        rpc_structs::RpcStatus,
        r#"select shard_id, chain_id, block_level, block_hash, block_hash_previous, block_timestamp, transaction_count from indexer_status where shard_id = $1"#,
        shard_id
    )
    .fetch_optional(&db_pool)
    .await?;

    // use the DbStatus to determine block_start.  Only use .env if there is no status such as the first run
    let mut block_start = if let Some(status) = db_status.as_ref() {
        assert_eq!(status.chain_id, chain_id);
        status.block_level + 1
    } else {
        shard.block_start
    };

    if let Some(replay_from_block) = shard.replay_from_block {
        if replay_from_block < block_start {
            // update the db status for the replay block
            sqlx::query!(
                r#"
                    update indexer_status
                    set 
                        block_level = bh.block_level, 
                        block_hash = bh.block_hash, 
                        block_hash_previous = bh.block_hash_previous, 
                        block_timestamp = bh.block_timestamp, 
                        transaction_count = (
                            select sum(transaction_count)
                            from block_history bh 
                            where shard_id = $1
                            and block_level <= $2
                        )
                    FROM (
                        select block_level, block_hash, block_hash_previous, block_timestamp
                        from block_history
                        where shard_id = $1 and block_level = $2
                    ) as bh
                    where shard_id = $1
                    "#,
                shard_id,
                replay_from_block,
            )
            .execute(&db_pool)
            .await?;

            // delete from block
            sqlx::query!("delete from block_history where shard_id = $1 and block_level > $2", shard_id, replay_from_block,)
                .execute(&db_pool)
                .await?;

            // set block_start and db_status to replay_from_block
            block_start = replay_from_block + 1;
            db_status = sqlx::query_as!(
                rpc_structs::RpcStatus,
                r#"select shard_id, chain_id, block_level, block_hash, block_hash_previous, block_timestamp, transaction_count from indexer_status where shard_id = $1"#,
                shard_id
            )
            .fetch_optional(&db_pool)
            .await?;
        }
    };

    let rpc_poller = poller::RpcPoller {
        rpc,
        db: db_pool,
        shard_id,
        chain_id,
        block_level_scan: block_start,
        block_start,
        block_head: 0,
        status: db_status,
        block_end: shard.block_end,
        polling_frequency: config_polling.polling_frequency,
    };
    // rpc_poller.start_scanning().await;
    Ok(rpc_poller)
}

#[allow(dead_code)]
pub fn sender_to_hex<S>(buffer: &Option<TypeFixedBytes21>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match buffer {
        Some(buf) => {
            let hex_str = buf.encode_hex::<String>();
            serializer.serialize_str(&hex_str)
        }
        None => serializer.serialize_none(),
    }
}
#[allow(dead_code)]
// https://serde.rs/impl-serialize.html
pub fn vec_sender_to_hex<S>(vec_buffers: &Option<Vec<TypeFixedBytes21>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match vec_buffers {
        Some(buf) => {
            let mut seq = serializer.serialize_seq(Some(buf.len()))?;
            for e in buf {
                let hex_str = e.encode_hex::<String>();
                seq.serialize_element(&hex_str)?;
            }
            seq.end()
        }
        _ => unreachable!(),
    }
}

// #[derive(Debug, Serialize)]
// pub enum WsDataType {
//     Block,
//     Transfer,
//     SmartContract,
// }
