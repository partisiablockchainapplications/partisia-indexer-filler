use anyhow::Result;
use sqlx::{PgPool, Postgres, Transaction};
use std::{borrow::Cow, convert::TryInto};

use super::etherscan::get_etherscan_event_log;
use crate::{
    abi::{to_be_u256, ByocCoins, ByocDepositTypes},
    bridge::{
        ethereum::{structs_eth::PartisiaEthWithdraw, withdraw_state::get_withdraw_keys},
        Poller,
    },
    parse_config::{ConfigEtherscan, ConfigPendingWithdrawEthereum},
    routes::structs_db::{PendingNonce, PendingNonceHeap},
};

pub struct EthereumWithdraw {
    db: PgPool,
    etherscan_endpoint: Cow<'static, str>,
    polling_frequency: u64,
    from_block: i64,
    eth_address: Cow<'static, str>,
    etherscan_api_key: Cow<'static, str>,
    topic: Cow<'static, str>,
}

impl EthereumWithdraw {
    pub fn new(cfg: ConfigEtherscan, db: PgPool) -> Self {
        Self {
            etherscan_endpoint: cfg.api_endpoint,
            polling_frequency: cfg.polling_frequency,
            from_block: cfg.from_block,
            eth_address: cfg.address,
            etherscan_api_key: cfg.api_key,
            topic: cfg.topic_withdraw,
            db,
        }
    }
}
#[async_trait]
impl Poller for EthereumWithdraw {
    async fn get_start_block(&self) -> i64 {
        // get the start block
        let block_start = sqlx::query!(r#"select eth_block_number + 1 as block_start from etherscan_withdraws order by id desc limit 1"#)
            .map(|row| row.block_start)
            .fetch_one(&self.db)
            .await
            .unwrap_or(None);
        block_start.unwrap_or(self.from_block)
    }

    async fn do_scan(&self) -> Result<i64> {
        // get all the stuff from etherscan
        // let block_level_hex = format!("0x{:x}", self.from_block);
        let ary_withdraws = get_etherscan_event_log(&self.etherscan_endpoint, &self.etherscan_api_key, &self.eth_address, &self.from_block.to_string(), &self.topic).await?;

        let mut from_block = self.from_block;
        for withdraw in ary_withdraws {
            // insert into the db
            let partisia_eth_withdraw: PartisiaEthWithdraw = withdraw.data.as_slice().try_into()?;
            from_block = withdraw.block_number;
            let amount = to_be_u256(&partisia_eth_withdraw.amount);
            sqlx::query!(
                r#"
                insert into etherscan_withdraws (eth_transaction_hash, eth_block_number, eth_timestamp, withdrawal_nonce, destination, amount, epoch_nonce)
                select $1,$2,$3,$4,$5,$6,$7
                where not exists (select from etherscan_withdraws where withdrawal_nonce = $4 and epoch_nonce = $7);
                "#,
                &withdraw.transaction_hash,
                &withdraw.block_number,
                &withdraw.timestamp * 1000,
                &partisia_eth_withdraw.withdrawal_nonce,
                &partisia_eth_withdraw.destination.as_slice(),
                &amount.as_slice(),
                &partisia_eth_withdraw.epoch_nonce,
            )
            .execute(&self.db)
            .await?;
            info!("etherscan archived for {} {}", "withdraw", withdraw.block_number);

            from_block += 1;
        }

        Ok(from_block)
    }

    fn get_tag(&self) -> Cow<'static, str> {
        Cow::Borrowed("etherscan withdraws")
    }

    fn get_polling_frequency(&self) -> u64 {
        self.polling_frequency
    }

    fn set_from_block(&mut self, from_block: i64) {
        self.from_block = from_block;
    }
}

pub struct PendingWithdraw {
    db: PgPool,
    contract_endpoint: Cow<'static, str>,
    polling_frequency: u64,
    // withdraw_withhold: u64,
}

impl PendingWithdraw {
    pub fn new(cfg: ConfigPendingWithdrawEthereum, db: PgPool) -> Self {
        Self {
            db,
            contract_endpoint: cfg.endpoint,
            polling_frequency: cfg.polling_frequency,
            // 0.1% is withheld as fee
            // withdraw_withhold: 1, // this later gets divided by 1000
        }
    }
}

#[async_trait]
impl Poller for PendingWithdraw {
    fn get_tag(&self) -> Cow<'static, str> {
        Cow::Borrowed("pending partisia withdraws")
    }

    async fn get_start_block(&self) -> i64 {
        // get the start block
        0
    }

    async fn do_scan(&self) -> Result<i64> {
        // link any null nonce to contract state
        // joining on payload_data invocation_byte of 00 so that only the user withdraw is return. invocation_byte of 01 will be the node signers
        let ary_pending = sqlx::query_as!(
            PendingNonceHeap,
            r#"
            select bt.id, bt.amount, bt.eth_address, btt.transaction_hash
            from byoc_transfer bt
            join (
                select btt.token_transfer_id, btt.transaction_hash
                from byoc_token_transactions btt
                join block_transactions bt on btt.transaction_hash = bt.transaction_hash
                where substring(bt.payload_data,1,1) = E'\\x00'
            ) btt on bt.id = btt.token_transfer_id
            where bt.nonce is null and bt.deposit_type = $1 and bt.coin_id = $2 
            order by bt.id desc
            "#,
            i16::from(ByocDepositTypes::Withdraw),
            i16::from(ByocCoins::Ethereum),
        )
        .fetch_all(&self.db)
        .await?
        .into_iter()
        .map(|d| d.try_into())
        .collect::<Result<Vec<PendingNonce>, _>>()?;

        // return early if there is nothing null meaning everything has been populated
        if ary_pending.len() == 0 {
            return Ok(0);
        }

        // get the nonce for the last withdraw that has been added
        let mut withdraw_indexer_nonces: (i64, i64) = sqlx::query!(
            r#"
            select epoch_nonce, nonce 
            from byoc_transfer 
            where 
                nonce is not null and epoch_nonce is not null 
                and deposit_type = $1 and coin_id = $2 
            order by epoch_nonce desc, nonce desc
            limit 1
            "#,
            i16::from(ByocDepositTypes::Withdraw),
            i16::from(ByocCoins::Ethereum),
        )
        .map(|row| (row.epoch_nonce.expect("sql query enforce not null"), row.nonce.expect("sql query enforce not null")))
        .fetch_one(&self.db)
        .await
        .unwrap_or((0, -1));

        // limiting the state return to only the last nonce I have polled
        let withdraw_state_values = get_withdraw_keys(&self.contract_endpoint, withdraw_indexer_nonces).await?;

        // If the epoch_nonce and withdraw_nonce have not changed then return early
        // if withdraw_state_values.epoch_nonce == withdraw_indexer_nonces.0 && withdraw_state_values.withdraw_nonce == withdraw_indexer_nonces.1 {
        //     return Ok(0);
        // }
        info!("withdraw_keys: {:?}", withdraw_indexer_nonces);

        // get any pending from the signature
        for pending in ary_pending {
            // traverse the withdraw state to match on tx_hash
            if let Some(withdraw_key) = withdraw_state_values.iter().find(|w| w.requesting_transaction == pending.transaction_hash) {
                // always update in sequential order
                // if it is the same epoch make sure withdraw_nonce is only +1 otherwise skip
                // if it is the next epoch make sure withdraw_nonce is 0 otherwise skip
                if (withdraw_key.epoch_nonce == withdraw_indexer_nonces.0 && withdraw_key.withdraw_nonce == withdraw_indexer_nonces.1 + 1)
                    || (withdraw_key.epoch_nonce == withdraw_indexer_nonces.0 + 1 && withdraw_key.withdraw_nonce == 0)
                {
                    let mut db_trx: Transaction<Postgres> = self.db.begin().await?;

                    // I have two update queries because as it updates byoc_token_transactions looks like this:
                    // token_transfer_id	shard_id	transaction_hash	withdraw_nonce	signature	epoch_nonce
                    // 1	1	c0030a3ec36ad41219848eaa408f44aa767d817344f22a17235d1a93ce9ad4de	[NULL]	[NULL]	[NULL]
                    // [NULL]	1	59cb4f2500c4f940f59bfc0aa28e8ca2d85c533c54cbbd8b467ba375e3421cbe	0	011475e3f1898ab9a807003837f61b91819d93bc3e7de1a1fc4403115effdfb7b76551ca2fe6e2459c3636bec0fdc11a022a1b4c0631f7b3880fad9b9f396e107c	0
                    // update byoc_transfer for the nonce
                    info!(
                        "updated polling_pending_withdraw: withdraw_nonce {} epoch_nonce {} id {}",
                        &withdraw_key.withdraw_nonce, &withdraw_key.epoch_nonce, &pending.id
                    );
                    sqlx::query!(
                        r#"
                        with bt as (
                            update byoc_transfer
                            set nonce = $1, epoch_nonce = $2
                            where id = $3
                            returning id
                        )
                        update byoc_token_transactions
                        set token_transfer_id = ( select id from bt )
                        where withdraw_nonce = $1 and epoch_nonce = $2 and coin_id = $4
                        "#,
                        &withdraw_key.withdraw_nonce,
                        &withdraw_key.epoch_nonce,
                        &pending.id,
                        i16::from(ByocCoins::Ethereum),
                    )
                    .execute(&mut db_trx)
                    .await?;

                    // update byoc_transfer for the nonce
                    sqlx::query!(
                        r#"
                        update byoc_token_transactions
                        set withdraw_nonce = $1, epoch_nonce = $2
                        where token_transfer_id = $3
                        "#,
                        &withdraw_key.withdraw_nonce,
                        &withdraw_key.epoch_nonce,
                        &pending.id,
                    )
                    .execute(&mut db_trx)
                    .await?;

                    db_trx.commit().await?;

                    info!("updated pending withdraw {:?}", withdraw_key);

                    // increment the withdraw_key with updated nonces
                    withdraw_indexer_nonces = (withdraw_key.epoch_nonce, withdraw_key.withdraw_nonce);
                    // finally remove the nonce from withdraw_keys so it cannot match again
                    // withdraw_keys.remove(i64::try_into(withdraw_key.key)?);
                }
            }
        }

        Ok(0)
    }

    fn get_polling_frequency(&self) -> u64 {
        self.polling_frequency
    }

    // dont need from_block for withdraws as I am polling the db and not the etherscan api
    fn set_from_block(&mut self, _from_block: i64) {}
}
