use anyhow::Result;
use primitive_types::U256;
use serde::{de, Deserialize, Deserializer};
use std::{array::TryFromSliceError, borrow::Cow, convert::TryInto, str::FromStr};

use crate::parse_config::hex_to_bytes;
use crate::reqwest_wrapper::ReqwestWrapper;
use crate::routes::{TypeFixedBytes32, TypeOptFixedBytes20};

#[derive(Debug, Deserialize)]
pub struct WithdrawState {
    #[serde(rename = "serializedContract")]
    serialized_contract: WithdrawRoot,
}

#[derive(Debug, Deserialize)]
struct WithdrawRoot {
    #[serde(rename = "currentEpoch", deserialize_with = "string_to_num")]
    current_epoch: i64,
    // #[serde(rename = "oracleNonce", deserialize_with = "string_to_num")]
    // _oracle_nonce: i64,
    // #[serde(rename = "withdrawNonce", deserialize_with = "string_to_num")]
    // _withdraw_nonce: i64,
    epochs: Vec<Epochs>,
}

#[derive(Debug, Deserialize)]
struct Epochs {
    #[serde(deserialize_with = "string_to_num")]
    key: i64,
    value: EpochValue,
}

#[derive(Debug, Deserialize)]
struct EpochValue {
    withdrawals: Vec<WithdrawsKey>,
}
#[derive(Debug, Deserialize)]
struct WithdrawsKey {
    #[serde(deserialize_with = "string_to_num")]
    key: i64,
    value: WithdrawValue,
}
#[derive(Debug, Deserialize)]
struct WithdrawValue {
    #[serde(deserialize_with = "string_to_u256")]
    amount: U256,
    #[serde(rename = "requestingTransaction", deserialize_with = "hex_to_bytes")]
    requesting_transaction: TypeFixedBytes32,
    receiver: WithdrawEthReciever,
    // #[serde(deserialize_with = "ary_hex_to_sig")]
    // signatures: Vec<[u8; 65]>,
    // signatures: Vec<Option<[u8; 65]>>
}
#[derive(Debug, Deserialize)]
struct WithdrawEthReciever {
    identifier: WithdrawEthIdentifier,
}
#[derive(Debug, Deserialize)]
struct WithdrawEthIdentifier {
    #[serde(rename = "data", deserialize_with = "opt_base64_to_bytes")]
    eth_address: TypeOptFixedBytes20,
}
// fn ary_hex_to_sig<'a, D>(d: D) -> Result<Vec<Option<[u8; 65]>>, D::Error>
// where
//     D: Deserializer<'a>,
// {
//     let data = <Vec<Option<Cow<str>>>>::deserialize(d)?;

//     // deserialize data from null or hex string into Option<[u8;65]>
//     let mut vec: Vec<Option<[u8; 65]>> = Vec::with_capacity(data.len());
//     for d in data {
//         if let Some(hex) = d {
//             match hex::decode(hex.as_ref()) {
//                 Ok(bytes) => match bytes.try_into() {
//                     Ok(a) => vec.push(Some(a)),
//                     Err(e) => {
//                         return Err(de::Error::invalid_length(e.len(), &"hex address of 65 bytes"));
//                     }
//                 },
//                 Err(e) => {
//                     return Err(de::Error::custom(e));
//                 }
//             }
//         } else {
//             vec.push(None)
//         }
//     }

//     Ok(vec)
// }
// #[allow(dead_code)]
// fn ary_hex_to_sig<'a, D>(d: D) -> Result<Vec<[u8; 65]>, D::Error>
// where
//     D: Deserializer<'a>,
// {
//     // deserialize data into Vec<&str>
//     let data = <Vec<Option<Cow<str>>>>::deserialize(d)?;

//     // hex decode from &str into Vec<u8>
//     match data.into_iter().filter_map(|v| v).map(|s| hex::decode(s.as_ref())).collect::<Result<Vec<_>, _>>() {
//         // try to convert the Vec<u8> into an array of [u8;65]
//         Ok(v) => match v.into_iter().map(|f| f.try_into()).collect() {
//             Ok(a) => Ok(a),
//             Err(e) => Err(de::Error::invalid_length(e.len(), &"hex address of 65 bytes")),
//         },
//         Err(e) => {
//             // user provided invalid hex string for address
//             Err(de::Error::custom(e))
//         }
//     }
// }

fn string_to_num<'a, D, T>(d: D) -> Result<T, D::Error>
where
    D: Deserializer<'a>,
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Display,
{
    // deserialize data into &str
    let s = <Cow<str>>::deserialize(d)?;

    // hex decode from &str into i64
    match s.as_ref().parse::<T>() {
        Ok(i) => Ok(i),
        Err(e) => Err(de::Error::custom(e)),
    }
}

fn string_to_u256<'a, D>(d: D) -> Result<U256, D::Error>
where
    D: Deserializer<'a>,
{
    // deserialize data into &str
    let s = <Cow<str>>::deserialize(d)?;

    // hex decode from &str into i64
    match U256::from_dec_str(s.as_ref()) {
        Ok(i) => Ok(i),
        Err(e) => Err(de::Error::custom(e)),
    }
}

// Convert base64 string into fixed length bytes
fn opt_base64_to_bytes<'a, D, T>(d: D) -> Result<Option<T>, D::Error>
where
    D: Deserializer<'a>,
    for<'b> T: std::convert::TryFrom<&'b [u8], Error = TryFromSliceError>,
{
    // deserialize data into &str
    if let Ok(s) = <Cow<str>>::deserialize(d) {
        // base64 decode from &str into array
        if let Ok(b) = base64::decode(s.as_ref()) {
            Ok(b.as_slice().try_into().ok())
        } else {
            Ok(None)
        }
    } else {
        Ok(None)
    }
}

// #[derive(new, Debug, Deserialize)]
// pub struct WithdrawStateValues {
//     // pub epoch_nonce: i64,
//     // pub withdraw_nonce: i64,
//     #[new(default)]
//     pub withdraw_state: Vec<WithdrawStateInfo>,
// }

#[derive(Debug, Deserialize)]
pub struct WithdrawStateInfo {
    pub epoch_nonce: i64,
    pub withdraw_nonce: i64,
    pub eth_address: TypeOptFixedBytes20,
    pub requesting_transaction: TypeFixedBytes32,
    pub amount: U256,
}

pub async fn get_withdraw_keys(url: &str, withdraw_indexer_nonces: (i64, i64)) -> Result<Vec<WithdrawStateInfo>> {
    let client = ReqwestWrapper::new().client;
    let res: WithdrawState = client.get(url).send().await?.json().await?;

    // let mut state = WithdrawStateValues::new(); //, res.serialized_contract.withdraw_nonce);
    let mut state = Vec::new(); //, res.serialized_contract.withdraw_nonce);

    // seems like epoch_nonce is equal to the key and withdraw_nonce is one ahead
    for idx_epoch in withdraw_indexer_nonces.0..=res.serialized_contract.current_epoch {
        if let Some(epoch) = res.serialized_contract.epochs.iter().find(|e| e.key == idx_epoch) {
            // If the epoch is the same then start the idx at where it left off. If epoch has changed then start from 0
            let idx_withdraw_start = if withdraw_indexer_nonces.0 == idx_epoch { withdraw_indexer_nonces.1 + 1 } else { 0 };
            for idx_withdraw in idx_withdraw_start..epoch.value.withdrawals.len() as i64 {
                if let Some(withdraw) = epoch.value.withdrawals.iter().find(|e| e.key == idx_withdraw) {
                    state.push(WithdrawStateInfo {
                        epoch_nonce: epoch.key,
                        withdraw_nonce: withdraw.key,
                        eth_address: withdraw.value.receiver.identifier.eth_address,
                        amount: withdraw.value.amount,
                        requesting_transaction: withdraw.value.requesting_transaction,
                    });
                } else {
                    error!("malformed Withdraw Contract State unable to find withdraw_nonce {}", idx_withdraw);
                }
            }
        } else {
            error!("malformed Withdraw Contract State unable to find epoch_nonce {}", idx_epoch);
        }
    }

    Ok(state)
}
