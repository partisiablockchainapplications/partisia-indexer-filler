use anyhow::Result;
use sqlx::PgPool;
use std::{borrow::Cow, convert::TryInto};

use super::etherscan::get_etherscan_event_log;
use crate::{
    abi::to_be_u256,
    bridge::{ethereum::structs_eth::PartisiaEthDeposit, Poller},
    parse_config::ConfigEtherscan,
};

pub struct EthereumDeposit {
    db: PgPool,
    etherscan_endpoint: Cow<'static, str>,
    polling_frequency: u64,
    from_block: i64,
    eth_address: Cow<'static, str>,
    etherscan_api_key: Cow<'static, str>,
    topic: Cow<'static, str>,
}

impl EthereumDeposit {
    pub fn new(cfg: ConfigEtherscan, db: PgPool) -> Self {
        Self {
            etherscan_endpoint: cfg.api_endpoint,
            polling_frequency: cfg.polling_frequency,
            from_block: cfg.from_block,
            eth_address: cfg.address,
            etherscan_api_key: cfg.api_key,
            topic: cfg.topic_deposit,
            db,
        }
    }
}
#[async_trait]
impl Poller for EthereumDeposit {
    async fn do_scan(&self) -> Result<i64> {
        // get all the stuff from etherscan
        // let block_level_hex = format!("0x{:x}", self.from_block);
        let ary_deposits = get_etherscan_event_log(&self.etherscan_endpoint, &self.etherscan_api_key, &self.eth_address, &self.from_block.to_string(), &self.topic).await?;

        let mut from_block = self.from_block;
        for deposit in ary_deposits {
            // insert into the db
            let partisia_eth_deposit: PartisiaEthDeposit = deposit.data.as_slice().try_into()?;
            from_block = deposit.block_number;

            sqlx::query!(
                r#"
                insert into etherscan_deposits (eth_transaction_hash, eth_block_number, eth_timestamp, nonce, address, amount)
                select $1,$2,$3,$4,$5,$6
                where not exists (select from etherscan_deposits where nonce = $4);
                "#,
                &deposit.transaction_hash,
                &deposit.block_number,
                &deposit.timestamp * 1000,
                &partisia_eth_deposit.nonce,
                &partisia_eth_deposit.address.as_slice(),
                &to_be_u256(&partisia_eth_deposit.amount).to_vec(),
            )
            .execute(&self.db)
            .await?;
            info!("etherscan archived for {} {}", "deposit", deposit.block_number);

            from_block += 1;
        }

        Ok(from_block)
    }

    fn get_polling_frequency(&self) -> u64 {
        self.polling_frequency
    }

    fn get_tag(&self) -> Cow<'static, str> {
        Cow::Borrowed("etherscan deposit")
    }

    async fn get_start_block(&self) -> i64 {
        // get the start block
        let block_start = sqlx::query!(r#"select eth_block_number + 1 as block_start from etherscan_deposits order by id desc limit 1"#)
            .map(|row| row.block_start)
            .fetch_one(&self.db)
            .await
            .unwrap_or(None);
        block_start.unwrap_or(self.from_block)
    }

    fn set_from_block(&mut self, from_block: i64) {
        self.from_block = from_block;
    }
}
