use std::convert::{TryFrom, TryInto};

use super::{
    etherscan::{EtherscanLogEvent, EtherscanRes},
    structs_eth::PartisiaEthDeposit,
};
use crate::bridge::ethereum::{structs_eth::PartisiaEthWithdraw, withdraw_state::WithdrawState};
use primitive_types::U256;
use serde_json::{json, Value};

#[test]
fn etherscan_call_success() {
    let json: Value = json!({
      "status": "1",
      "message": "OK",
      "result": [
        {
          "address": "0x90a362349bfb5edd641d1dd37a3f48286d4f4f73",
          "topics": [
            "0x01bc4bb7c8ad3891d2159f29189c74d9528104fb81b05893732d1f06621e2c4f"
          ],
          "data": "0x0000000000000000000000000000000000000000000000000000000000000005006ab1d227a91f63fb0a2e257774ed2a179f332f22000000000000000000000000000000000000000000000000000000000000000000000000038d7ea4c68000",
          "blockNumber": "0xb18596",
          "timeStamp": "0x61bbd264",
          "gasPrice": "0x9502f908",
          "gasUsed": "0x8334",
          "logIndex": "0x",
          "transactionHash": "0x7f3d47cc4366ed6f8b1ca1b093ddc4bdea9f9f90a6328aa83114199c3e658726",
          "transactionIndex": "0x1"
        }
      ]
    });
    let etherscan_res = EtherscanRes::<Vec<EtherscanLogEvent>>::try_from(json).unwrap();
    let res = etherscan_res.result;
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].address, hex_literal::hex!("90a362349bfb5edd641d1dd37a3f48286d4f4f73"));
    assert_eq!(res[0].block_number, 11634070);
}

#[test]
fn etherscan_call_fail() {
    let json: Value = json!({
      "status": "0",
      "message": "NOTOK",
      "result": "Invalid API Key"
    });
    let err = EtherscanRes::<Vec<EtherscanLogEvent>>::try_from(json).unwrap_err();
    assert_eq!(err.to_string(), "Invalid API Key");
}

#[test]
fn etherscan_call_log_withdraw() {
    let json: Value = json!({
      "status": "1",
      "message": "OK",
      "result": [
        {
          "address": "0xb80791eb684ecf5569d0799f7ed92cf353587b94",
          "topics": [
            "0x418d9d559dcfaccb0d982076dc9b51f3fa42c63f43ffd1cf51a7789b473d5332"
          ],
          "data": "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000009407278718290c2e0b996ed9c5f9cc2b439c043d00000000000000000000000000000000000000000000000001027988ed97a2000000000000000000000000000000000000000000000000000000000000000000",
          "blockNumber": "0xb41a63",
          "timeStamp": "0x61df2514",
          "gasPrice": "0xbc54f9e4",
          "gasUsed": "0x198fa",
          "logIndex": "0x2",
          "transactionHash": "0x05e5949b178e6bb7e9fe2dba7069912ccdfb421d6ef109384d5164e13f7d3d07",
          "transactionIndex": "0x7"
        }
      ]
    });
    let etherscan_res = EtherscanRes::<Vec<EtherscanLogEvent>>::try_from(json).unwrap();
    let result = etherscan_res.result;
    assert_eq!(result.len(), 1);
    assert_eq!(result[0].address, hex_literal::hex!("b80791eb684ecf5569d0799f7ed92cf353587b94"));
}

#[test]
fn etherscan_decode_data_deposit() {
    let data = hex_literal::hex!("0000000000000000000000000000000000000000000000000000000000000005006ab1d227a91f63fb0a2e257774ed2a179f332f22000000000000000000000000000000000000000000000000000000000000000000000000038d7ea4c68000");
    let partisia_eth_deposit: PartisiaEthDeposit = data.as_slice().try_into().unwrap();
    assert_eq!(partisia_eth_deposit.nonce, 5);
    assert_eq!(partisia_eth_deposit.address, hex_literal::hex!("006ab1d227a91f63fb0a2e257774ed2a179f332f22"));
    assert_eq!(partisia_eth_deposit.amount, U256::from_dec_str("1000000000000000").unwrap());
}

#[test]
fn etherscan_decode_data_withdraw() {
    let data = hex_literal::hex!("00000000000000000000000000000000000000000000000000000000000000000000000000000000000000009407278718290c2e0b996ed9c5f9cc2b439c043d0000000000000000000000000000000000000000000000000162ea854d0fc00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003");
    let partisia_eth_withdraw: PartisiaEthWithdraw = data.as_slice().try_into().unwrap();
    // println!("{:?}", partisia_eth_withdraw);
    assert_eq!(partisia_eth_withdraw.withdrawal_nonce, 0);
    assert_eq!(partisia_eth_withdraw.epoch_nonce, 0);
    assert_eq!(partisia_eth_withdraw.bitmask, 3);
    assert_eq!(partisia_eth_withdraw.destination, hex_literal::hex!("9407278718290C2E0b996ed9c5F9Cc2b439c043D"));
    assert_eq!(partisia_eth_withdraw.amount, U256::from_dec_str("99900000000000000").unwrap());
}

#[test]
fn encode_block_level() {
    let block_level = 11813967;
    let block_hex = "0xb4444f";
    let block_level_hex = format!("0x{:x}", block_level);

    // check number to hex
    assert_eq!(block_level_hex, block_hex);

    // check hex to number
    assert_eq!(U256::from_str_radix(block_hex, 16).unwrap().as_u64(), block_level);
}

#[test]
fn hex_decode_with_0x() {
    hex::decode("aabbcc").unwrap();
    hex::decode("0xaabbcc").unwrap_err();
}

#[test]
// curl https://betareader.partisiablockchain.com/shards/Shard0/blockchain/contracts/043b1822925da011657f9ab3d6ff02cf1e0bfe0146\?requireContractState\=true | jq
fn withdraw_state() {
    let json = json!({
      "type": "GOVERNANCE",
      "address": "043b1822925da011657f9ab3d6ff02cf1e0bfe0146",
      "jarHash": "500620f8daba63261a5c079ae8b822e138465269b0e6c15f7df73aa38757b35a",
      "storageLength": 42877,
      "serializedContract": {
        "byocContract": {
          "identifier": {
            "data": "S3nYpfpranO71ucjG2GsUqhr+6k="
          }
        },
        "currentEpoch": "0",
        "epochs": [
          {
            "key": "0",
            "value": {
              "disputes": [],
              "oracles": [
                {
                  "identity": "008769a4de5e4cd742cc39d7d01cd5789bf79f2f23",
                  "key": "A1Zs3JI0DpefDpt895pg6xcJeT12iivMBF8A1ovu7ObY"
                },
                {
                  "identity": "00e46ada75a810bf6ff0f28e47f2c435c6b8ee9ad9",
                  "key": "Am+6zsTkLEm/Z8Wib6ys5bawDaaCeVrNB9V0W1BaPqwN"
                },
                {
                  "identity": "0010c669fa9d10bd561d90ded99df8589e1011591a",
                  "key": "AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB"
                }
              ],
              "withdrawals": [
                {
                  "key": "0",
                  "value": {
                    "amount": "12121000000000000",
                    "receiver": {
                      "identifier": {
                        "data": "FD1qVvh39KaiiPKMYJpyCOvzbcE="
                      }
                    },
                    "requestingTransaction": "fd507367151925f5c5863e8f11fe8633976fc782d53efbb2c9505d2ac15aadbb",
                    "signatures": [
                      "01b4481a76f5e333197dd483deb7819a9e44adf0d0cbd0fe7fac6507947b2eb1d23da8ff5fa17169f0bef71f1c52b48370fd7d58a4769859c139fba67cdf4a0694",
                      "00f6aaf73056c39904371c465cdc288bf71b2b95af5ce5d8ca87db388132c486a47d2db26ece46657fbc65fe869b4cf84810f5a7aa0c00b16f7679046366dadbb1",
                      "000a7175f0cedc0ad50d7fbf04cfd4257f0680e10504b18657de05d63384d77fa6348103274cca7619e7992c2b957dc98a79d16b1e7e44795f2f49440b474f8d65"
                    ]
                  }
                },
                {
                  "key": "1",
                  "value": {
                    "amount": "91100000000000000",
                    "receiver": {
                      "identifier": {
                        "data": "FD1qVvh39KaiiPKMYJpyCOvzbcE="
                      }
                    },
                    "requestingTransaction": "c9c976169f3e8a719b0a7034f9db46feacaedb35f742da0d1e065a438cf98123",
                    "signatures": [
                      "01bc16e64efd2b7c5f6698b6ac35830e73b86d406eb38531de32f76818f7494ccd56636ae61b6e8780061feaa3c5c4087dc2cb43ca79cea43e10f3c6938a2126d5",
                      "0107016716ce5020c7903d48c635ca4ee2500eb4a81b2969b4745493cf021c3a2000a7d63359a2b29f6f7f3454f91dd8f10fe361a041e4c8f6e382f622b76987c3",
                      "01c6f165c3ce81130cd32e1e10af286bcff644cf4e9af4df0a857c6c7c95b8b1e4347213f9fce39af58bd3575963c78a4fb150fdd267cd75c1a8a68034f6bdfffe"
                    ]
                  }
                },
                {
                  "key": "2",
                  "value": {
                    "amount": "100000000000000000",
                    "receiver": {
                      "identifier": {
                        "data": "2wHy59jw2EdxwYfIVWk2PttwRmg="
                      }
                    },
                    "requestingTransaction": "064c72f38c309bc2984118bf9573d8d53cd8dbce3de616c66d4ed70ded9f47ad",
                    "signatures": [
                      "006ea5d1e7a390f3ba6c9fd7b0dc70bee426db1b177533b6f8e8e11df4d47335a506a01c167da8708592eeb92987a4dc50a9d5fb370445a28220b93f6c50f0ef0f",
                      "018a33e3caacfeb2ff20834f9d9eae6457800ca6fd6a1d56c68c4de2b43334ba257daaa92cc1751ea34b10de918dff837306ccf9e454838d480261649b7e9e5dff",
                      "0089900f39da069c75c7046bd013e36b1070d35c6bc4ab328f84c7548540466b030eb3bd9e3e03c90778f4365d2400fd6ffbf3d3140844ac1c7b1a586bf6931b17"
                    ]
                  }
                }
              ]
            }
          }
        ],
        "largeOracleContract": "04f1ab744630e57fb9cfcd42e6ccbf386977680014",
        "maximumWithdrawalPerEpoch": "50000000000000000000",
        "oracleNonce": "0",
        "symbol": "MPC_ROPSTEN",
        "withdrawMinimum": "10000000000000000",
        "withdrawNonce": "3",
        "withdrawalSum": "203221000000000000"
      },
      "abi": ""
    }
    );

    let withdraw_state: WithdrawState = serde_json::from_value(json).unwrap();

    println!("{:?}", withdraw_state);
}
