use anyhow::Result;
use std::borrow::Cow;
use tokio::time;

pub mod ethereum;
pub mod polygon;

#[async_trait]
pub trait Poller {
    async fn get_start_block(&self) -> i64;
    async fn do_scan(&self) -> Result<i64>;
    fn get_polling_frequency(&self) -> u64;
    fn get_tag(&self) -> Cow<'static, str>;
    fn set_from_block(&mut self, from_block: i64);
}

pub async fn main_polling<T: Poller>(mut poller: T) {
    // get the start marker
    let mut from_block = poller.get_start_block().await;
    poller.set_from_block(from_block);
    info!("begin polling {} from {}", poller.get_tag(), from_block);

    loop {
        let scanner = poller.do_scan().await;
        match scanner {
            Ok(r) => {
                from_block = r;
                poller.set_from_block(from_block);
            }
            Err(e) => {
                // catch
                error!("{} error {}: {}", poller.get_tag(), from_block, e);
            }
        }

        // Sleep for a little before next poll
        time::sleep(time::Duration::from_millis(poller.get_polling_frequency())).await;
    }
}
