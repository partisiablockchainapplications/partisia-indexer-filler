use anyhow::{ensure, Result};
use ethabi::{decode, ParamType};
use primitive_types::U256;
use std::convert::{TryFrom, TryInto};

use crate::routes::TypeFixedBytes21;

// From the 'event' Deposit in the abi
pub const SIZE_DATA_DEPOSIT: usize = 32 * 3;
#[derive(Debug)]
pub struct PartisiaPolygonDeposit {
    pub nonce: i64,
    pub address: TypeFixedBytes21,
    pub amount: U256,
}

impl TryFrom<&[u8]> for PartisiaPolygonDeposit {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        ensure!(bytes.len() == SIZE_DATA_DEPOSIT, "size for Transaction Inner should be {} but is {}", SIZE_DATA_DEPOSIT, bytes.len());

        let decoded_output = decode(&[ParamType::Uint(256), ParamType::FixedBytes(32), ParamType::Uint(256)], &bytes).expect("byte length enforced above");

        Ok(Self {
            nonce: decoded_output.get(0).expect("known").to_owned().into_uint().expect("known").as_u64().to_owned().try_into()?,
            address: decoded_output.get(1).expect("known").to_owned().into_fixed_bytes().expect("known")[..21].try_into()?,
            amount: decoded_output.get(2).expect("known").to_owned().into_uint().expect("known").to_owned(),
        })
    }
}

// From the 'event' Withdraw in the abi
pub const SIZE_DATA_WITHDRAW: usize = 32 * 5;
#[derive(Debug)]
pub struct PartisiaPolygonWithdraw {
    pub withdrawal_nonce: i64,
    pub destination: [u8; 20],
    pub amount: U256,
    pub epoch_nonce: i64,
    pub bitmask: i64,
}

impl TryFrom<&[u8]> for PartisiaPolygonWithdraw {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        ensure!(bytes.len() == SIZE_DATA_WITHDRAW, "size for Transaction Inner should be {} but is {}", SIZE_DATA_WITHDRAW, bytes.len());

        let decoded_output = decode(
            &[ParamType::Uint(256), ParamType::FixedBytes(32), ParamType::Uint(256), ParamType::Uint(256), ParamType::Uint(256)],
            &bytes,
        )
        .expect("byte length enforced above");

        Ok(Self {
            withdrawal_nonce: decoded_output.get(0).expect("known").to_owned().into_uint().expect("known").as_u64().to_owned().try_into()?,
            destination: decoded_output.get(1).expect("known").to_owned().into_fixed_bytes().expect("known")[12..].try_into()?,
            amount: decoded_output.get(2).expect("known").to_owned().into_uint().expect("known").to_owned(),
            epoch_nonce: decoded_output.get(3).expect("known").to_owned().into_uint().expect("known").as_u64().to_owned().try_into()?,
            bitmask: decoded_output.get(4).expect("known").to_owned().into_uint().expect("known").as_u64().to_owned().try_into()?,
        })
    }
}
