use primitive_types::U256;
use serde_json::{json, Value};
use std::convert::TryFrom;

use crate::bridge::polygon::polygon_scan::{PolygonScanLogEvent, PolygonScanRes};

#[test]
fn scan_call_success() {
    // curl https://api.polygonscan.com/api\?module\=logs\&action\=getLogs\&toBlock\=latest\&address\=0x4c4ecb1efb3bc2a065af1f714b60980a6562c26f\&topic0\=0x01bc4bb7c8ad3891d2159f29189c74d9528104fb81b05893732d1f06621e2c4f\&apikey\=DTM83GG6RUPBE2A4BSW44E75J469GZHG7G\&fromBlock\=32032472 | jq
    let json: Value = json!({
      "status": "1",
      "message": "OK",
      "result": [
        {
          "address": "0x4c4ecb1efb3bc2a065af1f714b60980a6562c26f",
          "topics": [
            "0x01bc4bb7c8ad3891d2159f29189c74d9528104fb81b05893732d1f06621e2c4f"
          ],
          "data": "0x000000000000000000000000000000000000000000000000000000000000000000f6218e747b82b0e47227b1c16857ecfb3ef02c1f000000000000000000000000000000000000000000000000000000000000000000000000000000017d7840",
          "blockNumber": "0x1e8f125",
          "blockHash": "0x4d4327450884bd5c71de971d447ecaa09fc62793361a1722e7cb538f7da7d0d2",
          "timeStamp": "0x62fe9051",
          "gasPrice": "0x6fc23ac0d",
          "gasUsed": "0x164b7",
          "logIndex": "0xcb",
          "transactionHash": "0xd75bc8f567fc512399b127428e516bef5e838847d235ca58bcc10c795baae887",
          "transactionIndex": "0x30"
        }
      ]
    });
    let scan_res = PolygonScanRes::<Vec<PolygonScanLogEvent>>::try_from(json).unwrap();
    let res = scan_res.result;
    assert_eq!(res.len(), 1);
    assert_eq!(res[0].address, hex_literal::hex!("4c4ecb1efb3bc2a065af1f714b60980a6562c26f"));
    assert_eq!(res[0].block_number, 32043301);
}

#[test]
fn scan_call_fail() {
    let json: Value = json!({
      "status": "0",
      "message": "NOTOK",
      "result": "Invalid API Key"
    });
    let err = PolygonScanRes::<Vec<PolygonScanLogEvent>>::try_from(json).unwrap_err();
    assert_eq!(err.to_string(), "Invalid API Key");
}

#[test]
fn scan_call_log_withdraw() {
    let json: Value = json!({
      "status": "1",
      "message": "OK",
      "result": [
        {
          "address": "0xb80791eb684ecf5569d0799f7ed92cf353587b94",
          "topics": [
            "0x418d9d559dcfaccb0d982076dc9b51f3fa42c63f43ffd1cf51a7789b473d5332"
          ],
          "data": "0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000009407278718290c2e0b996ed9c5f9cc2b439c043d00000000000000000000000000000000000000000000000001027988ed97a2000000000000000000000000000000000000000000000000000000000000000000",
          "blockNumber": "0xb41a63",
          "timeStamp": "0x61df2514",
          "gasPrice": "0xbc54f9e4",
          "gasUsed": "0x198fa",
          "logIndex": "0x2",
          "transactionHash": "0x05e5949b178e6bb7e9fe2dba7069912ccdfb421d6ef109384d5164e13f7d3d07",
          "transactionIndex": "0x7"
        }
      ]
    });
    let scan_res = PolygonScanRes::<Vec<PolygonScanLogEvent>>::try_from(json).unwrap();
    let result = scan_res.result;
    assert_eq!(result.len(), 1);
    assert_eq!(result[0].address, hex_literal::hex!("b80791eb684ecf5569d0799f7ed92cf353587b94"));
}

#[test]
fn scan_decode_data_deposit() {}

#[test]
fn scan_decode_data_withdraw() {}

#[test]
fn encode_block_level() {
    let block_level = 11813967;
    let block_hex = "0xb4444f";
    let block_level_hex = format!("0x{:x}", block_level);

    // check number to hex
    assert_eq!(block_level_hex, block_hex);

    // check hex to number
    assert_eq!(U256::from_str_radix(block_hex, 16).unwrap().as_u64(), block_level);
}

#[test]
fn hex_decode_with_0x() {
    hex::decode("aabbcc").unwrap();
    hex::decode("0xaabbcc").unwrap_err();
}

#[test]
// curl https://reader.partisiablockchain.com/shards/Shard0/blockchain/contracts/04adfe4aaacc824657e49a59bdc8f14df87aa8531a\?requireContractState\=true | jq
fn withdraw_state() {}
