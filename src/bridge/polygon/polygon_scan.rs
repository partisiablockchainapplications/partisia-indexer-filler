use std::{
    borrow::Cow,
    convert::{TryFrom, TryInto},
};

use anyhow::{ensure, Result};
use primitive_types::U256;
use serde::{de, Deserialize, Deserializer};
use serde_json::Value;

use crate::{abi::take_value_or_bail, reqwest_wrapper::ReqwestWrapper};

#[derive(Debug, Deserialize)]
pub struct PolygonScanRes<T> {
    pub result: T,
}
#[derive(Debug, Deserialize)]
pub struct PolygonScanLogEvent {
    #[serde(deserialize_with = "hex_to_vec")]
    pub address: Vec<u8>,
    pub topics: Vec<String>,
    #[serde(deserialize_with = "hex_to_vec")]
    pub data: Vec<u8>,
    #[serde(rename = "blockNumber", deserialize_with = "hex_to_number")]
    pub block_number: i64,
    #[serde(rename = "timeStamp", deserialize_with = "hex_to_number")]
    pub timestamp: i64,
    #[serde(rename = "transactionHash", deserialize_with = "hex_to_vec")]
    pub transaction_hash: Vec<u8>,
}
impl<T> TryFrom<Value> for PolygonScanRes<T>
where
    T: de::DeserializeOwned,
{
    type Error = anyhow::Error;

    fn try_from(mut json: Value) -> Result<Self, Self::Error> {
        let status: i32 = take_value_or_bail::<String>(&mut json, "status")?.parse()?;
        let message: String = take_value_or_bail(&mut json, "message")?;

        // If there are no records it etherscan returns an error but I dont want it as an error so return result which is usually empty vec
        if status == 0 && message == "No records found" {
            let t: T = take_value_or_bail(&mut json, "result")?;
            return Ok(Self { result: t });
        }
        // If it is an error then result comes in as an error string
        ensure!(status == 1 && message == "OK", take_value_or_bail::<String>(&mut json, "result")?);

        let t: T = take_value_or_bail(&mut json, "result")?;
        Ok(Self { result: t })
    }
}
pub fn hex_to_vec<'a, D>(d: D) -> Result<Vec<u8>, D::Error>
where
    D: Deserializer<'a>,
{
    // deserialize data into &str
    let hex = <Cow<str>>::deserialize(d)?;
    match hex::decode(hex.trim_start_matches("0x")) {
        Ok(data) => Ok(data),
        Err(e) => Err(de::Error::custom(e)),
    }
}
pub fn hex_to_number<'a, D>(d: D) -> Result<i64, D::Error>
where
    D: Deserializer<'a>,
{
    // deserialize data into &str
    let hex = <Cow<str>>::deserialize(d)?;

    // hex decode from &str into i64
    match U256::from_str_radix(&hex, 16) {
        Ok(n) => match n.as_u64().try_into() {
            Ok(i) => Ok(i),
            Err(_) => Err(de::Error::invalid_length(n.as_usize(), &"number too large for i64")),
        },
        Err(e) => Err(de::Error::custom(e)),
    }
    // Ok(num.try_into()?)
}

// https://api.polygonscan.com/api
// returns in asc sorted
pub async fn get_polygon_scan_event_log(url: &str, api_key: &str, addr: &str, from_block: &str, topic: &str) -> Result<Vec<PolygonScanLogEvent>> {
    let client = ReqwestWrapper::new().client;
    let query = [
        ("module", "logs"),
        ("action", "getLogs"),
        ("toBlock", "latest"),
        // https://ethereum.stackexchange.com/questions/26640/find-topic0-topic1-topic2
        // topic is sha3 hashing of event signature Transfer(address,address,uint256) - an ERC20 token event signature
        // /home/linux/git/partisia-bridge-eth/src/boot/eth_abi/abi.json
        // const topicDeposit = Web3.utils.sha3("Deposit(uint64,bytes21,uint256)")
        // const topicWithdraw = Web3.utils.sha3("withdraw(uint64,address,uint256,uint32,bytes)")
        // const topicWithdrawFromPastEpoch = Web3.utils.sha3("withdrawFromPastEpoch(uint64,address,uint256,uint64,bytes32[])")
        ("topic0", topic),
        ("fromBlock", from_block),
        ("address", addr),
        ("apikey", api_key),
    ];

    info!("url: {} query: {:?}", url, query);
    let json: Value = client.get(url).query(&query).send().await?.json().await?;
    let res = PolygonScanRes::<Vec<PolygonScanLogEvent>>::try_from(json)?;
    Ok(res.result)
}
