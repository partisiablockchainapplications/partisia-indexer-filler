use self::structs_db::*;
use primitive_types::U256;
use rocket::response::Responder;
use rocket::serde::{json::Json, Deserialize, Serialize};
use rocket_okapi::{gen::OpenApiGenerator, okapi::openapi3::Responses, response::OpenApiResponderInner, JsonSchema, Result};

pub mod endpoints;
pub mod pg;
pub mod structs_db;

pub type TypeBytes = Vec<u8>;
pub type TypeU256 = U256;
pub type TypeFixedBytes20 = [u8; 20];
pub type TypeFixedBytes21 = [u8; 21];
pub type TypeFixedBytes32 = [u8; 32];
pub type TypeFixedBytes65 = [u8; 65];
pub type TypeOptBytes = Option<Vec<u8>>;
pub type TypeOptFixedBytes20 = Option<TypeFixedBytes20>;
// pub type TypeOptFixedBytes21 = Option<TypeFixedBytes21>;
pub type TypeOptFixedBytes32 = Option<TypeFixedBytes32>;
pub type TypeOptFixedBytes65 = Option<TypeFixedBytes65>;

#[cfg(test)]
mod test;

// https://api.rocket.rs/v0.5-rc/rocket/derive.Responder.html
#[derive(Responder)]
#[response(bound = "T: Serialize, E: Serialize")]
pub enum ResponseJson<T, E> {
    #[response(status = 200, content_type = "json")]
    Ok(Json<T>),
    #[response(status = 500, content_type = "json")]
    Err(Json<E>),
}

impl<T, E> OpenApiResponderInner for ResponseJson<T, E>
where
    T: Serialize,
    T: JsonSchema,
    T: std::marker::Send,
{
    fn responses(gen: &mut OpenApiGenerator) -> Result<Responses> {
        <Json<T>>::responses(gen)
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct ResErr {
    #[serde(skip_serializing_if = "Option::is_none")]
    code: Option<u16>,
    message: String,
}
