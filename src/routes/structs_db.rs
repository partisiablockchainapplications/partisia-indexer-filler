use hex::ToHex;
use macro_rules_attribute::macro_rules_attribute;
use paste::paste;
use rocket_okapi::JsonSchema;
use serde::Serialize;
use std::array::TryFromSliceError;
use std::convert::{TryFrom, TryInto};

use crate::indexer::TokenStandard;

use super::{TypeBytes, TypeFixedBytes20, TypeFixedBytes21, TypeFixedBytes32, TypeFixedBytes65, TypeOptBytes, TypeOptFixedBytes20, TypeOptFixedBytes32, TypeOptFixedBytes65, TypeU256};

// This macro adds a impl From the to convert from db struct into proto_structs
macro_rules! struct_to_proto {
    (
        $(#[$struct_meta:meta])*
        $struct_vis:vis
        struct $StructName:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_ty:ty
            ),* $(,)?
        }
    ) => {
        // generate the struct definition we have been given
        $(#[$struct_meta])*
        $struct_vis
        struct $StructName {
            $(
                $(#[$field_meta])*
                $field_vis $field_name: $field_ty,
            )*
        }
        impl From<$StructName> for $crate::proto_structs::$StructName {
            fn from(st: $StructName) -> Self {
                Self {
                    $(
                        $field_name: st_into!(st, $field_name, $field_ty),
                    )*
                }
            }
        }
    };
}
macro_rules! st_into {
    ($st:ident, $field_name:ident, TypeU256) => {
        $st.$field_name.to_vec()
    };
    ($st:ident, $field_name:ident, TypeFixedBytes20) => {
        $st.$field_name.to_vec()
    };
    ($st:ident, $field_name:ident, TypeFixedBytes21) => {
        $st.$field_name.to_vec()
    };
    ($st:ident, $field_name:ident, TypeFixedBytes32) => {
        $st.$field_name.to_vec()
    };
    ($st:ident, $field_name:ident, TypeFixedBytes65) => {
        $st.$field_name.to_vec()
    };
    ($st:ident, $field_name:ident, $field_ty:ty) => {
        $st.$field_name.into()
    };
}
// This macro creates another struct that is an exact copy except:
// struct name has suffix of Heap (ex: DbByoc -> DbByocHeap)
// any fixed length bytes are converted into Vec<u8>
// am impl for try_into is create to convert from DbByocHeap back into DbByoc
// the point of this is to pull from sqlx:Pg where everything is Vec<u8> and then convert into a struct with fixed bytes
macro_rules! struct_to_heap {
    (
        $(#[$struct_meta:meta])*
        $struct_vis:vis
        struct $StructName:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_ty:ty
            ),* $(,)?
        }
    ) => (
        // generate the struct definition we have been given
        $(#[$struct_meta])*
        $struct_vis
        struct $StructName {
            $(
                $(#[$field_meta])*
                $field_vis $field_name: $field_ty,
            )*
        }

        paste! {
            $(#[$struct_meta])*
            $struct_vis
            struct [< $StructName Heap >] {
                $(
                    $(#[$field_meta])*
                    $field_vis $field_name: ty_heap!($field_ty),
                )*
            }

            // moves the Vec<u8> in fixed size array using try_into from Slice in std::array
            impl TryFrom<[< $StructName Heap >]> for $StructName {
                type Error = TryFromSliceError;
                fn try_from(st: [< $StructName Heap >]) -> Result<Self, Self::Error> {
                    Ok(Self {
                        $(
                            // $field_name: st.$field_name,
                            $field_name: st_try_into!(st, $field_name, $field_ty),
                        )*
                    })
                }
            }

        }
    )
}

macro_rules! struct_to_schema {
    (
        $(#[$struct_meta:meta])*
        $struct_vis:vis
        struct $StructName:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_ty:ty
            ),* $(,)?
        }
    ) => (
        // generate the struct definition we have been given
        $(#[$struct_meta])*
        $struct_vis
        struct $StructName {
            $(
                $(#[$field_meta])*
                $field_vis $field_name: $field_ty,
            )*
        }

        paste! {
            #[derive(Debug, Serialize, JsonSchema)]
            $struct_vis
            struct [< $StructName Schema >] {
                $(
                    // $(#[$field_meta])*
                    $field_vis $field_name: ty_schema!($field_ty),
                )*
            }

            // moves the Vec<u8> in fixed size array using try_into from Slice in std::array
            impl From<$StructName> for [< $StructName Schema >] {
                fn from(st: $StructName) -> Self {
                    Self {
                        $(
                            $field_name: st_into_json_schema!(st, $field_name, $field_ty),
                        )*
                    }
                }
            }

        }
    )
}
macro_rules! st_into_json_schema {
    ($st:ident, $field_name:ident, TypeU256) => {
        $st.$field_name.to_string()
    };
    ($st:ident, $field_name:ident, TypeBytes) => {
        hex::encode($st.$field_name)
    };
    ($st:ident, $field_name:ident, TypeFixedBytes20) => {
        hex::encode($st.$field_name)
    };
    ($st:ident, $field_name:ident, TypeFixedBytes21) => {
        hex::encode($st.$field_name)
    };
    ($st:ident, $field_name:ident, TypeFixedBytes32) => {
        hex::encode($st.$field_name)
    };
    ($st:ident, $field_name:ident, TypeFixedBytes65) => {
        hex::encode($st.$field_name)
    };
    ($st:ident, $field_name:ident, TypeOptBytes) => {
        if let Some(v) = $st.$field_name {
            Some(hex::encode(v))
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes20) => {
        if let Some(v) = $st.$field_name {
            Some(hex::encode(v))
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes21) => {
        if let Some(v) = $st.$field_name {
            Some(hex::encode(v))
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes32) => {
        if let Some(v) = $st.$field_name {
            Some(hex::encode(v))
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes65) => {
        if let Some(v) = $st.$field_name {
            Some(hex::encode(v))
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, $field_ty:ty) => {
        $st.$field_name
    };
}
macro_rules! st_try_into {
    ($st:ident, $field_name:ident, TypeU256) => {
        $st.$field_name.as_slice().try_into()?
    };
    ($st:ident, $field_name:ident, TypeFixedBytes20) => {
        $st.$field_name.as_slice().try_into()?
    };
    ($st:ident, $field_name:ident, TypeFixedBytes21) => {
        $st.$field_name.as_slice().try_into()?
    };
    ($st:ident, $field_name:ident, TypeFixedBytes32) => {
        $st.$field_name.as_slice().try_into()?
    };
    ($st:ident, $field_name:ident, TypeFixedBytes65) => {
        $st.$field_name.as_slice().try_into()?
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes20) => {
        if let Some(v) = $st.$field_name {
            Some(<TypeFixedBytes20>::try_from(v.as_slice())?)
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes21) => {
        if let Some(v) = $st.$field_name {
            Some(<TypeFixedBytes21>::try_from(v.as_slice())?)
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes32) => {
        if let Some(v) = $st.$field_name {
            Some(<TypeFixedBytes32>::try_from(v.as_slice())?)
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, TypeOptFixedBytes65) => {
        if let Some(v) = $st.$field_name {
            Some(<TypeFixedBytes65>::try_from(v.as_slice())?)
        } else {
            None
        }
    };
    ($st:ident, $field_name:ident, $field_ty:ty) => {
        $st.$field_name
    };
}
macro_rules! ty_heap {
    (TypeU256) => {Vec<u8>};
    (TypeFixedBytes20) => {Vec<u8>};
    (TypeFixedBytes21) => {Vec<u8>};
    (TypeFixedBytes32) => {Vec<u8>};
    (TypeFixedBytes65) => {Vec<u8>};
    (TypeOptFixedBytes20) => {Option<Vec<u8>>};
    (TypeOptFixedBytes21) => {Option<Vec<u8>>};
    (TypeOptFixedBytes32) => {Option<Vec<u8>>};
    (TypeOptFixedBytes65) => {Option<Vec<u8>>};
    ($field_ty:ty) => {
        $field_ty
    };
}
macro_rules! ty_schema {
    (TypeBytes) => {String};
    (TypeU256) => {String};
    (TypeFixedBytes20) => {String};
    (TypeFixedBytes21) => {String};
    (TypeFixedBytes32) => {String};
    (TypeFixedBytes65) => {String};
    (TypeOptBytes) => {Option<String>};
    (TypeOptFixedBytes20) => {Option<String>};
    (TypeOptFixedBytes21) => {Option<String>};
    (TypeOptFixedBytes32) => {Option<String>};
    (TypeOptFixedBytes65) => {Option<String>};
    ($field_ty:ty) => {
        $field_ty
    };
}

// this struct will be used to represent database record
#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbStatus {
    pub shard_id: i16,
    pub chain_id: String,
    pub block_level: i64,
    pub block_hash: TypeFixedBytes32,
    pub block_hash_previous: TypeFixedBytes32,
    pub block_timestamp: i64,
    pub transaction_count: i64,
}

#[macro_rules_attribute(struct_to_proto!)]
#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug, Serialize)]
pub struct DbBlock {
    pub id: i64,
    pub shard_id: i16,
    pub block_level: i64,
    pub block_hash: TypeFixedBytes32,
    pub block_hash_previous: TypeFixedBytes32,
    pub block_timestamp: i64,
    pub producer: i32,
    pub committee_id: i16,
    pub transaction_count: i32,
}

#[macro_rules_attribute(struct_to_proto!)]
#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug, Serialize)]
pub struct DbTransaction {
    pub id: i64,
    pub transaction_hash: TypeFixedBytes32,
    pub shard_id: i16,
    pub block_level: i64,
    pub block_hash: TypeFixedBytes32,
    pub block_timestamp: i64,
    #[serde(serialize_with = "fixed_bytes_to_bytes")]
    pub trx_signature: TypeFixedBytes65,
    pub address_from: TypeFixedBytes21,
    pub header_contract: TypeFixedBytes21,
    pub header_payload_length: i32,
    pub inner_nonce: i32,
    pub inner_valid_to: i64,
    pub inner_cost: i32,
    // I am creating an option for payload_data as this can be large and most times don't need it
    pub payload_data: TypeOptBytes,
    pub is_event_trace: bool,
    pub is_public_contract: bool,
    pub trace_count: i32,
    pub has_error: bool,
    pub error_message: Option<String>,
}

#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbTransactionBlock {
    pub id: i64,
    pub transaction_hash: TypeFixedBytes32,
    pub shard_id: i16,
    pub block_level: i64,
}
#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbTransactionTrace {
    pub tx_hash: TypeFixedBytes32,
    pub shard_id: i16,
    pub execution_succeeded: bool,
    pub error_message: Option<String>,
}

#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbByoc {
    pub id: i64,
    pub shard_id: i16,
    pub transaction_hash: TypeFixedBytes32,
    pub coin_id: i16,
    pub deposit_type: i16,
    pub nonce: Option<i64>,
    pub address: TypeFixedBytes21,
    pub amount: TypeU256,
    pub block_hash: TypeFixedBytes32,
    pub block_timestamp: i64,
    pub block_level: i64,
    pub inner_cost: i32,
    pub withdraw_nonce: Option<i64>,
    pub epoch_nonce: Option<i64>,
    pub signature: TypeOptFixedBytes65,
    pub eth_address: TypeOptFixedBytes20,
    pub eth_transaction_hash: TypeOptFixedBytes32,
    pub eth_block_number: Option<i64>,
}

#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbContractPublic {
    pub id: i64,
    pub address_contract: TypeFixedBytes21,
    pub address_creator: TypeFixedBytes21,
    pub tx_hash: TypeFixedBytes32,
    pub abi: TypeBytes,
    pub payload_init: TypeBytes,
    pub token_standard: Option<i16>,
    pub shard_id: Option<i16>,
}

#[macro_rules_attribute(struct_to_schema!)]
#[macro_rules_attribute(struct_to_heap!)]
#[derive(Debug)]
pub struct DbContractPublicLegacy {
    pub id: i64,
    pub address_contract: TypeFixedBytes21,
    pub address_creator: TypeFixedBytes21,
    pub tx_hash: TypeFixedBytes32,
    pub abi: TypeBytes,
    pub payload_init: TypeBytes,
    pub is_erc20: Option<bool>,
    pub shard_id: Option<i16>,
}

impl From<DbContractPublic> for DbContractPublicLegacy {
    fn from(st: DbContractPublic) -> Self {
        let is_erc20 = if let Some(token_standard) = st.token_standard {
            Some(token_standard == i16::from(TokenStandard::Mpc20))
        } else {
            None
        };
        Self {
            id: st.id,
            address_contract: st.address_contract,
            address_creator: st.address_creator,
            tx_hash: st.tx_hash,
            abi: st.abi,
            payload_init: st.payload_init,
            shard_id: st.shard_id,
            is_erc20,
        }
    }
}

#[macro_rules_attribute(struct_to_heap!)]
pub struct PendingNonce {
    pub id: i64,
    pub amount: TypeFixedBytes32,
    pub eth_address: TypeOptFixedBytes20,
    pub transaction_hash: TypeFixedBytes32,
    // address: Vec<u8>,
    // eth_address: Vec<u8>,
}

#[derive(Debug, Serialize, JsonSchema)]
pub struct DbCount {
    pub count: i64,
}

// Create the custom serializers
#[allow(dead_code)]
pub fn buffer_to_hex<S>(buffer: &[u8], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let hex_str = buffer.encode_hex::<String>();
    serializer.serialize_str(&hex_str)
}

pub fn fixed_bytes_to_bytes<S>(buffer: &[u8], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_bytes(&buffer)
}

#[allow(dead_code)]
pub fn opt_buffer_to_hex<S, T>(opt: &Option<T>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: AsRef<[u8]>,
{
    if let Some(buffer) = opt {
        let hex_str = hex::encode(buffer);
        serializer.serialize_str(&hex_str)
    } else {
        serializer.serialize_none()
    }
}
#[allow(dead_code)]
pub fn u256_to_dec_string<S, T>(u256: &T, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: std::fmt::Debug,
{
    serializer.serialize_str(&format!("{:?}", &u256))
}
