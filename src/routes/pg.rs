use super::endpoints::Options;
use super::{
    DbBlock, DbBlockHeap, DbByoc, DbByocHeap, DbContractPublic, DbContractPublicHeap, DbCount, DbStatus, DbStatusHeap, DbTransaction, DbTransactionBlock, DbTransactionBlockHeap, DbTransactionHeap,
    DbTransactionTrace, DbTransactionTraceHeap,
};
use crate::abi::{ByocCoins, ByocDepositTypes};
use anyhow::Result;
use sqlx::postgres::PgPool;
use std::convert::TryInto;
use std::sync::atomic::{AtomicI32, Ordering};

pub static QUERY_LIMIT: AtomicI32 = AtomicI32::new(0);
fn get_query_limit() -> i32 {
    QUERY_LIMIT.load(Ordering::SeqCst)
}

type DB = rocket::State<PgPool>;

pub async fn get_count(db_pool: &DB, table_name: &str) -> Result<DbCount> {
    let count = sqlx::query!("SELECT n_live_tup as count FROM pg_stat_user_tables where relname = $1", table_name)
        .map(|row| row.count)
        .fetch_one(db_pool.inner())
        .await?
        .unwrap_or(0);

    Ok(DbCount { count })
}
pub async fn get_status(db_pool: &DB) -> Result<Vec<DbStatus>> {
    let db_res: Result<Vec<DbStatus>, _> = sqlx::query_as!(
        DbStatusHeap,
        r#"
        select chain_id,block_level,block_hash,block_hash_previous,shard_id,block_timestamp,transaction_count
        from indexer_status
        order by shard_id
        "#,
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_block_by_hash(db_pool: &DB, hash: &str) -> Result<DbBlock> {
    let db_res = sqlx::query_as!(
        DbBlockHeap,
        r#"
        select bh.id,bh.shard_id,bh.block_level,bh.block_hash,bh.block_hash_previous,bh.block_timestamp,bh.producer,bh.committee_id,bh.transaction_count
        from block_history bh
        where block_hash = $1
        "#,
        &hex::decode(hash)?
    )
    .fetch_one(db_pool.inner())
    .await?
    .try_into()?;

    Ok(db_res)
}

pub async fn get_blocks_by_time(db_pool: &DB, from: i64, to: i64) -> Result<Vec<DbBlock>> {
    let db_res: Result<Vec<DbBlock>, _> = sqlx::query_as!(
        DbBlockHeap,
        r#"
        select bh.id,bh.shard_id,bh.block_level,bh.block_hash,bh.block_hash_previous,bh.block_timestamp,bh.producer,bh.committee_id,bh.transaction_count
        from block_history bh
        where block_timestamp >= $1 and block_timestamp <= $2
        order by block_timestamp desc,shard_id
        limit $3
        "#,
        from,
        to,
        get_query_limit() as i64
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_blocks_latest(db_pool: &DB, opt: &Options) -> Result<Vec<DbBlock>> {
    // let lim = std::cmp::min(n.unwrap_or(1), get_query_limit());
    // info!("lim: {}", lim);
    let db_res: Result<Vec<DbBlock>, _> = sqlx::query_as!(
        DbBlockHeap,
        r#"
        select bh.id,bh.shard_id,bh.block_level,bh.block_hash,bh.block_hash_previous,bh.block_timestamp,bh.producer,bh.committee_id,bh.transaction_count
        from block_history bh
        where bh.id < $2
        order by bh.id desc
        limit $1
        "#,
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        &opt.id.unwrap_or(i64::MAX),
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_trx_by_hash(db_pool: &DB, tx_hash: &str) -> Result<DbTransaction> {
    let db_res = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,coalesce(bt.payload_data) as payload_data,bt.block_timestamp,bt.block_hash, bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt 
        where bt.transaction_hash = $1
        order by bt.block_timestamp desc,bt.shard_id
        "#,
        &hex::decode(tx_hash)?,
    )
    .fetch_one(db_pool.inner())
    .await?.try_into()?;

    Ok(db_res)
}

pub async fn get_trx_from_address(db_pool: &DB, address: &str, opt: &Options) -> Result<Vec<DbTransaction>> {
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $4 = false then bt.payload_data end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt 
        where bt.address_from = $1 and bt.id < $2
        order by bt.id desc
        limit $3
        "#,
        &hex::decode(address)?,
        &opt.id.unwrap_or(i64::MAX),
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        opt.skip_payload.unwrap_or(false)
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}
pub async fn get_trx_to_address(db_pool: &DB, address: &str, opt: &Options) -> Result<Vec<DbTransaction>> {
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $4 = false then bt.payload_data end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt 
        left join contract_recipient cr on cr.tx_hash = bt.transaction_hash
        where cr.address = $1 and bt.id < $2
        order by bt.id desc
        limit $3
        "#,
        &hex::decode(address)?,
        &opt.id.unwrap_or(i64::MAX),
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        opt.skip_payload.unwrap_or(false)
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_trxs_traces_by_base(db_pool: &DB, tx_base: &str) -> Result<Vec<DbTransactionTrace>> {
    let db_res: Result<Vec<DbTransactionTrace>, _> = sqlx::query_as!(
        DbTransactionTraceHeap,
        r#"select tx_hash, shard_id, execution_succeeded, error_message from transaction_trace tt where tx_base = $1"#,
        &hex::decode(tx_base)?,
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}
pub async fn get_trxs_by_address(db_pool: &DB, address: &str, opt: &Options) -> Result<Vec<DbTransaction>> {
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $5 = false then bt.payload_data end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt 
        left join contract_recipient cr on cr.tx_hash = bt.transaction_hash and cr.address = $1
        where bt.address_from = $1 and 
            (case when $4 then bt.id > $2 else bt.id < $2 end)
        order by 
            (case when $4 then bt.id end) asc,
            (case when not $4 then bt.id end) desc
        limit $3
        "#,
        &hex::decode(address)?,
        &opt.id.unwrap_or(i64::MAX),
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        opt.asc.unwrap_or(false),
        opt.skip_payload.unwrap_or(false)
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_trxs_by_contract(db_pool: &DB, address: &str, opt: &Options) -> Result<Vec<DbTransaction>> {
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $4 = false then bt.payload_data end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt 
        where bt.header_contract = $1 and bt.id < $2
        order by bt.id desc
        limit $3
        "#,
        &hex::decode(address)?,
        &opt.id.unwrap_or(i64::MAX),
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        opt.skip_payload.unwrap_or(false)
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}
pub async fn get_trxs_latest(db_pool: &DB, opt: &Options) -> Result<Vec<DbTransaction>> {
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $3 = false then bt.payload_data end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt
        where bt.id < $2 and is_event_trace = false
        order by bt.id desc
        limit $1
        "#,
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        &opt.id.unwrap_or(i64::MAX),
        opt.skip_payload.unwrap_or(false)
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_trxs_by_address_for_token(db_pool: &DB, address: &str, token_contract: &str, token_standard: i16, opt: &Options) -> Result<Vec<DbTransaction>> {
    // in this case I if skip_payload is enabled it will still return payload for public contract actions
    let db_res: Result<Vec<DbTransaction>, _> = sqlx::query_as!(
        DbTransactionHeap,
        r#"
        select bt.transaction_hash,bt.shard_id,bt.block_level,bt.trx_signature,bt.address_from,bt.header_contract,bt.header_payload_length,bt.inner_nonce,bt.inner_valid_to,bt.inner_cost,
            case when $3 = false or substring(bt.header_contract,1,1) = E'\\x02' then bt.payload_data else substring(bt.payload_data,1,1) end as payload_data,
            bt.block_timestamp,bt.block_hash,bt.id,bt.is_public_contract,bt.has_error,bt.error_message,bt.trace_count,bt.is_event_trace
        from block_transactions bt
        join contract_public_abi cpa on cpa.token_standard = $7 and cpa.address_contract = $4
        join contract_recipient cr on cr.address_contract = cpa.address_contract and cr.tx_hash = bt.transaction_hash and cr.address = $5
        where (case when $6 then bt.id > $2 else bt.id < $2 end)
        order by
            (case when $6 then bt.id end) asc,
            (case when not $6 then bt.id end) desc
        limit $1
        "#,
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        &opt.id.unwrap_or(i64::MAX),
        opt.skip_payload.unwrap_or(false),
        &hex::decode(token_contract)?,
        &hex::decode(address)?,
        opt.asc.unwrap_or(false),
        token_standard,
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_tokens_by_address(db_pool: &DB, address: &str, token_standard: i16) -> Result<Vec<DbContractPublic>> {
    let db_res: Result<Vec<DbContractPublic>, _> = sqlx::query_as!(
        DbContractPublicHeap,
        r#"
        select cpa.*
        from contract_public_abi cpa
        where
            cpa.token_standard = $2 and
            exists (
                select 1 from contract_recipient cr where cpa.address_contract = cr.address_contract and cr.address =  $1
            ) 
        order by id
        "#,
        &hex::decode(address)?,
        token_standard,
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}
pub async fn get_byoc_transfers_by_address(db_pool: &DB, address: &str, opt: &Options) -> Result<Vec<DbByoc>> {
    let db_res: Result<Vec<DbByoc>, _> = sqlx::query_as!(
        DbByocHeap,
        r#"
        select byt.id, bd.shard_id, bd.transaction_hash, byt.coin_id, byt.deposit_type, byt.nonce, byt.address, byt.amount, byt.eth_address,
            bt.block_timestamp, bt.block_hash, bt.block_level, bt.inner_cost,
            bd.withdraw_nonce, bd.epoch_nonce, bd.signature, 
            eth.eth_transaction_hash,
            eth.eth_block_number
        from byoc_token_transactions bd
        join (
            select *
            from byoc_transfer
            where address = $1 and id < $2
            order by id desc
            limit $3
        ) byt on bd.token_transfer_id = byt.id 
        join block_transactions bt on bd.transaction_hash = bt.transaction_hash
        left join (
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, nonce, null as epoch_nonce, address, amount, coin_id, $4::int2 as deposit_type
            from (
                select *, $6::int2 as coin_id from etherscan_deposits ed 
                union all
                select *, $7::int2 from polygon_scan_deposits psd 
            ) d
            union all
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, withdrawal_nonce, epoch_nonce, destination, amount, coin_id, $5::int2 as deposit_type
            from (
                select *, $6::int2 as coin_id from etherscan_withdraws ew 
                union all
                select *, $7::int2 as coin_id from polygon_scan_withdraws psw
            ) w
        ) eth on eth.coin_id = byt.coin_id and eth.nonce = byt.nonce and eth.deposit_type = byt.deposit_type and
            case when byt.deposit_type = $5 then eth.epoch_nonce = byt.epoch_nonce else true end
        order by byt.id desc, bt.block_timestamp asc
        "#,
        &hex::decode(address)?,
        &opt.id.unwrap_or(i64::MAX),
        std::cmp::min(opt.n.unwrap_or(1), get_query_limit()) as i64,
        i16::from(ByocDepositTypes::Deposit),
        i16::from(ByocDepositTypes::Withdraw),
        i16::from(ByocCoins::Ethereum),
        i16::from(ByocCoins::PolygonUSDC),
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_byoc_transfers_by_transaction(db_pool: &DB, tx_hash: &str) -> Result<Vec<DbByoc>> {
    let db_res: Result<Vec<DbByoc>, _> = sqlx::query_as!(
        DbByocHeap,
        r#"
        select byt.id, bd.shard_id, bd.transaction_hash, byt.coin_id, byt.deposit_type, byt.nonce, byt.address, byt.amount, byt.eth_address,
            bt.block_timestamp, bt.block_hash, bt.block_level, bt.inner_cost,
            bd.withdraw_nonce, bd.epoch_nonce, bd.signature, 
            eth.eth_transaction_hash,
            eth.eth_block_number
        from byoc_token_transactions bd
        join (
            select *
            from byoc_transfer
            where id = (
                select token_transfer_id
                from byoc_token_transactions
                where transaction_hash = $1
            )
            order by id desc
        ) byt on bd.token_transfer_id = byt.id
        join block_transactions bt on bd.transaction_hash = bt.transaction_hash
        left join (
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, nonce, null as epoch_nonce, address, amount, coin_id, $2::int2 as deposit_type
            from (
                select *, $4::int2 as coin_id from etherscan_deposits ed 
                union all
                select *, $5::int2 from polygon_scan_deposits psd 
            ) d
            union all
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, withdrawal_nonce, epoch_nonce, destination, amount, coin_id, $3::int2 as deposit_type
            from (
                select *, $4::int2 as coin_id from etherscan_withdraws ew 
                union all
                select *, $5::int2 as coin_id from polygon_scan_withdraws psw
            ) w
        ) eth on eth.coin_id = byt.coin_id and eth.nonce = byt.nonce and eth.deposit_type = byt.deposit_type and
            case when byt.deposit_type = $3 then eth.epoch_nonce = byt.epoch_nonce else true end
        order by byt.id desc, bt.block_timestamp asc
        "#,
        &hex::decode(tx_hash)?,
        i16::from(ByocDepositTypes::Deposit),
        i16::from(ByocDepositTypes::Withdraw),
        i16::from(ByocCoins::Ethereum),
        i16::from(ByocCoins::PolygonUSDC),
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}
pub async fn get_byoc_transfers_withdraw_pending(db_pool: &DB, address: &str, coin_id: i16) -> Result<Vec<DbByoc>> {
    let db_res: Result<Vec<DbByoc>, _> = sqlx::query_as!(
        DbByocHeap,
        r#"
        select byt.id, bd.shard_id, bd.transaction_hash, byt.coin_id, byt.deposit_type, byt.nonce, byt.address, byt.amount, byt.eth_address,
            bt.block_timestamp, bt.block_hash, bt.block_level, bt.inner_cost,
            bd.withdraw_nonce, bd.epoch_nonce, bd.signature, 

            eth.eth_transaction_hash,
            eth.eth_block_number

        from byoc_token_transactions bd
        join (
            select *
            from byoc_transfer
            where id in (
                select bt.id
                from byoc_transfer bt
                join byoc_token_transactions btt on bt.id = btt.token_transfer_id

                left join etherscan_withdraws eth_w on eth_w.epoch_nonce = bt.epoch_nonce and eth_w.withdrawal_nonce = bt.nonce and $4::int2 = bt.coin_id
                left join polygon_scan_withdraws poly_w on poly_w.epoch_nonce = bt.epoch_nonce and poly_w.withdrawal_nonce = bt.nonce and $5::int2 = bt.coin_id

                -- filter on deposit_type of withdraw and coin_id of param
                where 
                    bt.deposit_type = $1 and 
                    bt.coin_id = $2 and 
                    bt.nonce is not null and 
                    coalesce(eth_w.eth_transaction_hash,poly_w.polygon_transaction_hash) is null and 
                    btt.signature is not null
                and bt.address = $3
                group by bt.id
            )
            order by id desc
        ) byt on bd.token_transfer_id = byt.id
        join block_transactions bt on bd.transaction_hash = bt.transaction_hash

        -- only need to join on withdraws
        left join (
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, withdrawal_nonce as nonce, epoch_nonce, destination, amount, coin_id, $1::int2 as deposit_type
            from (
                select *, $4::int2 as coin_id from etherscan_withdraws ew 
                union all
                select *, $5::int2 as coin_id from polygon_scan_withdraws psw
            ) w
        ) eth on eth.coin_id = byt.coin_id and eth.nonce = byt.nonce and eth.deposit_type = byt.deposit_type and
            eth.epoch_nonce = byt.epoch_nonce
    
        order by bd.withdraw_nonce desc, bt.block_timestamp asc
        "#,
        i16::from(ByocDepositTypes::Withdraw),
        i16::from(coin_id),
        &hex::decode(address)?,
        i16::from(ByocCoins::Ethereum),
        i16::from(ByocCoins::PolygonUSDC),
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

// this will return deposits pending when you know the ethereum hash.  This is used to poll the status in the bridge
pub async fn get_byoc_transfers_deposit_pending(db_pool: &DB, tx_hash: &str, coin_id: i16) -> Result<Vec<DbByoc>> {
    let db_res: Result<Vec<DbByoc>, _> = sqlx::query_as!(
        DbByocHeap,
        r#"
        select byt.id, bd.shard_id, bd.transaction_hash, byt.coin_id, byt.deposit_type, byt.nonce, byt.address, byt.amount, byt.eth_address,
            bt.block_timestamp, bt.block_hash, bt.block_level, bt.inner_cost,
            bd.withdraw_nonce, bd.epoch_nonce, bd.signature, 
            eth.eth_transaction_hash,
            eth.eth_block_number
        from byoc_token_transactions bd
        join byoc_transfer byt on bd.token_transfer_id = byt.id 
        join block_transactions bt on bd.transaction_hash = bt.transaction_hash

        join (
            select id, eth_transaction_hash, eth_block_number, eth_timestamp, nonce, address, amount, coin_id, $1::int2 as deposit_type
            from (
                select *, $4::int2 as coin_id from etherscan_deposits ed 
                union all
                select *, $5::int2 from polygon_scan_deposits psd 
            ) d
            where eth_transaction_hash = $2 and coin_id = $3
        ) eth on eth.deposit_type = byt.deposit_type and eth.coin_id = byt.coin_id and eth.nonce = byt.nonce
        "#,
        i16::from(ByocDepositTypes::Deposit),
        &hex::decode(tx_hash.trim_start_matches("0x"))?,
        coin_id,
        i16::from(ByocCoins::Ethereum),
        i16::from(ByocCoins::PolygonUSDC),
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_transactions_by_block(db_pool: &DB, shard_id: i16, block_level: i64) -> Result<Vec<DbTransactionBlock>> {
    let db_res: Result<Vec<DbTransactionBlock>, _> = sqlx::query_as!(
        DbTransactionBlockHeap,
        r#"
        select bt.id, bt.transaction_hash, bt.shard_id, bt.block_level
        from block_transactions bt
        where bt.shard_id = $1 and bt.block_level = $2
        "#,
        &shard_id,
        &block_level
    )
    .fetch_all(db_pool.inner())
    .await?
    .into_iter()
    .map(|d| d.try_into())
    .collect();

    Ok(db_res?)
}

pub async fn get_contract_public(db_pool: &DB, address: &str) -> Result<DbContractPublic> {
    let db_res = sqlx::query_as!(
        DbContractPublicHeap,
        r#"
        select id, address_contract, address_creator, tx_hash, abi, token_standard, shard_id, payload_init
        from contract_public_abi
        where address_contract = $1
        "#,
        &hex::decode(address)?
    )
    .fetch_one(db_pool.inner())
    .await?
    .try_into()?;

    Ok(db_res)
}
