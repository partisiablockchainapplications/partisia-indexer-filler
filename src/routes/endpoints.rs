use rocket::http::Status;
use rocket::serde::json::Json;
use rocket::Request;
use rocket::Shutdown;
use rocket_okapi::openapi;
use rocket_okapi::JsonSchema;
use sqlx::postgres::PgPool;

use crate::indexer::TokenStandard;

use super::structs_db::*;
use super::{pg, ResErr, ResponseJson};
type DB = rocket::State<PgPool>;

#[derive(FromForm, JsonSchema)]
pub struct Options {
    pub n: Option<i32>,
    pub id: Option<i64>,
    pub asc: Option<bool>,
    pub skip_payload: Option<bool>,
}

#[get("/shutdown")]
pub fn _shutdown(shutdown: Shutdown) -> &'static str {
    shutdown.notify();
    "Shutting down..."
}

#[catch(default)]
pub fn default_catcher(status: Status, _request: &Request) -> ResponseJson<ResErr, ResErr> {
    ResponseJson::Err(Json(ResErr {
        code: Some(status.code),
        message: status.code.to_string(),
    }))
}

#[openapi]
#[get("/ping")]
pub async fn ping() -> &'static str {
    "pong"
}
#[openapi]
#[get("/status")]
pub async fn status(db_pool: &DB) -> ResponseJson<Vec<DbStatusSchema>, ResErr> {
    match pg::get_status(db_pool).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/block/hash/<hash>")]
pub async fn block_by_hash(db_pool: &DB, hash: &str) -> ResponseJson<DbBlockSchema, ResErr> {
    match pg::get_block_by_hash(db_pool, hash).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/blocks/time/<from>/<to>")]
pub async fn blocks_by_time(db_pool: &DB, from: i64, to: i64) -> ResponseJson<Vec<DbBlockSchema>, ResErr> {
    match pg::get_blocks_by_time(db_pool, from, to).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/blocks/latest?<opt..>")]
pub async fn blocks_latest(db_pool: &DB, opt: Options) -> ResponseJson<Vec<DbBlockSchema>, ResErr> {
    match pg::get_blocks_latest(db_pool, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/blocks/count")]
pub async fn blocks_count(db_pool: &DB) -> ResponseJson<DbCount, ResErr> {
    match pg::get_count(db_pool, "block_history").await {
        Ok(expr) => ResponseJson::Ok(Json(expr)),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transaction/<tx_hash>")]
pub async fn trx_by_hash(db_pool: &DB, tx_hash: &str) -> ResponseJson<DbTransactionSchema, ResErr> {
    match pg::get_trx_by_hash(db_pool, tx_hash).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}

#[openapi]
#[get("/transactions/from/<address>?<opt..>")]
pub async fn trxs_from_address(db_pool: &DB, address: &str, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trx_from_address(db_pool, address, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transactions/to/<address>?<opt..>")]
pub async fn trxs_to_address(db_pool: &DB, address: &str, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trx_to_address(db_pool, address, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}

#[openapi]
#[get("/transactions/address/<address>?<opt..>")]
pub async fn trxs_by_address(db_pool: &DB, address: &str, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trxs_by_address(db_pool, address, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transactions/traces/<tx_base>")]
pub async fn trxs_traces_by_base(db_pool: &DB, tx_base: &str) -> ResponseJson<Vec<DbTransactionTraceSchema>, ResErr> {
    match pg::get_trxs_traces_by_base(db_pool, tx_base).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transactions/contract/<address>?<opt..>")]
pub async fn trxs_by_contract(db_pool: &DB, address: &str, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trxs_by_contract(db_pool, address, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}

#[openapi]
#[get("/byoc/address/<address>?<opt..>")]
pub async fn byoc_by_address(db_pool: &DB, address: &str, opt: Options) -> ResponseJson<Vec<DbByocSchema>, ResErr> {
    match pg::get_byoc_transfers_by_address(db_pool, address, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/byoc/tx_hash/<tx_hash>")]
pub async fn byoc_by_transaction(db_pool: &DB, tx_hash: &str) -> ResponseJson<Vec<DbByocSchema>, ResErr> {
    match pg::get_byoc_transfers_by_transaction(db_pool, tx_hash).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/byoc/withdraw_pending/<address>/<coin_id>")]
pub async fn byoc_withdraw_pending(db_pool: &DB, address: &str, coin_id: i16) -> ResponseJson<Vec<DbByocSchema>, ResErr> {
    match pg::get_byoc_transfers_withdraw_pending(db_pool, address, coin_id).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/byoc/deposit_pending/<tx_hash>/<coin_id>")]
pub async fn byoc_deposit_pending(db_pool: &DB, tx_hash: &str, coin_id: i16) -> ResponseJson<Vec<DbByocSchema>, ResErr> {
    match pg::get_byoc_transfers_deposit_pending(db_pool, tx_hash, coin_id).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transactions_by_block/<shard_id>/<block_level>")]
pub async fn transactions_by_block(db_pool: &DB, shard_id: i16, block_level: i64) -> ResponseJson<Vec<DbTransactionBlockSchema>, ResErr> {
    match pg::get_transactions_by_block(db_pool, shard_id, block_level).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/transactions/latest?<opt..>")]
pub async fn trxs_lastest(db_pool: &DB, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trxs_latest(db_pool, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
// deprecated but keep for legacy
#[openapi]
#[get("/transactions/address/erc20/<address>/<erc20>?<opt..>")]
pub async fn trxs_by_address_for_erc20(db_pool: &DB, address: &str, erc20: &str, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trxs_by_address_for_token(db_pool, address, erc20, i16::from(TokenStandard::Mpc20), &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
// deprecated but keep for legacy
#[openapi]
#[get("/erc20/<address>")]
pub async fn contract_erc20(db_pool: &DB, address: &str) -> ResponseJson<Vec<DbContractPublicLegacySchema>, ResErr> {
    match pg::get_tokens_by_address(db_pool, address, i16::from(TokenStandard::Mpc20)).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| DbContractPublicLegacy::from(d).into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}

#[openapi]
#[get("/transactions/address/tokens/<address>/<token_contract>/<token_standard>?<opt..>")]
pub async fn trxs_by_address_for_token(db_pool: &DB, address: &str, token_contract: &str, token_standard: i16, opt: Options) -> ResponseJson<Vec<DbTransactionSchema>, ResErr> {
    match pg::get_trxs_by_address_for_token(db_pool, address, token_contract, token_standard, &opt).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/contract/<address>/<token_standard>")]
pub async fn contract_token(db_pool: &DB, address: &str, token_standard: i16) -> ResponseJson<Vec<DbContractPublicSchema>, ResErr> {
    match pg::get_tokens_by_address(db_pool, address, token_standard).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into_iter().map(|d| d.into()).collect())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}

#[openapi]
#[get("/transactions/count")]
pub async fn trxs_count(db_pool: &DB) -> ResponseJson<DbCount, ResErr> {
    match pg::get_count(db_pool, "block_transactions").await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
#[openapi]
#[get("/contract_public/<address>")]
pub async fn contract_public(db_pool: &DB, address: &str) -> ResponseJson<DbContractPublicSchema, ResErr> {
    match pg::get_contract_public(db_pool, address).await {
        Ok(expr) => ResponseJson::Ok(Json(expr.into())),
        Err(e) => ResponseJson::Err(Json(ResErr { code: None, message: e.to_string() })),
    }
}
