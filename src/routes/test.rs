use prost::Message;

use super::*;
use crate::{proto_structs, web_socket::ws_open_sockets::SubscribeTypes};

#[test]
fn serialize_block() {
    let db_block = DbBlock {
        id: 1000,
        shard_id: 1,
        block_level: 3456,
        block_hash: hex_literal::hex!("4d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850"),
        block_hash_previous: hex_literal::hex!("0d389a4eef4eac8c7d347a9d69750ed84d2660184b06f1ceb04294f6c3f1fc50"),
        block_timestamp: 1640032242839,
        producer: 17,
        committee_id: 2,
        transaction_count: 50,
    };

    let ws_sender = proto_structs::WsSenderProto {
        data_type: u8::from(SubscribeTypes::Block).into(),
        sender: None,
        recipients: Vec::new(),
        data: Some(proto_structs::ws_sender_proto::Data::Block(db_block.into())),
    };
    let bytes = ws_sender.encode_to_vec();
    let hex_result =
        "0801225908e807100118801b22204d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed8502a200d389a4eef4eac8c7d347a9d69750ed84d2660184b06f1ceb04294f6c3f1fc50309799a8ccdd2f381140024832";
    assert_eq!(hex::encode(&bytes), hex_result);
    // println!("{}", hex::encode(&bytes));
}

#[test]
fn serialize_transaction() {
    let db_transaction = DbTransaction {
        id: 33111,
        transaction_hash: hex_literal::hex!("6233b77cdef13ceb6354e0260bdbe4663a863890dfa10d9209936821f5ff4aad"),
        shard_id: 2,
        block_level: 11142,
        block_hash: hex_literal::hex!("4d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850"),
        block_timestamp: 1640032242839,
        trx_signature: hex_literal::hex!("01215ce75ae2f097be5c264c124c1c28e5b88d05760279da82e706bf6c87bef62f5fe5c0a7c043e1f88ba15af34ad5d5e07eb5fd55fe77010974219cc432791f9f"),
        address_from: hex_literal::hex!("007b2a1674af4548d5e4744278d19df6f4c63881e1"),
        header_contract: hex_literal::hex!("045dbd4c13df987d7fb4450e54bcd94b34a80f2351"),
        header_payload_length: 62,
        inner_nonce: 543,
        inner_valid_to: 1640032302651,
        inner_cost: 0,
        payload_data: Some(hex_literal::hex!("000000000000000008006ab1d227a91f63fb0a2e257774ed2a179f332f220000000000000000000000000000000000000000000000000186cc6acd4b0000").to_vec()),
        is_public_contract: false,
        trace_count: 0,
        is_event_trace: false,
        has_error: false,
        error_message: None,
    };

    let ws_sender = proto_structs::WsSenderProto {
        data_type: u8::from(SubscribeTypes::Transaction).into(),
        sender: Some(db_transaction.header_contract.to_vec()),
        recipients: vec![db_transaction.header_contract.to_vec()],
        data: Some(proto_structs::ws_sender_proto::Data::Transaction(db_transaction.into())),
    };

    let bytes = ws_sender.encode_to_vec();

    let hex_result="08021215045dbd4c13df987d7fb4450e54bcd94b34a80f23511a15045dbd4c13df987d7fb4450e54bcd94b34a80f23512a910208d7820212206233b77cdef13ceb6354e0260bdbe4663a863890dfa10d9209936821f5ff4aad18022086572a204d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850309799a8ccdd2f3a4101215ce75ae2f097be5c264c124c1c28e5b88d05760279da82e706bf6c87bef62f5fe5c0a7c043e1f88ba15af34ad5d5e07eb5fd55fe77010974219cc432791f9f4215007b2a1674af4548d5e4744278d19df6f4c63881e14a15045dbd4c13df987d7fb4450e54bcd94b34a80f2351503e589f0460bbecabccdd2f7a3e000000000000000008006ab1d227a91f63fb0a2e257774ed2a179f332f220000000000000000000000000000000000000000000000000186cc6acd4b0000";
    assert_eq!(hex::encode(&bytes), hex_result);
    // println!("{}", hex::encode(&bytes));
}
