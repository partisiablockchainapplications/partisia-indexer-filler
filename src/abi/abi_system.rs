use anyhow::{ensure, Result};
use num_derive::FromPrimitive;
use std::convert::{TryFrom, TryInto};

use crate::routes::TypeFixedBytes21;

#[derive(FromPrimitive)]
#[repr(u8)]
pub enum TokenInvocation {
    Create = 0,
    Mint,
    Destroy,
    Transfer,
    Pause,
    Unpause,
    NewGuard,
    Split,
}
/* System Contracts */
// https://gitlab.com/privacyblockchain/demo/betanet/-/blob/master/integration-test/src/test/java/io/privacyblockchain/betanet/contracts/BetanetAddresses.java

pub const SIZE_BYOC_DEPOSIT: usize = 62;
pub const SIZE_BYOC_WITHDRAW: usize = 53;
pub const SIZE_BYOC_WITHDRAW_SIGNER: usize = 82;

#[derive(Default, Clone, Debug)]
pub struct PayloadByocDeposit {
    pub invocation_type: u8,
    pub nonce: u64,
    pub address: TypeFixedBytes21,
    pub amount: primitive_types::U256,
}

// 000000000000000001006ab1d227a91f63fb0a2e257774ed2a179f332f2200000000000000000000000000000000000000000000000000470de4df820000
impl TryFrom<&[u8]> for PayloadByocDeposit {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        ensure!(bytes.len() == SIZE_BYOC_DEPOSIT, "size for Transaction Inner should be {} but is {}", SIZE_BYOC_DEPOSIT, bytes.len());
        Ok(Self {
            invocation_type: bytes[0],
            nonce: u64::from_be_bytes(bytes[1..9].try_into()?),
            address: bytes[9..30].try_into()?,
            amount: primitive_types::U256::from_big_endian(&bytes[30..62]),
        })
    }
}

#[derive(Default, Clone, Debug)]
pub struct PayloadByocWithdraw {
    pub invocation_byte: u8,
    pub eth_address: [u8; 20],
    pub amount: primitive_types::U256,
}

// 009407278718290c2e0b996ed9c5f9cc2b439c043d0000000000000000000000000000000000000000000000000102bbc5490eb000
impl TryFrom<&[u8]> for PayloadByocWithdraw {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        ensure!(bytes.len() == SIZE_BYOC_WITHDRAW, "size for Transaction Inner should be {} but is {}", SIZE_BYOC_WITHDRAW, bytes.len());
        Ok(Self {
            invocation_byte: bytes[0],
            eth_address: bytes[1..21].try_into()?,
            amount: primitive_types::U256::from_big_endian(&bytes[21..]),
        })
    }
}
#[derive(Clone, Debug)]
pub struct PayloadByocWithdrawSigner {
    pub invocation_byte: u8,
    pub epoch_nonce: u64,
    pub withdraw_nonce: u64,
    pub signature: [u8; 65],
}

// 0100000000000000000000000000000000011475e3f1898ab9a807003837f61b91819d93bc3e7de1a1fc4403115effdfb7b76551ca2fe6e2459c3636bec0fdc11a022a1b4c0631f7b3880fad9b9f396e107c
impl TryFrom<&[u8]> for PayloadByocWithdrawSigner {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        ensure!(
            bytes.len() == SIZE_BYOC_WITHDRAW_SIGNER,
            "size for Transaction Inner should be {} but is {}",
            SIZE_BYOC_WITHDRAW_SIGNER,
            bytes.len()
        );
        Ok(Self {
            invocation_byte: bytes[0],
            epoch_nonce: u64::from_be_bytes(bytes[1..9].try_into()?),
            withdraw_nonce: u64::from_be_bytes(bytes[9..17].try_into()?),
            signature: bytes[17..].try_into()?,
        })
    }
}

#[derive(Clone, Debug)]
pub struct PayloadPublicDeploy {
    pub invocation_byte: u8,
    pub wasm: Vec<u8>,
    pub abi: Vec<u8>,
    pub init: Vec<u8>,
}

impl TryFrom<&[u8]> for PayloadPublicDeploy {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        let len_wasm = u32::from_be_bytes(bytes[1..5].try_into()?) as usize;
        let bytes_remaining = &bytes[5..];
        let wasm = bytes_remaining[..len_wasm].to_vec();

        ensure!(wasm.len() == len_wasm, "Invalid Length WASM");

        let bytes_remaining = &bytes_remaining[len_wasm..];
        let len_abi = u32::from_be_bytes(bytes_remaining[0..4].try_into()?) as usize;
        let bytes_remaining = &bytes_remaining[4..];
        let abi = bytes_remaining[..len_abi].to_vec();
        ensure!(abi.len() == len_abi, "Invalid Length ABI");

        let bytes_remaining = &bytes_remaining[len_abi..];
        let len_init = u32::from_be_bytes(bytes_remaining[0..4].try_into()?) as usize;
        let bytes_remaining = &bytes_remaining[4..];
        let init = bytes_remaining[..len_init].to_vec();
        ensure!(init.len() == len_init, "Invalid Length INIT");

        ensure!(
            bytes.len() == len_wasm + len_abi + len_init + 13,
            "size for Transaction Inner should be {} but is {}",
            len_wasm + len_abi + len_init + 13,
            bytes.len()
        );
        Ok(Self {
            invocation_byte: bytes[0],
            wasm,
            abi,
            init,
        })
    }
}
