use self::abi_system::{PayloadByocDeposit, PayloadByocWithdraw, PayloadByocWithdrawSigner, PayloadPublicDeploy};
use anyhow::{bail, ensure, Result};
use hex::ToHex;
use log::error;
use num_enum::IntoPrimitive;
use primitive_types::U256;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use serde_json::{json, Value};
use sqlx::{postgres::Postgres, Transaction};
use std::convert::{TryFrom, TryInto};

use crate::{
    indexer::{
        event_decoder::{get_token_standard, parse_all_addresses_from_action},
        FnKind,
    },
    routes::TypeFixedBytes21,
    SHARD_MAP,
};

pub mod abi_system;
#[cfg(test)]
mod test;

pub fn to_be_u256(u256: &U256) -> [u8; 32] {
    let mut bytes = [0; 32];
    u256.to_big_endian(bytes.as_mut());
    bytes
}

pub const SIZE_INNER: usize = 24;
pub const SIZE_HEADER: usize = 25;

#[derive(IntoPrimitive)]
#[repr(i16)]
pub enum ByocCoins {
    Ethereum = 1,
    PolygonUSDC = 2,
}

#[derive(IntoPrimitive)]
#[repr(i16)]
pub enum ByocDepositTypes {
    Deposit = 0,
    Withdraw = 1,
}

#[derive(IntoPrimitive)]
#[repr(u8)]
pub enum TransactionPayloadType {
    Deploy = 0,
    Interact = 1,
    Remove = 2,
}

#[derive(Debug, Clone, Serialize)]
pub struct TrxPayload {
    #[serde(serialize_with = "buffer_to_hex")]
    pub signature: Vec<u8>,
    pub inner: AbiTransactionInner,
    pub header: AbiTransactionHeader,
    #[serde(serialize_with = "buffer_to_hex")]
    pub payload: Vec<u8>,
}
#[derive(Debug, Clone, Serialize)]
pub struct TrxPayloadEvent {
    pub payload: Vec<u8>,
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct AbiTransactionInner {
    pub nonce: u64,
    pub valid_to: u64,
    pub cost: u64,
}
impl AbiTransactionInner {
    pub fn from_slice(bytes: &[u8]) -> Result<Self> {
        ensure!(bytes.len() == SIZE_INNER, "size for Transaction Inner should be {} but is {}", SIZE_INNER, bytes.len());
        Ok(Self {
            nonce: u64::from_be_bytes(bytes[0..8].try_into()?),
            valid_to: u64::from_be_bytes(bytes[8..16].try_into()?),
            cost: u64::from_be_bytes(bytes[16..24].try_into()?),
        })
    }
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct AbiTransactionHeader {
    #[serde(serialize_with = "buffer_to_hex")]
    pub contract: TypeFixedBytes21,
    pub payload_length: u32,
}
impl AbiTransactionHeader {
    pub fn from_slice(bytes: &[u8]) -> Result<Self> {
        ensure!(bytes.len() == SIZE_HEADER, "size for Transaction Header should be {} but is {}", SIZE_HEADER, bytes.len());
        Ok(Self {
            contract: bytes[0..21].try_into()?,
            payload_length: u32::from_be_bytes(bytes[21..25].try_into()?),
        })
    }
}

pub async fn deserialize_payload_byoc_deposit(tx_hash: &[u8], bytes: &[u8], shard_id: i16, coin_id: i16, tx: &mut Transaction<'_, Postgres>) -> Result<PayloadByocDeposit> {
    // for now just the transfer invocation
    let tkn: PayloadByocDeposit = bytes.try_into()?;
    // https://stackoverflow.com/a/62205017/7959616
    // this way avoid the unnecessary on conflict do update
    sqlx::query!(
        r#"
        with byoc as (
            insert into byoc_transfer (coin_id, deposit_type, nonce, address, amount)
            select $1, $2, $3, $4, $5
			where not exists (select from byoc_transfer where coin_id = $1 and deposit_type = $2 and nonce = $3)
            RETURNING id
        ), e as (
            select byoc.id from byoc
            union
            SELECT id FROM byoc_transfer WHERE coin_id = $1 and deposit_type = $2 and nonce = $3
        )
        insert into byoc_token_transactions (token_transfer_id, shard_id, transaction_hash, withdraw_nonce, coin_id)
        select e.id, $6, $7, $3, $1
        from e
        "#,
        coin_id,
        i16::from(ByocDepositTypes::Deposit),
        &i64::try_from(tkn.nonce)?,
        &tkn.address.to_vec(),
        &to_be_u256(&tkn.amount).to_vec(),
        shard_id,
        tx_hash,
    )
    .execute(tx)
    .await?;
    Ok(tkn)
}

pub async fn deserialize_payload_byoc_withdraw(tx_hash: &[u8], bytes: &[u8], shard_id: i16, coin_id: i16, address_from: &[u8], tx: &mut Transaction<'_, Postgres>) -> Result<PayloadByocWithdraw> {
    // for now just the transfer invocation
    let tkn: PayloadByocWithdraw = bytes.try_into()?;

    // insert without nonce; it will be null and PendingWithdraw poller will fill in properly
    sqlx::query!(
        r#"
        with byoc as (
            insert into byoc_transfer (coin_id, deposit_type, address, amount, eth_address)
            values ($1, $2, $3, $4, $5)
            RETURNING id
        )
        insert into byoc_token_transactions (token_transfer_id, shard_id, transaction_hash, coin_id)
        select byoc.id, $6, $7, $1
        from byoc
        "#,
        coin_id,
        i16::from(ByocDepositTypes::Withdraw),
        &address_from.to_vec(),
        &to_be_u256(&tkn.amount).to_vec(),
        &tkn.eth_address.as_slice().to_vec(),
        shard_id,
        tx_hash,
    )
    .execute(tx)
    .await?;
    Ok(tkn)
}
pub async fn deserialize_payload_byoc_signer(tx_hash: &[u8], bytes: &[u8], shard_id: i16, coin_id: i16, tx: &mut Transaction<'_, Postgres>) -> Result<PayloadByocWithdrawSigner> {
    // for now just the transfer invocation
    let tkn: PayloadByocWithdrawSigner = bytes.try_into()?;

    // inserts into byoc_token_transactions a signed transaction including token_transfer_id if known from the polling_pending_withdraw
    // otherwise it inserts with a null for token_transfer_id
    sqlx::query!(
        r#"
        insert into byoc_token_transactions (token_transfer_id, transaction_hash, shard_id, withdraw_nonce, signature, epoch_nonce, coin_id)
        values ( (select id from byoc_transfer where deposit_type = $5 and epoch_nonce = $6 and nonce = $3 and coin_id = $7), $1, $2, $3, $4, $6, $7)
        "#,
        tx_hash,
        shard_id,
        &i64::try_from(tkn.withdraw_nonce)?,
        &tkn.signature.to_vec(),
        i16::from(ByocDepositTypes::Withdraw),
        &i64::try_from(tkn.epoch_nonce)?,
        coin_id,
    )
    .execute(tx)
    .await?;
    Ok(tkn)
}
pub fn derive_shard_id(address: &[u8]) -> u32 {
    let shard_map = &SHARD_MAP;
    let shard_count = shard_map.len() as u32 - 1;

    let bytes: [u8; 4] = address[17..].try_into().expect("always 4 bytes");
    let i: u32 = i32::from_be_bytes(bytes).abs().try_into().expect("always allowed with abs");
    i % shard_count
}

pub async fn log_error_notify_address(tx: &mut Transaction<'_, Postgres>, tx_hash: &[u8], error_category: &str, error_params: &str, error_message: &str) -> Result<()> {
    error!("unable to decode action tx_hash: {} error: {} {}", hex::encode(tx_hash), error_category, error_message);
    // log the error
    sqlx::query!(
        r#"
        insert into abi_decode_errors (tx_hash, error_category, error_params, error_message)
        values ($1, $2, $3, $4)
        "#,
        tx_hash,
        error_category,
        error_params,
        error_message,
    )
    .execute(tx)
    .await?;
    Ok(())
}

pub async fn deserialize_payload_public_deploy(tx_hash: &[u8], bytes: &[u8], address_creator: &[u8], tx: &mut Transaction<'_, Postgres>) -> Result<()> {
    let payload_public_deploy: PayloadPublicDeploy = bytes.try_into()?;
    let PayloadPublicDeploy { abi, init, .. } = &payload_public_deploy;
    let address_contract = [&[2], &tx_hash[12..]].concat();
    let address_contract: TypeFixedBytes21 = address_contract.try_into().map_err(|_| anyhow::Error::msg("address_contract malformed, not the proper 21 bytes"))?;

    let shard_id = derive_shard_id(&address_contract) as i16;
    let token_standard = match get_token_standard(&abi) {
        Ok(v) => v,
        Err(e) => {
            log_error_notify_address(
                tx,
                tx_hash,
                "get_token_standard",
                serde_json::to_string(&json!({ "abi": hex::encode(&abi) }))?.as_str(),
                e.to_string().as_str(),
            )
            .await?;
            None
        }
    };
    // now decode the init payload
    let vec_address = match parse_all_addresses_from_action(&abi, &init, &FnKind::Init) {
        Ok(mut addresses) => {
            addresses.push(address_creator.try_into()?);
            addresses.sort();
            addresses.dedup();
            addresses
        }
        Err(e) => {
            log_error_notify_address(
                tx,
                tx_hash,
                "parse_all_addresses_from_action",
                serde_json::to_string(&json!({ "abi": hex::encode(&abi), "payload": hex::encode(&init), "fnKind": "init"}))?.as_str(),
                e.to_string().as_str(),
            )
            .await?;
            vec![address_creator.try_into()?]
        }
    };

    let vec_a = vec_address.iter().map(|a| a.to_vec()).collect::<Vec<Vec<u8>>>();
    sqlx::query!(
        r#"
        with cpa as (
            insert into contract_public_abi (address_contract, address_creator, tx_hash, abi, payload_init, shard_id, token_standard)
            values ($1, $2, $3, $4, $5, $6, $7)
            returning id
        ),
        a as (
            select UNNEST($8::bytea[]) as address
        )
        insert into contract_recipient (tx_hash, address_contract, address)
        select $9, $1, a.address
        from a, cpa
        on conflict do nothing
        "#,
        address_contract.as_slice(),
        &address_creator,
        &tx_hash,
        &abi,
        &init,
        shard_id,
        token_standard,
        vec_a.as_slice(),
        tx_hash,
    )
    .execute(tx)
    .await?;

    Ok(())
}

pub fn deserialize_transaction_normal(v: &[u8]) -> Result<TrxPayload> {
    let signature: Vec<u8> = v[..65].to_vec();

    let v_trx = &v[65..];
    let inner: AbiTransactionInner = AbiTransactionInner::from_slice(&v_trx[0..SIZE_INNER])?;
    let header: AbiTransactionHeader = AbiTransactionHeader::from_slice(&v_trx[SIZE_INNER..SIZE_INNER + SIZE_HEADER])?;
    let v_payload = v_trx[SIZE_INNER + SIZE_HEADER..].to_vec();

    ensure!(
        v_payload.len() == header.payload_length as usize,
        "got payload length of {} but expecting {}",
        v_payload.len(),
        header.payload_length
    );
    // for now always return v_payload
    Ok(TrxPayload {
        signature,
        inner,
        header,
        payload: v_payload,
    })
}

#[allow(dead_code)]
pub fn hex_to_buffer<'a, D>(d: D) -> Result<Vec<u8>, D::Error>
where
    D: Deserializer<'a>,
{
    let data = <&str>::deserialize(d)?;
    match hex::decode(data) {
        Ok(v) => Ok(v),
        Err(e) => Err(de::Error::custom(e)),
    }
}
pub fn buffer_to_hex<S>(buffer: &[u8], serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let hex_str = buffer.encode_hex::<String>();
    serializer.serialize_str(&hex_str)
}

pub fn take_value_or_bail<T>(v: &mut Value, s: &str) -> Result<T>
where
    T: de::DeserializeOwned,
{
    if let Some(x) = v.get_mut(s) {
        Ok(serde_json::from_value(x.take())?)
    } else {
        bail!("Cannot find required json value {}", s)
    }
}
