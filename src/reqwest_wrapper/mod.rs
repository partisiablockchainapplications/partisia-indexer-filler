use reqwest::Client;
use std::{
    sync::atomic::{AtomicU64, Ordering},
    time::Duration,
};

pub static REQWEST_TIMEOUT: AtomicU64 = AtomicU64::new(0);

pub struct ReqwestWrapper {
    pub client: Client,
}

impl ReqwestWrapper {
    pub fn new() -> Self {
        let reqwest_timeout = REQWEST_TIMEOUT.load(Ordering::SeqCst);
        let client = if reqwest_timeout == 0 {
            reqwest::Client::new()
        } else {
            reqwest::ClientBuilder::new()
                .timeout(Duration::from_millis(reqwest_timeout))
                .build()
                .expect("unable to build reqwest client")
        };
        Self { client }
    }
}
