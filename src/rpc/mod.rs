use anyhow::Result;
use async_trait::async_trait;

use self::rpc_structs::{RpcBlock, RpcContract, RpcTransaction, TransactionTrace};

pub mod api;
pub mod rpc_structs;
#[cfg(test)]
mod test;
#[cfg(test)]
mod test_recursive;

#[async_trait]
pub trait RpcEndpoints {
    async fn get_chain_id(&self) -> Result<String>;
    async fn get_transaction(&self, tx_hash: &str, require_final: bool, shard_name: Option<&str>) -> Result<RpcTransaction>;
    async fn get_transaction_trace(&self, tx_hash: &str, require_final: bool) -> Result<TransactionTrace>;
    async fn get_block(&self, block_num: Option<i64>) -> Result<RpcBlock>;
    async fn get_contract(&self, address_contract: &[u8], require_contract_state: bool) -> Result<RpcContract>;
    // async fn get_block_by_hash(&self, block_hash: &str, shard_name: Option<&str>) -> Result<RpcBlock>;
}
