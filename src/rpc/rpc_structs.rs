use anyhow::{ensure, Result};
use serde::{Deserialize, Serialize};

use crate::{
    abi,
    routes::{TypeFixedBytes21, TypeFixedBytes32},
    SHARD_MAP,
};

#[derive(Debug, Clone, Serialize)]
pub struct RpcStatus {
    pub shard_id: i16,
    pub chain_id: String,
    pub block_level: i64,
    #[serde(serialize_with = "abi::buffer_to_hex")]
    pub block_hash: Vec<u8>,
    #[serde(serialize_with = "abi::buffer_to_hex")]
    pub block_hash_previous: Vec<u8>,
    pub block_timestamp: i64,
    pub transaction_count: i64,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct RpcBlock {
    #[serde(rename = "blockTime")]
    pub block_level: i64,
    #[serde(rename = "identifier")]
    pub block_hash: String,
    #[serde(rename = "parentBlock")]
    pub block_hash_previous: String,
    #[serde(rename = "productionTime")]
    pub block_timestamp: i64,
    pub transactions: Vec<String>,
    pub events: Vec<String>,
    pub producer: i32,
    #[serde(rename = "committeeId")]
    pub committee_id: i16,
}

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct RpcEvent {
    pub identifier: String,
    #[serde(rename = "destinationShard", default = "default_governance")]
    pub destination_shard: String,
}

fn default_governance() -> String {
    let default_shard = "Governance".to_string();
    assert!(SHARD_MAP.get(default_shard.as_str()).is_some(), "You must have a default shard_name of {}", &default_shard);
    default_shard
}
#[derive(Deserialize, Serialize, Debug)]
pub struct RpcTransaction {
    // #[serde(serialize_with = "buffer_to_hex")]
    // #[serde(deserialize_with = "hex_to_buffer")]
    #[serde(rename = "transactionPayload")]
    pub transaction_payload: String,
    pub block: String,
    #[serde(rename = "blockTime")]
    pub block_level: i64,
    #[serde(rename = "productionTime")]
    pub block_timestamp: i64,
    pub from: Option<String>,
    pub identifier: String,
    #[serde(rename = "executionSucceeded")]
    pub execution_succeeded: bool,
    pub events: Option<Vec<RpcEvent>>,
    pub finalized: bool,
    #[serde(rename = "isEvent")]
    pub is_event: bool,
    #[serde(rename = "failureCause")]
    pub failure_cause: Option<RpcTransactionFailure>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct RpcTransactionFailure {
    #[serde(rename = "errorMessage")]
    pub error_message: String,
    // #[serde(rename = "stackTrace")]
    // pub stack_trace: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct RpcContract {
    #[serde(rename = "type")]
    pub contract_type: String,
    pub address: String,
    #[serde(rename = "jarHash")]
    pub jar_hash: String,
    #[serde(rename = "storageLength")]
    pub storage_length: u64,
    #[serde(rename = "serializedContract")]
    pub serialized_contract: Option<serde_json::Value>,
    pub abi: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct TransactionTrace {
    pub has_error: bool,
    pub transaction_payload: Vec<u8>,
    pub block_hash: TypeFixedBytes32,
    pub from: TypeFixedBytes21,
    pub tx_hash: TypeFixedBytes32,
    pub event_trace: Vec<TransactionEvent>,
    pub finalized: bool,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct TransactionEvent {
    pub tx_hash: TypeFixedBytes32,
    pub shard_id: i16,
    pub execution_succeeded: bool,
    pub error_message: Option<String>,
    pub transaction_payload: Vec<u8>,
    pub block_level: i64,
    pub trace_count: i32,
    pub block_timestamp: i64,
    pub block_hash: TypeFixedBytes32,
}

impl TryFrom<Vec<RpcTransaction>> for TransactionTrace {
    type Error = anyhow::Error;
    fn try_from(rpc: Vec<RpcTransaction>) -> Result<Self, Self::Error> {
        ensure!(rpc.len() > 0, "cannot pass in empty tx");
        let (events, _): (Vec<_>, Vec<_>) = rpc.iter().map(|t| t.events.as_ref()).partition(Option::is_some);
        let events: Vec<_> = events.into_iter().flat_map(Option::unwrap).collect();

        let mut event_trace = Vec::with_capacity(rpc.len() - 1);
        let mut has_error = false;

        // loop through and parse out the trace
        let tx_base = &rpc[0];

        for tx in &rpc[1..] {
            if !tx.execution_succeeded {
                has_error = true;
            }
            let error_message = if let Some(failure_cause) = &tx.failure_cause {
                // let s: String = std::mem::take(&mut failure_cause.error_message);
                Some(failure_cause.error_message.clone())
            } else {
                None
            };
            let from_trace = events
                .iter()
                .find(|e| e.identifier == tx.identifier)
                .expect(&format!("identifier should always be found {}", tx.identifier));
            event_trace.push(TransactionEvent {
                tx_hash: hex::decode(&tx.identifier)?.as_slice().try_into()?,
                shard_id: SHARD_MAP.get(from_trace.destination_shard.as_str()).expect("SHARD_MAP should always be found").id,
                execution_succeeded: tx.execution_succeeded,
                error_message,
                block_level: tx.block_level,
                transaction_payload: base64::decode(&tx.transaction_payload)?,
                trace_count: i32::try_from(tx.events.as_ref().unwrap_or(&Vec::new()).len())?,
                block_timestamp: tx.block_timestamp,
                block_hash: hex::decode(&tx.block)?.as_slice().try_into()?,
            })
        }

        Ok(Self {
            has_error,
            transaction_payload: base64::decode(&tx_base.transaction_payload)?,
            block_hash: hex::decode(&tx_base.block)?.as_slice().try_into()?,
            from: hex::decode(tx_base.from.as_ref().unwrap_or(&"000000000000000000000000000000000000000000".to_string()))?
                .as_slice()
                .try_into()?,
            tx_hash: hex::decode(&tx_base.identifier)?.as_slice().try_into()?,
            event_trace,
            finalized: tx_base.finalized,
        })
    }
}
