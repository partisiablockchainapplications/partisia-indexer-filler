use crate::indexer::poller::get_transactions_with_trace;
use crate::parse_config::ShardsEnv;
use crate::rpc::rpc_structs::{RpcBlock, RpcContract, RpcTransaction};
use crate::rpc::RpcEndpoints;
use crate::SHARD_MAP;
use anyhow::{bail, Result};
use async_trait::async_trait;
use rand::Rng;
use serde_json::json;
use tokio::time;

use super::rpc_structs::TransactionTrace;
struct RpcTest {}

async fn random_delay() {
    let delay = rand::thread_rng().gen_range(50..1000);
    time::sleep(time::Duration::from_millis(delay)).await;
}

pub fn init_shard_map_for_testing() {
    let vec_shards: Vec<ShardsEnv> = serde_json::from_str(
        r#"[
        {
           "id": 99,
           "name":"Governance",
           "endpoint":"https://betareader.partisiablockchain.com"
        },
        {
           "id": 0,
           "name":"Shard0",
           "endpoint":"https://betareader.partisiablockchain.com/shards/Shard0"
        },
        {
           "id": 1,
           "name":"Shard1",
           "endpoint":"https://betareader.partisiablockchain.com/shards/Shard1"
        },
        {
           "id": 2,
           "name":"Shard2",
           "endpoint":"https://betareader.partisiablockchain.com/shards/Shard2"
        }
     ]"#,
    )
    .unwrap();

    vec_shards.into_iter().for_each(|shard| {
        SHARD_MAP.insert(shard.name.clone(), shard);
    });
}

impl RpcTest {
    fn new() -> Self {
        init_shard_map_for_testing();
        Self {}
    }
    async fn get_transaction_failed(&self, tx_hash: &str, require_final: bool) -> Result<RpcTransaction> {
        random_delay().await;
        if tx_hash.len() != 64 {
            bail!("FAIL");
        }
        // → curl https://betareader.partisiablockchain.com/shards/Shard1/blockchain/transaction/5090f7dd1999d0c995a99cf2cd0b00a98f4429b5d8d6baa5a52adb97f044201d | jq
        let str_json = format!(
            r#"{{
                "transactionPayload": "AQAAAAZTaGFyZDHDFuH2yq8WqA9+8P0VvSax8SKEfsqQvcLdOB9Ku5VPxgAAarHSJ6kfY/sKLiV3dO0qF58zLyIAAAAAAAAAuQEBpAgtnVYHSezQ/6Hcqq7iwssl2IEAAAAeAwBqsdInqR9j+wouJXd07SoXnzMvIgAAAAAAmJaAAQAAAAZTaGFyZDEAAAAAAAIn9wAAAAAAAAACAAAAAAAAAAQBAA==",
                "block": "ee518bf908d0c82c8e59096baa03731957b67b0338f86fe29b219f4b2a935f44",
                "productionTime": 1666946188283,
                "blockTime": 220970,
                "identifier": "{}",
                "executionSucceeded": false,
                "finalized": {},
                "isEvent": true,
                "failureCause": {{
                  "errorMessage": "Cost (2500) exceeded maximum (185+0) set for transaction.",
                  "stackTrace": "java.lang.RuntimeException: Cost (2500) exceeded maximum (185+0) set for transaction.\n\tat com.partisiablockchain.blockchain.transaction.FeeCollector.assertWithinMaximum(FeeCollector.java:114)\n\tat com.partisiablockchain.blockchain.transaction.FeeCollector.registerCpuFee(FeeCollector.java:82)\n\tat com.partisiablockchain.blockchain.transaction.ExecutionContextTransaction.registerCpuFee(ExecutionContextTransaction.java:218)\n\tat com.partisiablockchain.blockchain.contract.binder.SysBinderContextImpl.registerCpuFee(SysBinderContextImpl.java:86)\n\tat com.partisiablockchain.binder.sys.SysContext.deductFee(SysContext.java:65)\n\tat com.partisiablockchain.binder.sys.SystemContractBinder.registerCpuAndCreateContext(SystemContractBinder.java:109)\n\tat com.partisiablockchain.binder.sys.SystemContractBinder.execute(SystemContractBinder.java:79)\n\tat com.partisiablockchain.binder.sys.SystemContractBinder.invoke(SystemContractBinder.java:94)\n\tat com.partisiablockchain.binder.sys.SystemContractBinder.invoke(SystemContractBinder.java:43)\n\tat com.partisiablockchain.blockchain.contract.binder.SysBlockchainContract.invoke(SysBlockchainContract.java:73)\n\tat com.partisiablockchain.blockchain.contract.InteractWithContractTransaction.typedInvoke(InteractWithContractTransaction.java:92)\n\tat com.partisiablockchain.blockchain.contract.InteractWithContractTransaction.execute(InteractWithContractTransaction.java:78)\n\tat com.partisiablockchain.blockchain.transaction.ExecutionContext$EventExecutor.execute(ExecutionContext.java:492)\n\tat com.partisiablockchain.blockchain.transaction.ExecutionContext.handleTransaction(ExecutionContext.java:319)\n\tat com.partisiablockchain.blockchain.transaction.ExecutionContext.execute(ExecutionContext.java:201)\n\tat com.partisiablockchain.blockchain.TransactionProcessor.processTransaction(TransactionProcessor.java:62)\n\tat com.partisiablockchain.blockchain.TransactionProcessor.processLocalEvents(TransactionProcessor.java:78)\n\tat com.partisiablockchain.blockchain.BlockchainLedger.appendValidBlock(BlockchainLedger.java:573)\n\tat com.partisiablockchain.blockchain.BlockchainLedger.floodAndAppendValidBlock(BlockchainLedger.java:499)\n\tat com.partisiablockchain.blockchain.BlockchainLedger.appendExecutableBlock(BlockchainLedger.java:401)\n\tat com.partisiablockchain.blockchain.BlockchainLedger.appendBlock(BlockchainLedger.java:386)\n\tat com.partisiablockchain.blockchain.NetworkRequestHandler.lambda$newPacket$0(NetworkRequestHandler.java:78)\n\tat com.secata.tools.thread.SharedPoolSingleThreadedExecution$SingleThreadExecutor.lambda$execute$0(SharedPoolSingleThreadedExecution.java:85)\n\tat com.secata.tools.thread.SharedPoolSingleThreadedExecution$Work.lambda$start$0(SharedPoolSingleThreadedExecution.java:147)\n\tat java.base/java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source)\n\tat java.base/java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)\n\tat java.base/java.lang.Thread.run(Unknown Source)\n"
                }}
            }}"#,
            tx_hash, require_final
        );
        Ok(serde_json::from_str(&str_json)?)
    }
    async fn get_contract(&self, address_contract: &[u8], require_contract_state: bool) -> Result<RpcContract> {
        random_delay().await;

        let json = if require_contract_state {
            json!({
                "type": "PUBLIC",
                "address": hex::encode(address_contract),
                "jarHash": "cb38a8a8fedaf5eedb8a578f736498a0d1bceb0cb6cbcae0762b0f19238a8c3d",
                "storageLength": 209192,
                "serializedContract": {
                    "state": {
                    "data": "CgAAAFRlc3QgVG9rZW4DAAAAUVdFBADh9QUAAAAAAAAAAAAAAAAAAgAAAACn5BWXaR1KRrGHGm2Csyyagym1Y6CGAQAAAAAAAAAAAAAAAAAAtI/8XSdXRLFLYJ+AGAxE73eZKqxgWvQFAAAAAAAAAAAAAAAAAAAAAAUAAABtcGMyMAUAAAAxLjAuMA=="
                    }
                },
                "abi": "UEJDQUJJBwAABAEAAAAADwAAAApUb2tlblN0YXRlAAAAAgAAAAVtcGMyMAALAAAAB3ZlcnNpb24ADgAAAA5Jbml0aWFsQmFsYW5jZQAAAAIAAAAHYWRkcmVzcw0AAAAGYW1vdW50BQAAAAxNcGMyMEluaXRNc2cAAAADAAAABGluZm8ADAAAABBpbml0aWFsX2JhbGFuY2VzDgABAAAABm1pbnRlchIADQAAAAtUcmFuc2Zlck1zZwAAAAIAAAACdG8NAAAABmFtb3VudAUAAAAPVHJhbnNmZXJGcm9tTXNnAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQAAAApBcHByb3ZlTXNnAAAAAgAAAAdzcGVuZGVyDQAAAAZhbW91bnQFAAAAB01pbnRNc2cAAAACAAAACXJlY2lwaWVudA0AAAAGYW1vdW50BQAAAAdCdXJuTXNnAAAAAQAAAAZhbW91bnQFAAAAC0J1cm5Gcm9tTXNnAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQAAABRJbmNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABREZWNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABJNUEMyMENvbnRyYWN0U3RhdGUAAAAFAAAABGluZm8ADAAAAAx0b3RhbF9zdXBwbHkFAAAABm1pbnRlchIADQAAAAhiYWxhbmNlcw8NBQAAAAphbGxvd2FuY2VzDw0PDQUAAAAJVG9rZW5JbmZvAAAAAwAAAARuYW1lCwAAAAZzeW1ib2wLAAAACGRlY2ltYWxzAQAAAAZNaW50ZXIAAAACAAAABm1pbnRlcg0AAAAIY2FwYWNpdHkSBQAAABNDb250cmFjdFZlcnNpb25CYXNlAAAAAgAAAARuYW1lCwAAAAd2ZXJzaW9uCwAAAAkBAAAACmluaXRpYWxpemX/////DwAAAAEAAAADbXNnAAICAAAACHRyYW5zZmVyAQAAAAIAAAACdG8NAAAABmFtb3VudAUCAAAADXRyYW5zZmVyX2Zyb20DAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQIAAAAHYXBwcm92ZQUAAAACAAAAB3NwZW5kZXINAAAABmFtb3VudAUCAAAABG1pbnQHAAAAAgAAAAlyZWNpcGllbnQNAAAABmFtb3VudAUCAAAABGJ1cm4JAAAAAQAAAAZhbW91bnQFAgAAAAlidXJuX2Zyb20RAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQIAAAASaW5jcmVhc2VfYWxsb3dhbmNlEwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQIAAAASZGVjcmVhc2VfYWxsb3dhbmNlFQAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAA",
            })
        } else {
            json!({
                "type": "PUBLIC",
                "address": hex::encode(address_contract),
                "jarHash": "cb38a8a8fedaf5eedb8a578f736498a0d1bceb0cb6cbcae0762b0f19238a8c3d",
                "storageLength": 209192,
                "serializedContract": null,
                "abi": "UEJDQUJJBwAABAEAAAAADwAAAApUb2tlblN0YXRlAAAAAgAAAAVtcGMyMAALAAAAB3ZlcnNpb24ADgAAAA5Jbml0aWFsQmFsYW5jZQAAAAIAAAAHYWRkcmVzcw0AAAAGYW1vdW50BQAAAAxNcGMyMEluaXRNc2cAAAADAAAABGluZm8ADAAAABBpbml0aWFsX2JhbGFuY2VzDgABAAAABm1pbnRlchIADQAAAAtUcmFuc2Zlck1zZwAAAAIAAAACdG8NAAAABmFtb3VudAUAAAAPVHJhbnNmZXJGcm9tTXNnAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQAAAApBcHByb3ZlTXNnAAAAAgAAAAdzcGVuZGVyDQAAAAZhbW91bnQFAAAAB01pbnRNc2cAAAACAAAACXJlY2lwaWVudA0AAAAGYW1vdW50BQAAAAdCdXJuTXNnAAAAAQAAAAZhbW91bnQFAAAAC0J1cm5Gcm9tTXNnAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQAAABRJbmNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABREZWNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABJNUEMyMENvbnRyYWN0U3RhdGUAAAAFAAAABGluZm8ADAAAAAx0b3RhbF9zdXBwbHkFAAAABm1pbnRlchIADQAAAAhiYWxhbmNlcw8NBQAAAAphbGxvd2FuY2VzDw0PDQUAAAAJVG9rZW5JbmZvAAAAAwAAAARuYW1lCwAAAAZzeW1ib2wLAAAACGRlY2ltYWxzAQAAAAZNaW50ZXIAAAACAAAABm1pbnRlcg0AAAAIY2FwYWNpdHkSBQAAABNDb250cmFjdFZlcnNpb25CYXNlAAAAAgAAAARuYW1lCwAAAAd2ZXJzaW9uCwAAAAkBAAAACmluaXRpYWxpemX/////DwAAAAEAAAADbXNnAAICAAAACHRyYW5zZmVyAQAAAAIAAAACdG8NAAAABmFtb3VudAUCAAAADXRyYW5zZmVyX2Zyb20DAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQIAAAAHYXBwcm92ZQUAAAACAAAAB3NwZW5kZXINAAAABmFtb3VudAUCAAAABG1pbnQHAAAAAgAAAAlyZWNpcGllbnQNAAAABmFtb3VudAUCAAAABGJ1cm4JAAAAAQAAAAZhbW91bnQFAgAAAAlidXJuX2Zyb20RAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQIAAAASaW5jcmVhc2VfYWxsb3dhbmNlEwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQIAAAASZGVjcmVhc2VfYWxsb3dhbmNlFQAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAA",
            })
        };
        Ok(serde_json::from_value(json)?)
    }
}
#[async_trait]
impl RpcEndpoints for RpcTest {
    async fn get_chain_id(&self) -> Result<String> {
        random_delay().await;
        Ok("PARTISIA beta net (0.19)".to_string())
    }
    async fn get_transaction(&self, tx_hash: &str, require_final: bool, _shard_name: Option<&str>) -> Result<RpcTransaction> {
        random_delay().await;
        if tx_hash.len() != 64 {
            bail!("FAIL");
        }
        // → curl https://betareader.partisiablockchain.com/shards/Shard1/blockchain/transaction/c316e1f6caaf16a80f7ef0fd15bd26b1f122847eca90bdc2dd381f4abb954fc6 | jq
        let str_json = format!(
            r#"{{
                "transactionPayload": "ACh13oxl8I/Sa01KAGkfVL0XADjSs+/ZXxamrRkxZ3Y0brPJnn1jS240gHuA02qGLP2N0WPIzbvy7claIk6QJJMAAAAAAAAAOgAAAYCWA7+mAAAAAAAABKEBpAgtnVYHSezQ/6Hcqq7iwssl2IEAAAAeAwBqsdInqR9j+wouJXd07SoXnzMvIgAAAAAAmJaA",
                "block": "ee518bf908d0c82c8e59096baa03731957b67b0338f86fe29b219f4b2a935f44",
                "productionTime": 1666946188283,
                "blockTime": 220970,
                "identifier": "{}",
                "executionSucceeded": true,
                "events": [
                  {{
                    "identifier": "5090f7dd1999d0c995a99cf2cd0b00a98f4429b5d8d6baa5a52adb97f044201d",
                    "destinationShard": "Shard1"
                  }}
                ],
                "finalized": {},
                "isEvent": false
              }}"#,
            tx_hash, require_final
        );
        Ok(serde_json::from_str(&str_json)?)
    }
    // TODO need to rework some of these tests to be more clear
    async fn get_transaction_trace(&self, tx_hash: &str, require_final: bool) -> Result<TransactionTrace> {
        if tx_hash.len() != 64 {
            bail!("FAIL");
        }
        let tx_hash_success = "c316e1f6caaf16a80f7ef0fd15bd26b1f122847eca90bdc2dd381f4abb954fc6";
        let tx_hash_failure = "5090f7dd1999d0c995a99cf2cd0b00a98f4429b5d8d6baa5a52adb97f044201d";
        let tx_success: RpcTransaction = self.get_transaction(tx_hash_success, require_final, None).await?;
        let tx_failure: RpcTransaction = self.get_transaction_failed(tx_hash_failure, require_final).await?;

        let vec_tx = vec![tx_success, tx_failure];
        Ok(vec_tx.try_into().unwrap())
    }
    async fn get_block(&self, block_num: Option<i64>) -> Result<RpcBlock> {
        random_delay().await;
        // → curl https://betareader.partisiablockchain.com/shards/Shard0/blockchain/blocks/8da2c01aada58b3fddc99728f4ee5680b190563ede8c8be3fb2ecea01f6d91e3 | jq
        let str_json = format!(
            r#"{{
            "parentBlock": "5e1ec9d4a2dd3f5e1c2d23467141cc9422edf0d5269090362c05ffd533029f71",
            "blockTime": {},
            "committeeId": 1,
            "productionTime": 1628712037939,
            "transactions": [
                "c326ceb1cc4869e3260b8f1db6c8f41a9fcdeca6024bb196645ff7c459c632bd"
            ],
            "events": [],
            "identifier": "8da2c01aada58b3fddc99728f4ee5680b190563ede8c8be3fb2ecea01f6d91e3",
            "state": "cd08b1cd3406647cee90e1d370a014bdea8516f242953554153a14c052a66413",
            "producer": 17
            }}"#,
            block_num.unwrap_or(1000)
        );
        Ok(serde_json::from_str(&str_json)?)
    }

    async fn get_contract(&self, _address_contract: &[u8], _require_contract_state: bool) -> Result<RpcContract> {
        unimplemented!()
    }
}

#[tokio::test]
async fn failed_rpc_fetch() {
    let x = RpcTest::new().get_transaction("", true, None).await;
    assert!(x.is_err());
}
#[tokio::test]
async fn get_block_async() {
    let block_level = 12345;
    let res = RpcTest::new().get_block(Some(block_level)).await;
    assert!(res.is_ok());
    let block: RpcBlock = res.unwrap();
    assert_eq!(block.block_level, block_level);
}
#[tokio::test]
async fn get_transaction_async() {
    let tx_hash = "c326ceb1cc4869e3260b8f1db6c8f41a9fcdeca6024bb196645ff7c459c632bd";
    let require_final = true;
    let res = RpcTest::new().get_transaction(tx_hash, require_final, None).await;
    assert!(res.is_ok());
    let trx: RpcTransaction = res.unwrap();
    assert_eq!(trx.identifier, tx_hash);
    assert_eq!(trx.finalized, require_final);
}
#[tokio::test]
async fn get_contract() {
    let address_contract = hex_literal::hex!("027eef2ab47f75c63dfb567f6289a69c118fa58c04");
    let res = RpcTest::new().get_contract(address_contract.as_slice(), false).await;
    assert!(res.is_ok());
    let contract: RpcContract = res.unwrap();
    assert_eq!(contract.serialized_contract, None);
    assert_eq!(contract.contract_type, "PUBLIC");
    assert_eq!(contract.abi, "UEJDQUJJBwAABAEAAAAADwAAAApUb2tlblN0YXRlAAAAAgAAAAVtcGMyMAALAAAAB3ZlcnNpb24ADgAAAA5Jbml0aWFsQmFsYW5jZQAAAAIAAAAHYWRkcmVzcw0AAAAGYW1vdW50BQAAAAxNcGMyMEluaXRNc2cAAAADAAAABGluZm8ADAAAABBpbml0aWFsX2JhbGFuY2VzDgABAAAABm1pbnRlchIADQAAAAtUcmFuc2Zlck1zZwAAAAIAAAACdG8NAAAABmFtb3VudAUAAAAPVHJhbnNmZXJGcm9tTXNnAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQAAAApBcHByb3ZlTXNnAAAAAgAAAAdzcGVuZGVyDQAAAAZhbW91bnQFAAAAB01pbnRNc2cAAAACAAAACXJlY2lwaWVudA0AAAAGYW1vdW50BQAAAAdCdXJuTXNnAAAAAQAAAAZhbW91bnQFAAAAC0J1cm5Gcm9tTXNnAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQAAABRJbmNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABREZWNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABJNUEMyMENvbnRyYWN0U3RhdGUAAAAFAAAABGluZm8ADAAAAAx0b3RhbF9zdXBwbHkFAAAABm1pbnRlchIADQAAAAhiYWxhbmNlcw8NBQAAAAphbGxvd2FuY2VzDw0PDQUAAAAJVG9rZW5JbmZvAAAAAwAAAARuYW1lCwAAAAZzeW1ib2wLAAAACGRlY2ltYWxzAQAAAAZNaW50ZXIAAAACAAAABm1pbnRlcg0AAAAIY2FwYWNpdHkSBQAAABNDb250cmFjdFZlcnNpb25CYXNlAAAAAgAAAARuYW1lCwAAAAd2ZXJzaW9uCwAAAAkBAAAACmluaXRpYWxpemX/////DwAAAAEAAAADbXNnAAICAAAACHRyYW5zZmVyAQAAAAIAAAACdG8NAAAABmFtb3VudAUCAAAADXRyYW5zZmVyX2Zyb20DAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQIAAAAHYXBwcm92ZQUAAAACAAAAB3NwZW5kZXINAAAABmFtb3VudAUCAAAABG1pbnQHAAAAAgAAAAlyZWNpcGllbnQNAAAABmFtb3VudAUCAAAABGJ1cm4JAAAAAQAAAAZhbW91bnQFAgAAAAlidXJuX2Zyb20RAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQIAAAASaW5jcmVhc2VfYWxsb3dhbmNlEwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQIAAAASZGVjcmVhc2VfYWxsb3dhbmNlFQAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAA");
}
#[tokio::test]
async fn get_contract_with_state() {
    let address_contract = hex_literal::hex!("027eef2ab47f75c63dfb567f6289a69c118fa58c04");
    let res = RpcTest::new().get_contract(address_contract.as_slice(), true).await;
    assert!(res.is_ok());
    let contract: RpcContract = res.unwrap();
    assert_eq!(contract.serialized_contract.is_some(), true);
    assert_eq!(contract.contract_type, "PUBLIC");
    assert_eq!(contract.abi, "UEJDQUJJBwAABAEAAAAADwAAAApUb2tlblN0YXRlAAAAAgAAAAVtcGMyMAALAAAAB3ZlcnNpb24ADgAAAA5Jbml0aWFsQmFsYW5jZQAAAAIAAAAHYWRkcmVzcw0AAAAGYW1vdW50BQAAAAxNcGMyMEluaXRNc2cAAAADAAAABGluZm8ADAAAABBpbml0aWFsX2JhbGFuY2VzDgABAAAABm1pbnRlchIADQAAAAtUcmFuc2Zlck1zZwAAAAIAAAACdG8NAAAABmFtb3VudAUAAAAPVHJhbnNmZXJGcm9tTXNnAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQAAAApBcHByb3ZlTXNnAAAAAgAAAAdzcGVuZGVyDQAAAAZhbW91bnQFAAAAB01pbnRNc2cAAAACAAAACXJlY2lwaWVudA0AAAAGYW1vdW50BQAAAAdCdXJuTXNnAAAAAQAAAAZhbW91bnQFAAAAC0J1cm5Gcm9tTXNnAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQAAABRJbmNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABREZWNyZWFzZUFsbG93YW5jZU1zZwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAAABJNUEMyMENvbnRyYWN0U3RhdGUAAAAFAAAABGluZm8ADAAAAAx0b3RhbF9zdXBwbHkFAAAABm1pbnRlchIADQAAAAhiYWxhbmNlcw8NBQAAAAphbGxvd2FuY2VzDw0PDQUAAAAJVG9rZW5JbmZvAAAAAwAAAARuYW1lCwAAAAZzeW1ib2wLAAAACGRlY2ltYWxzAQAAAAZNaW50ZXIAAAACAAAABm1pbnRlcg0AAAAIY2FwYWNpdHkSBQAAABNDb250cmFjdFZlcnNpb25CYXNlAAAAAgAAAARuYW1lCwAAAAd2ZXJzaW9uCwAAAAkBAAAACmluaXRpYWxpemX/////DwAAAAEAAAADbXNnAAICAAAACHRyYW5zZmVyAQAAAAIAAAACdG8NAAAABmFtb3VudAUCAAAADXRyYW5zZmVyX2Zyb20DAAAAAwAAAARmcm9tDQAAAAJ0bw0AAAAGYW1vdW50BQIAAAAHYXBwcm92ZQUAAAACAAAAB3NwZW5kZXINAAAABmFtb3VudAUCAAAABG1pbnQHAAAAAgAAAAlyZWNpcGllbnQNAAAABmFtb3VudAUCAAAABGJ1cm4JAAAAAQAAAAZhbW91bnQFAgAAAAlidXJuX2Zyb20RAAAAAgAAAAVvd25lcg0AAAAGYW1vdW50BQIAAAASaW5jcmVhc2VfYWxsb3dhbmNlEwAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQIAAAASZGVjcmVhc2VfYWxsb3dhbmNlFQAAAAIAAAAHc3BlbmRlcg0AAAAGYW1vdW50BQAA");
}
#[tokio::test]
async fn get_transaction_failed_async() {
    let tx_hash = "c326ceb1...";
    let require_final = true;
    let res = RpcTest::new().get_transaction_failed(tx_hash, require_final).await;
    assert!(res.is_err());
}
#[tokio::test]
async fn get_transaction_trace_async() {
    let require_final = true;
    let res = RpcTest::new()
        .get_transaction_trace("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", require_final)
        .await;
    assert!(res.is_ok());
    let trx: TransactionTrace = res.unwrap();
    assert!(trx.has_error);
}
#[tokio::test]
async fn transaction_batch_fetch() {
    let rpc = RpcTest::new();
    let transactions: Vec<&str> = vec![
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaac",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaad",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaae",
    ];
    let now = tokio::time::Instant::now();
    let res = get_transactions_with_trace(&rpc, &transactions).await;
    println!("get_transactions time: {}", now.elapsed().as_millis());
    let trx: Vec<TransactionTrace> = res.unwrap();
    let trx_hashes: Vec<[u8; 32]> = trx.iter().map(|t| t.tx_hash).collect();
    assert_eq!(trx.len(), transactions.len());
    for t in trx {
        let hex = hex::encode(t.tx_hash);
        println!("t: {}", hex);
        assert!(trx_hashes.contains(&t.tx_hash));
    }
}
#[tokio::test]
async fn transaction_batch_fetch_fail() {
    let rpc = RpcTest::new();
    let transactions: Vec<&str> = vec![
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaac",
        "", // <-- fails here
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaae",
    ];
    let res = get_transactions_with_trace(&rpc, &transactions).await;

    // If one fetch fails the whole thing should fail
    assert!(res.is_err());
    // single error
    assert_eq!(res.unwrap_err().to_string(), "FAIL");
}
