use anyhow::{bail, ensure, Result};
use async_trait::async_trait;
use reqwest::StatusCode;
use serde_json::Value;
use std::borrow::Cow;
use url::Url;

use super::rpc_structs::{RpcBlock, RpcContract, RpcTransaction, TransactionTrace};
use crate::{
    abi::{self, derive_shard_id},
    indexer::poller::get_recursive,
    reqwest_wrapper::ReqwestWrapper,
    rpc::RpcEndpoints,
    SHARD_MAP, SHARD_MAP_ID,
};

fn get_url_from_shard(shard_name: &str, path: &str) -> Result<Url> {
    let shard_map = &SHARD_MAP;
    if let Some(shard) = shard_map.get(shard_name) {
        Ok(append_path(&shard.endpoint, path))
    } else {
        bail!("Cannot find shard_name: {}", shard_name)
    }
}
fn get_url_from_shard_id(shard_id: i16, path: &str) -> Result<Url> {
    let shard_map = &SHARD_MAP_ID;
    if let Some(shard) = shard_map.get(&shard_id) {
        Ok(append_path(&shard.endpoint, path))
    } else {
        bail!("Cannot find shard_id: {}", shard_id)
    }
}

#[derive(Debug)]
pub struct Rpc {
    shard_name: Cow<'static, str>,
}
impl Rpc {
    pub fn new(shard_name: Cow<'static, str>) -> Self {
        Self { shard_name }
    }
}

#[async_trait]
impl RpcEndpoints for Rpc {
    async fn get_chain_id(&self) -> Result<String> {
        let url: Url = get_url_from_shard(&self.shard_name, "blockchain/chainId")?;
        let client = ReqwestWrapper::new().client;
        let resp = client.get(url.as_str()).send().await?;
        match resp.status() {
            StatusCode::OK => {
                let mut v: Value = resp.json().await?;
                Ok(abi::take_value_or_bail(&mut v, "chainId")?)
            }
            e => {
                // The only time get_chain_id is called is in the init to validate
                // If this fails for any endpoint the program will panic
                error!("INVALID ENDPOINT: {}", url.as_ref());
                bail!(e)
            }
        }
    }
    async fn get_transaction(&self, tx_hash: &str, require_final: bool, shard_name: Option<&str>) -> Result<RpcTransaction> {
        let shard_name = shard_name.unwrap_or(&self.shard_name);
        let mut url: Url = get_url_from_shard(shard_name, &format!("blockchain/transaction/{}", tx_hash))?;
        url.set_query(Some(&format!("requireFinal={}", require_final)));
        let client = ReqwestWrapper::new().client;
        let res = client.get(url.as_str()).send().await?.text().await?;
        ensure!(res.len() > 0, "Cannot find tx_hash: {}", tx_hash);
        Ok(serde_json::from_str(&res)?)
    }

    async fn get_transaction_trace(&self, tx_hash: &str, require_final: bool) -> Result<TransactionTrace> {
        let tx_base = self.get_transaction(tx_hash, require_final, None).await?;
        let mut vec_trace: Vec<RpcTransaction> = Vec::new();

        // loop through all transactions in the trace
        if let Some(events) = &tx_base.events {
            for event in events {
                let tx = self.get_transaction(&event.identifier, true, Some(&event.destination_shard)).await?;
                get_recursive(self, &tx, &mut vec_trace).await?;
                vec_trace.push(tx);
            }
        }

        // insert the base tx at the beginning
        vec_trace.insert(0, tx_base);
        Ok(vec_trace.try_into()?)
    }
    async fn get_block(&self, block_num: Option<i64>) -> Result<RpcBlock> {
        let url: Url = if let Some(num) = block_num {
            get_url_from_shard(&self.shard_name, &format!("blockchain/blocks/blockTime/{}", num))?
        } else {
            get_url_from_shard(&self.shard_name, "blockchain/blocks/latest")?
        };
        let client = ReqwestWrapper::new().client;
        let resp = client.get(url.as_str()).send().await?;
        Ok(resp.json().await?)
    }
    async fn get_contract(&self, address_contract: &[u8], require_contract_state: bool) -> Result<RpcContract> {
        let shard_id = derive_shard_id(address_contract) as i16;
        let url = get_url_from_shard_id(shard_id, &format!("blockchain/contracts/{}", hex::encode(address_contract)))?;

        let client = ReqwestWrapper::new().client;
        let resp = client.get(url.as_str()).query(&[("requireContractState", require_contract_state)]).send().await?;

        Ok(resp.json().await?)
    }
}

fn append_path(endpoint: &Url, append: &str) -> Url {
    let mut url = endpoint.clone();
    if url.path() == "/" {
        url.set_path(append);
    } else {
        url.set_path(&format!("{}/{}", endpoint.path(), append));
    }
    url
}
