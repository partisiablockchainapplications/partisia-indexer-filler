use anyhow::Result;
use async_trait::async_trait;

use crate::indexer::poller::get_recursive;
use crate::rpc::rpc_structs::{RpcBlock, RpcTransaction};
use crate::rpc::RpcEndpoints;

use super::rpc_structs::{RpcContract, TransactionTrace};
use super::test::init_shard_map_for_testing;
struct RpcRecursionTest {}
impl RpcRecursionTest {
    fn new() -> Self {
        init_shard_map_for_testing();
        Self {}
    }
}
#[async_trait]
impl RpcEndpoints for RpcRecursionTest {
    async fn get_chain_id(&self) -> Result<String> {
        unimplemented!()
    }

    async fn get_transaction(&self, tx_hash: &str, _require_final: bool, _shard_name: Option<&str>) -> Result<RpcTransaction> {
        match tx_hash {
            "0000000000000000000000000000000000000000000000000000000000000001" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000001",
                    "executionSucceeded": true,
                    "events": [
                      {
                        "identifier": "0000000000000000000000000000000000000000000000000000000000000002",
                        "destinationShard": "Shard1"
                      },
                      {
                        "identifier": "0000000000000000000000000000000000000000000000000000000000000003",
                        "destinationShard": "Shard1"
                      }
                    ],
                    "finalized": true,
                    "isEvent": false
                  }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            "0000000000000000000000000000000000000000000000000000000000000002" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000002",
                    "executionSucceeded": true,
                    "events": [
                        {
                        "identifier": "0000000000000000000000000000000000000000000000000000000000000004",
                        "destinationShard": "Shard1"
                        }
                    ],
                    "finalized": true,
                    "isEvent": false
                    }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            "0000000000000000000000000000000000000000000000000000000000000003" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000003",
                    "executionSucceeded": true,
                    "events": [
                        {
                        "identifier": "0000000000000000000000000000000000000000000000000000000000000005",
                        "destinationShard": "Shard1"
                        },
                        {
                        "identifier": "0000000000000000000000000000000000000000000000000000000000000011",
                        "destinationShard": "Shard1"
                        }
                    ],
                    "finalized": true,
                    "isEvent": false
                    }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            "0000000000000000000000000000000000000000000000000000000000000004" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000004",
                    "executionSucceeded": true,
                    "finalized": true,
                    "isEvent": false
                    }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            "0000000000000000000000000000000000000000000000000000000000000005" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000005",
                    "executionSucceeded": true,
                    "finalized": true,
                    "isEvent": false
                    }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            "0000000000000000000000000000000000000000000000000000000000000011" => {
                let str_json = r#"{
                    "transactionPayload": "",
                    "block": "0000000000000000000000000000000000000000000000000000000000000000",
                    "productionTime": 1666946188283,
                    "blockTime": 220970,
                    "identifier": "0000000000000000000000000000000000000000000000000000000000000011",
                    "executionSucceeded": false,
                    "finalized": true,
                    "isEvent": true,
                    "failureCause": {
                        "errorMessage": "Cost (2500) exceeded maximum (185+0) set for transaction."
                    }
                }"#;
                Ok(serde_json::from_str(str_json)?)
            }
            _ => unreachable!(),
        }
    }

    async fn get_transaction_trace(&self, _tx_hash: &str, _require_final: bool) -> Result<TransactionTrace> {
        unimplemented!()
    }

    async fn get_block(&self, _block_num: Option<i64>) -> Result<RpcBlock> {
        unimplemented!()
    }
    async fn get_contract(&self, _address_contract: &[u8], _require_contract_state: bool) -> Result<RpcContract> {
        unimplemented!()
    }
}
#[tokio::test]
async fn test_recursive() {
    let rpc = &RpcRecursionTest::new();
    let tx_base = rpc.get_transaction("0000000000000000000000000000000000000000000000000000000000000001", true, None).await.unwrap();
    let mut vec_trace: Vec<RpcTransaction> = Vec::new();

    if let Some(events) = &tx_base.events {
        for event in events {
            let tx = rpc.get_transaction(&event.identifier, true, Some(&event.destination_shard)).await.unwrap();
            get_recursive(rpc, &tx, &mut vec_trace).await.unwrap();
            vec_trace.push(tx);
        }
    }

    vec_trace.insert(0, tx_base);

    // println!("{}", serde_json::to_string_pretty(&vec_trace).unwrap());
    let trace: TransactionTrace = vec_trace.try_into().unwrap();
    // println!("{}", serde_json::to_string_pretty(&trace).unwrap());
    assert_eq!(trace.has_error, true);
    assert_eq!(trace.event_trace.len(), 5);
    let failure = trace.event_trace.iter().find(|t| t.execution_succeeded == false).unwrap();
    assert_eq!(hex::encode(failure.tx_hash), "0000000000000000000000000000000000000000000000000000000000000011");
}
