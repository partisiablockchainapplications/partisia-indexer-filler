use lazy_static::lazy_static;
use rand::{thread_rng, Rng};
use std::{env, error::Error, fmt, fs, path::Path, process::Command};

lazy_static! {
    // the maximum seems to be around 130_000
    static ref MAX_ARG_LENGTH: usize = env::var("MAX_ARG_LENGTH").unwrap_or("100000".to_string()).parse().expect("MAX_ARG_LENGTH must be valid usize");
}

#[derive(Debug)]
pub enum ErrorAbiClient {
    Stderr(String),
    BinaryFailedToRun(String),
    Shell(String),
    UnkownExit(String),
    NoPermission,
}

impl fmt::Display for ErrorAbiClient {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ErrorAbiClient::Stderr(ref s) => write!(f, "Stderr: {}", s),
            ErrorAbiClient::BinaryFailedToRun(ref s) => write!(f, "BinaryFailedToRun: {}", s),
            ErrorAbiClient::Shell(ref s) => write!(f, "Shell: {}", s),
            ErrorAbiClient::UnkownExit(ref s) => write!(f, "UnkownExit: {}", s),
            ErrorAbiClient::NoPermission => write!(f, "NoPermission"),
        }
    }
}

impl Error for ErrorAbiClient {}

pub struct CmdArg<'a> {
    pub name: &'a str,
    pub value: &'a str,
}

fn get_cmd() -> Result<Command, ErrorAbiClient> {
    let cmd = if let Ok(path_abi_client_bin) = env::var("PATH_ABI_CLIENT_BIN") {
        Command::new(path_abi_client_bin)
    } else {
        let current_dir = match env::current_dir() {
            Ok(current_dir) => current_dir,
            Err(_) => return Err(ErrorAbiClient::NoPermission),
        };

        // Construct the relative path to the command
        let relative_path = Path::join(&current_dir, "abi-client-bin");
        Command::new(relative_path)
    };
    Ok(cmd)
}

pub fn run_abi_client_bin(args: Vec<CmdArg>) -> Result<Vec<u8>, ErrorAbiClient> {
    // create a Command object for the binary
    // use the PATH_ABI_CLIENT_BIN env variable or set to current dir
    let mut cmd = get_cmd()?;

    let max_arg_length = *MAX_ARG_LENGTH;
    for arg in &args {
        let mut idx = 0;
        while (idx * max_arg_length) < arg.value.len() {
            cmd.arg(arg.name).arg(&arg.value[idx * max_arg_length..std::cmp::min((idx + 1) * max_arg_length, arg.value.len())]);
            idx += 1;
        }
    }

    let mut res = cmd.output();
    if let Err(ref err) = res {
        // should use kind but it is not stable yet
        // if res.unwrap_err().kind() == std::io::ErrorKind::ArgumentListTooLong {
        if format!("{:?}", err.kind()) == "ArgumentListTooLong" {
            // try again as file
            let mut cmd = get_cmd()?;

            // build the args into a string. Its probably better to write directly into the file but this is fine for now
            let mut s = "".to_string();
            for arg in &args {
                s.extend(" ".chars());
                s.extend(arg.name.chars());
                s.extend(" ".chars());
                s.extend(arg.value.chars());
            }

            let mut rng = thread_rng();
            let random_file_name = rng.gen::<u128>().to_string();

            fs::write(&random_file_name, s).map_err(|e| ErrorAbiClient::BinaryFailedToRun(e.to_string()))?;

            cmd.arg("--from_file");
            cmd.arg(&random_file_name);
            res = cmd.output();
        }
    };

    match res {
        Ok(output) => {
            if !output.status.success() {
                match String::from_utf8(output.stderr) {
                    Ok(s) => Err(ErrorAbiClient::Shell(s)),
                    Err(_) => Err(ErrorAbiClient::UnkownExit("program output was not valid utf-8".to_string())),
                }
            } else {
                // print the output of the binary
                if !output.stderr.is_empty() {
                    Err(ErrorAbiClient::Stderr(String::from_utf8_lossy(&output.stderr).into()))
                } else {
                    Ok(output.stdout)
                }
            }
        }
        Err(e) => {
            // println!("err {:?}, {}", e.kind(), std::io::ErrorKind::ArgumentListTooLong.to_string());
            Err(ErrorAbiClient::BinaryFailedToRun(e.to_string()))
        }
    }
}
#[cfg(test)]
mod test;
