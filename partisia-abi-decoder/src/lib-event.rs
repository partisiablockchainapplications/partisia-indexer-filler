//! This is a smart contract to use for transaction and event deserialization.
//! There is no logic implemented in this contract, only the structure of the transactions and events.
//! The java deserialization implementation using this contract can be found [here](https://gitlab.com/secata/pbc/language/abi-client/-/tree/main/src/main/java/com/partisiablockchain/language/abiclient/transaction/TransactionReader.java)

#[macro_use]
extern crate pbc_contract_codegen;

use create_type_spec_derive::CreateTypeSpec;
use pbc_contract_common::address::Address;
use pbc_contract_common::context::ContractContext;
use pbc_contract_common::signature::Signature;
use read_write_rpc_derive::ReadWriteRPC;
use read_write_state_derive::ReadWriteState;

/// State for the contract, left empty, unused.
/// The "state" attribute is attached.
#[state]
struct ContractState {}

/// A transaction that has been signed by the private key.
/// One of the two types that is deserialized.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/SignedTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct SignedTransaction {
    signature: Signature,
    inner_part: InnerPart,
}

/// The inner part of a signed transaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/SignedTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct InnerPart {
    core: CoreTransactionPart,
    transaction: InteractWithContractTransaction,
}

/// The core part of any transaction in pbc, contains shared parts of the transaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/CoreTransactionPart.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CoreTransactionPart {
    nonce: i64,
    valid_to_time: i64,
    cost: i64,
}
/// Interact with contract transaction allows to do any interaction with a contract. rpc bytes
/// defines the input to the interaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/contract/InteractWithContractTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct InteractWithContractTransaction {
    contract_id: Address,
    payload: Vec<u8>,
}

/// An event that is executable in the blockchain.
/// One of the two types that is deserialized.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/ExecutableEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct ExecutableEvent {
    origin_shard: Option<String>,
    event_transaction: EventTransaction,
}

/// An event spawned from the blockchain.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/EventTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct EventTransaction {
    hash: [u8; 32],
    inner_event: InnerEvent,
    shard_route: ShardRoute,
    committee_id: i64,
    governance_version: i64,
    height: u8,
    return_envelope: Option<ReturnEnvelope>,
}

/// The actual event of the event transaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/EventTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
enum InnerEvent {
    #[discriminant(0)]
    Transaction { inner_transaction: InnerTransaction },
    #[discriminant(1)]
    Callback {
        callback_to_contract: CallbackToContract,
    },
    #[discriminant(2)]
    System {
        inner_system_event: InnerSystemEvent,
    },
    #[discriminant(3)]
    Sync { sync_event: SyncEvent },
}

/// A transaction event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/EventTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct InnerTransaction {
    from: Address,
    cost: i64,
    transaction: Transaction,
}

/// A callback event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/contract/CallbackToContract.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CallbackToContract {
    address: Address,
    callback_identifier: [u8; 32],
    from: Address,
    cost: i64,
    callback_rpc: Vec<u8>,
}

/// A system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
enum InnerSystemEvent {
    #[discriminant(0)]
    CreateAccount {
        create_account_event: CreateAccountEvent,
    },
    #[discriminant(1)]
    CheckExistence {
        check_existence_event: CheckExistenceEvent,
    },
    #[discriminant(2)]
    SetFeature { set_feature_event: SetFeatureEvent },
    #[discriminant(3)]
    UpdateLocalPluginState {
        update_local_plugin_state_event: UpdateLocalPluginStateEvent,
    },
    #[discriminant(4)]
    UpdateGlobalPluginState {
        update_global_plugin_state_event: UpdateGlobalPluginStateEvent,
    },
    #[discriminant(5)]
    UpdatePlugin {
        update_plugin_event: UpdatePluginEvent,
    },
    #[discriminant(6)]
    Callback { callback_event: CallbackEvent },
    #[discriminant(7)]
    CreateShard {
        create_shard_event: CreateShardEvent,
    },
    #[discriminant(8)]
    RemoveShard {
        remove_shard_event: RemoveShardEvent,
    },
    #[discriminant(9)]
    UpdateContextFreePluginState {
        update_context_free_plugin_state: UpdateContextFreePluginState,
    },
    #[discriminant(10)]
    UpgradeSystemContract {
        upgrade_system_contract_event: UpgradeSystemContractEvent,
    },
    #[discriminant(11)]
    RemoveContract { remove_contract: RemoveContract },
}

/// A create account system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CreateAccountEvent {
    to_create: Address,
}

/// A check existence system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CheckExistenceEvent {
    contract_or_account_address: Address,
}

/// A set feature system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct SetFeatureEvent {
    key: String,
    value: Option<String>,
}

/// An update local plugin system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct UpdateLocalPluginStateEvent {
    chain_plugin_type: ChainPluginType,
    update: LocalPluginStateUpdate,
}

/// Class representing an update to be performed against the local state of a plugin.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/contract/-/blob/main/src/main/java/com/partisiablockchain/contract/sys/LocalPluginStateUpdate.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct LocalPluginStateUpdate {
    context: Address,
    rpc: Vec<u8>,
}

/// Types of plugins in pbc. A plugin is a smart contract that works identically across shards.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/ChainPluginType.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
enum ChainPluginType {
    /**
     * Account plugin controls additional information about account and fees. There are different
     * types of state in relation to the account plugin:
     *
     * <ol>
     *   <li>Global state defines the behaviour and rules.
     *   <li>Shard state gathers the collected fees for services on chain
     *   <li>Account state holds any additional information, this could be BYOC.
     *   <li>Contract state specific parts is for e.g. fees
     * </ol>
     */
    #[discriminant(0)]
    Account {},
    /** Consensus validates block proposals and finalizes correct blocks. */
    #[discriminant(1)]
    Consensus {},
    /** Routing selects the right shard for any transaction. */
    #[discriminant(2)]
    Routing {},
}

/// An update global plugin system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct UpdateGlobalPluginStateEvent {
    chain_plugin_type: ChainPluginType,
    update: GlobalPluginStateUpdate,
}

/// An update that is to be performed against the global state of a plugin.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/contract/-/blob/main/src/main/java/com/partisiablockchain/contract/sys/GlobalPluginStateUpdate.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct GlobalPluginStateUpdate {
    rpc: Vec<u8>,
}

/// An update plugin system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct UpdatePluginEvent {
    chain_plugin_type: ChainPluginType,
    jar: Option<Vec<u8>>,
    invocation: Vec<u8>,
}

/// An callback system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CallbackEvent {
    return_envelope: ReturnEnvelope,
    completed_transaction: [u8; 32],
    success: bool,
    return_value: Vec<u8>,
}

/// A create shard system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CreateShardEvent {
    shard_id: String,
}

/// A remove shard system event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct RemoveShardEvent {
    shard_id: String,
}

/// A system event for updating the context free plugin state.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct UpdateContextFreePluginState {
    chain_plugin_type: ChainPluginType,
    rpc: Vec<u8>,
}

/// System vent for upgrading a system contract.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct UpgradeSystemContractEvent {
    contract_jar: Vec<u8>,
    binder_jar: Vec<u8>,
    abi: Vec<u8>,
    rpc: Vec<u8>,
    contract_address: Address,
}

/// System event used to remove a contract from the chain state.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/InnerSystemEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct RemoveContract {
    contract: Address,
}

/// A sync event.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/SyncEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct SyncEvent {
    accounts: Vec<AccountTransfer>,
}

/// The current state of an account.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/SyncEvent.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct AccountTransfer {
    address: Address,
    nonce: i64,
}

/// Unique routing to a shard.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/ShardRoute.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct ShardRoute {
    target_shard: Option<String>,
    nonce: i64,
}

/// Information about who requires a callback about the execution of a transaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/ReturnEnvelope.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct ReturnEnvelope {
    contract: Address,
}
/// A transaction in pbc.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/transaction/Transaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
enum Transaction {
    #[discriminant(0)]
    DeployContract {
        create_contract_transaction: CreateContractTransaction,
    },
    #[discriminant(1)]
    InteractContract {
        interact_with_contract_transaction: InteractWithContractTransaction,
    },
}

/// Create contract transaction.
/// The java implementation for the type can be found [here](https://gitlab.com/secata/pbc/core/blockchain/-/blob/main/src/main/java/com/partisiablockchain/blockchain/contract/CreateContractTransaction.java)
#[derive(ReadWriteRPC, ReadWriteState, CreateTypeSpec, Clone, PartialEq, Debug)]
struct CreateContractTransaction {
    address: Address,
    binder_jar: Vec<u8>,
    contract_jar: Vec<u8>,
    abi: Vec<u8>,
    rpc: Vec<u8>,
}

/// Initial function to bootstrap the contract's state. Left empty, unused.
#[init]
fn initialize(_ctx: ContractContext) -> ContractState {
    ContractState {}
}
