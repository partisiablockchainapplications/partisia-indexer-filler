use std::io::Result;
fn main() -> Result<()> {
    std::env::set_var("PROTOC", protobuf_src::protoc());
    let mut prost_config = prost_build::Config::new();
    prost_config.protoc_arg("--experimental_allow_proto3_optional").compile_protos(&["src/structs.proto"], &["src/"])?;

    // prost_build::compile_protos(&["src/structs.proto"], &["src/"])?;
    Ok(())
}
