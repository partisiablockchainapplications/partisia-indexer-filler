### Install Rust
[Rust Install](https://rust-lang.github.io/rustup/installation/index.html)

> prerequisites for building this project
```bash
# make sure ubuntu is up to date
sudo apt update

# install rust
curl https://sh.rustup.rs -sSf | sh

# install base-devel
apt install build-essential

# install openssl-sys
apt install libssl-dev

# install pkg config
apt install pkg-config

# install cmake needed for prost-build
apt install cmake
```

> You must also have your postgres database installed and setup
```bash
sudo apt install postgresql postgresql-contrib
sudo systemctl start postgresql.service

# If you want to create a special user and role
sudo su - postgres
createuser --interactive
```

#### Initialize the Postgres DB
```bash
# create an .env file in root with:
$ nano .env
DATABASE_URL=postgres://<user>:<pass>@<ip>/<dn_name>

# (optional) if you want to create the db before building (otherwise SQLX_OFFLINE is required in .env)
# install sqlx-cli
$ cargo install sqlx-cli --no-default-features --version 0.5.9 --features postgres

# create the db tables automatically
$ sqlx database create
$ sqlx migrate run

# generate sqlx-data.json for offline building
cargo sqlx prepare
```

### withdraw sanity check
```sql
-- this should return no rows.  All null token_transfer_id should be filled within 20 seconds by the withdraw poller
select *
from byoc_token_transactions btt
where token_transfer_id is null
```

### Build from source

```bash
# create your config for the app
$ cp _example.env .env
# modify as needed
$ nano .env

# compile
$ cargo build --release

# run
$ cargo run --release
```

### run tests
```bash
cargo test -- --show-output
```

### run
```bash
# display help
cargo run --release -- --help

# run as daemon process
cargo run --release -- -d

# run with custom env path (for example testnet config)
$ cargo run --release -- --rocket-toml-path=RocketTestnet.toml --env-path=_testnet.env --env-toml-path=_testnet.env.toml
```


### Swagger Docs
Swagger based on the OpenApi3 standard can be viewed at endpoint: `/swagger`

